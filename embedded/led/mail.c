// Description:	blink two LEDs with different frequency.
// In my programe: using polling technique to blink two LEDs
// Author:			Le Tuan Anh
// Date:				08/04/2015
#include "MKL46Z4.h"

uint32_t RED_LED = 1ul << 29; // hello
uint32_t GREEN_LED = 1ul << 5;
uint32_t i = 0;
void init_RED_LED(){
	SIM->SCGC5 |= SIM_SCGC5_PORTE_MASK;
	PORTE->PCR[29] = PORT_PCR_MUX(1UL);
	PTE->PDDR |= RED_LED;
}
void init_GREEN_LED(){
	SIM->SCGC5 |= SIM_SCGC5_PORTD_MASK;
	PORTD->PCR[5] = PORT_PCR_MUX(1UL);
	PTD->PDDR |= GREEN_LED;
	
}
//void BLINK_REDLED(){
//	uint32_t i = 0;
//	
//	PTE->PTOR = RED_LED;
//	for(i = 0; i < 3000000; i++){};
//}

//void BLINK_GREENLED(){
//	uint32_t i = 0;
//	
//	PTD->PTOR = GREEN_LED;
//	//for(i = 0; i < 3000000; i++){};
//}
int main(void){
	init_RED_LED();
	init_GREEN_LED();
	
	while(1){
		PTE->PTOR = RED_LED;
		PTD->PTOR = GREEN_LED;
		for(i = 0; i < 1000000; i++)
		{
			if(i == 500000)
				PTE->PTOR = RED_LED;
		}
	}
}
