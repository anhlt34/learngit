// Description:	Using button to toggle led, each press button, frequency is increase
// Author:			Le Tuan Anh
// Date:				14/04/2015

#include "MKL46Z4.h"                    // Device header
uint32_t RED_LED = 1ul << 29;
uint32_t GREEN_LED = 1ul << 5;
uint32_t SW1 = 1ul << 3;
uint32_t SW3 = 1ul << 12;
uint32_t x,y;
uint32_t count = 3000000;
// blink red-led
void init_SW1(){
	SIM->SCGC5 |= SIM_SCGC5_PORTC_MASK;
	PORTC->PCR[3] = PORT_PCR_MUX(1UL) + PORT_PCR_PS_MASK + PORT_PCR_PE_MASK; // GPIO + pullUp enable+ pull enable
	PTC->PDDR &= ~SW1; // PTC3  = input;
}
void init_SW3(){
	SIM->SCGC5 |= SIM_SCGC5_PORTC_MASK;
	PORTC->PCR[12] = PORT_PCR_MUX(1UL) + PORT_PCR_PS_MASK + PORT_PCR_PE_MASK; // GPIO + pullUp enable+ pull enable
	PTC->PDDR &= ~SW1; // PTC12  = input;
}
void init_RED_LED(){
	SIM->SCGC5 |= SIM_SCGC5_PORTE_MASK;
	PORTE->PCR[29] = PORT_PCR_MUX(1UL);
	PTE->PDDR |= RED_LED;
}
void init_GREEN_LED(){
	SIM->SCGC5 |= SIM_SCGC5_PORTD_MASK;
	PORTD->PCR[5] = PORT_PCR_MUX(1UL);
	PTD->PDDR |= GREEN_LED;
	
}

void BLINK_REDLED(){
	//uint32_t i = 0;
	
	PTE->PTOR = RED_LED;
	//for(i = 0; i < 3000000; i++){};
}

void BLINK_GREENLED(uint32_t count){
	uint32_t i = 0;
	
	PTD->PTOR = GREEN_LED; // dao trang thai bit
	for(i = 0; i < count; i++){};
}
int main(){
	init_RED_LED();
	init_GREEN_LED();
	init_SW1();
	init_SW3();
	while(1){
		x = PTC->PDIR & SW3; // x = input
		y = PTC->PDIR & SW1; // x = input
		if(x != SW3)// if press sw1
		{
			while(x != SW3)
			{
				x = PTC->PDIR & SW3; // chong doi phim khi an va giu
			}
			while(1){
				BLINK_GREENLED(count); 
				if((PTC->PDIR & SW3) != SW3)
				{
					count = count/2;
					break;
				}
			}
			
		}
		if(y != SW1)// if press sw1
		{
			while(x != SW1)
			{
				x = PTC->PDIR & SW1; // chong doi phim khi an va giu
			}
			BLINK_REDLED(); 
		}
	}
}
