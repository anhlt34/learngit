#===================================================================================================
#
#   @file    I2c_common.mk
#   @version 
#
#   @brief   
#   @details Makefile definitions for all FLS test suites
#
#===================================================================================================
#   Project              : 
#   Platform             : 
#   Peripheral           : 
#   Dependencies         : 
#
#   Autosar Version      : 
#   Autosar Revision     : 
#   Autosar Conf.Variant :
#   SW Version           : 
#   Build Version        : 
#
#   (c) Copyright M4_SRC_YEAR_ID M4_SRC_COPYRIGHTED_TO
#   All Rights Reserved.
#===================================================================================================
#===================================================================================================
#ifdef(`M4_SRC_KEEP_REVISION_HISTORY', `dnl - DO NOT modify this M4 line!
#Revision History:
#                             Modification     Tracking
#---------------------------   ----------    ------------  ------------------------------------------
#',)dnl - DO NOT modify this M4 line!
#==================================================================================================*/
################################################################################
# Test execution control, if enabled the user controls the execution
# if disabled the wait user block is removed
################################################################################
TEST_WAIT_USER  := FALSE
################################################################################

################################################################################
# List of all source .C  of Generate_code
################################################################################
SRC_DIRS     += $(EUNIT_SRC_DIRS)
INCLUDE_DIRS += $(EUNIT_INCLUDE_DIRS) \
                $(EUNIT_INCLUDE_DIRS)/../../test/beart_suite/ts_07/test/$(PLATFORM)/include 

CFG_FILE = $(wildcard $(CFG_DIR)/$(CONFIG_USED)/*.c)

################################################################################
# List of all head file of Generate_code
################################################################################

HEAD_FILE = $(wildcard $(CFG_DIR)/$(CONFIG_USED)/*.h)

      
ifeq ($(CCOV_ENABLE),ON)
INCLUDE_DIRS += $(DEV_DIR)/vnv_config/include_LDRA
CCOV_FILES+= $(wildcard $(PLATFORM_DIR)/drivers/src/$(LOWER_CASE_MODULE)/*.c)
#CCOV_FILES+= $(wildcard $(PLATFORM_DIR)/hal/src/$(LOWER_CASE_MODULE)/*.c)
SRC_FILES+= $(CCOV_MAIN_SRC_FILE)
endif
################################################################################
# List of all source directories (test, dependency, and generated .c files)
################################################################################
           
SRC_DIRS += $(PLATFORM_DIR)/drivers/src/flash   \
            $(PLATFORM_DIR)/drivers/src/adc   \
            $(PLATFORM_DIR)/drivers/src/lpi2c   \
            $(PLATFORM_DIR)/drivers/src/lpuart   \
            $(PLATFORM_DIR)/drivers/src/pins   \
            $(PLATFORM_DIR)/hal/src/pcc    \
            $(PLATFORM_DIR)/hal/src/port    \
            $(PLATFORM_DIR)/hal/src/scg   \
            $(PLATFORM_DIR)/hal/src/sim  \
            $(PLATFORM_DIR)/hal/src/adc    \
            $(PLATFORM_DIR)/hal/src/lpi2c    \
            $(PLATFORM_DIR)/hal/src/lpuart   \
            $(PLATFORM_DIR)/hal/src/pmc  \
            $(PLATFORM_DIR)/hal/src/rcm    \
            $(PLATFORM_DIR)/hal/src/smc    \
            $(PLATFORM_DIR)/hal/src/sim/S32K144 \
            $(PLATFORM_DIR)/drivers/src/clock  \
            $(PLATFORM_DIR)/drivers/src/clock/$(DERIVATIVE) \
            $(PLATFORM_DIR)/drivers/src/interrupt \
            $(PLATFORM_DIR)/devices \
            $(PLATFORM_DIR)/devices/$(DERIVATIVE)/startup \
            $(GENERATE_DIR)/src
################################################################################
# List of source files
################################################################################

SRC_FILES += $(CFG_DIR)/generic/src/I2c_Common.c 


# Invoking: Standard S32DS Assembler
ifeq ($(TOOLCHAIN),gcc)
    SRC_FILES += $(PLATFORM_DIR)/devices/$(DERIVATIVE)/startup/gcc/startup_S32K144.S
endif

ifeq ($(TOOLCHAIN),iar)
   SRC_FILES += $(PLATFORM_DIR)/devices/$(DERIVATIVE)/startup/iar/startup_S32K144.s
endif

ifeq ($(TOOLCHAIN),ghs)
   SRC_FILES += $(PLATFORM_DIR)/devices/$(DERIVATIVE)/startup/ghs/startup_S32K144.s
endif
################################################################################
# List of all include directories (test, dependency, and generated .h files)
################################################################################
 
INCLUDE_DIRS+= $(GENERATE_DIR)/include \
               $(CFG_DIR)/generic/include \
               $(CFG_DIR)/specific/include \
               $(CFG_DIR)/generic/arm \
               $(PLATFORM_DIR)/devices \
               $(PLATFORM_DIR)/devices/$(DERIVATIVE)/include \
               $(PLATFORM_DIR)/devices/$(DERIVATIVE)/startup \
               $(PLATFORM_DIR)/drivers/inc \
               $(PLATFORM_DIR)/hal/inc \
               $(PLATFORM_DIR)/drivers/src/clock/S32K144 \
               $(PLATFORM_DIR)/drivers/inc \
               $(DEV_DIR)/test/include/S32K14X
#MCU Lite
INCLUDE_DIRS += $(CFG_DIR)/../test/include 
################################################################################
# Definitions
################################################################################
CCOPT            +=     -DSCI_CHANNEL=2 \
                        -DSCI_BAUDRATE=9600 \
                        -DSCI_OSCFREQ=40000000 \
                        -DCPU_S32K144HFT0VLLT \
                        -DCPU_COMMON_INIT
