/***************************************************************************//*!
*   Freescale Semiconductor Inc.
*   (c) Copyright 2011 Freescale Semiconductor Inc.
*   ALL RIGHTS RESERVED.
*
*   @file             I2c_TC_0204.c
*   @details          Test case I2c_TC_0204.
*   @addtogroup       fsl_I2C_TESTS
*
*******************************************************************************/

/*==================================================================================================
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Nguyen Van Hieu (B46843)
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "I2c_TC_0204.h"

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id          I2c_TC_0204
* @brief            Check functional verification
* @details          Check functions:
*                       ADC_DRV_InitConverterStruct
*                       ADC_DRV_ConfigConverter
*                       ADC_DRV_GetConverterConfig
* @pre              N/A
* @post             N/A
*
* @test_level       ComponentValidation
* @test_type        Functional
* @test_technique   BlackBox
* @test_procedure   Steps:
                        -# 1. Initialize for LPI2C0 
                        -# 2. Initialize for LPI2C0 again 
                        -# 3. Initialize with instance =2 
                        -# 4. Finish test case by DeInitialize 
* @pass_criteria    LPI2C_DRV_MasterInit shall install correctly the I2C component.
* @requirements     LPI2C_Requirements.xlsx file
* @execution_type   Automated
* @hw_depend        N/A
* @sw_depend        N/A
* @defects          N/A
* @test_priority    High
* @note             N/A
*
* @keywords         N/A
*
*/
void I2c_TC_0204(void)
{
    /**************************************
    *I2c_TC_0204: LPI2C_DRV_SlaveInit 
    **************************************/
    /* Initialize and configure clocks
    *   -   see clock manager component for details
    */
    CLOCK_SYS_Init(g_clockManConfigsArr, FSL_CLOCK_MANAGER_CONFIG_CNT,
    g_clockManCallbacksArr, FSL_CLOCK_MANAGER_CALLBACK_CNT);
    CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_FORCIBLE);
    /* Initialize pins
    *   -   See PinSettings component for more info
    */
    Pins_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);
    /* Local variable definition */
    /* Table of base addresses for LPI2C instances. */
    LPI2C_Type * const g_lpi2cBase[LPI2C_INSTANCE_COUNT] = LPI2C_BASE_PTRS;
    LPI2C_Type *baseAddr;
    baseAddr = g_lpi2cBase[FSL_LPI2C2];
    
    uint32_t Result_baudRate,Result_baudRateHS;
    lpi2c_status_t Result_lpi2c_status;
    /* Initialize for LPI2C0 */
    Result_lpi2c_status = LPI2C_DRV_SlaveInit(FSL_LPI2C2, &lpi2c2_SlaveConfig_user, &Slave);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    
    /* Check interrupt enable */
    EU_ASSERT(LPI2C_HAL_SlaveGetInt(baseAddr, LPI2C_HAL_SLAVE_BIT_ERROR_INT) ==  true);
    EU_ASSERT(LPI2C_HAL_SlaveGetInt(baseAddr, LPI2C_HAL_SLAVE_FIFO_ERROR_INT) ==  true);
    EU_ASSERT(LPI2C_HAL_SlaveGetInt(baseAddr, LPI2C_HAL_SLAVE_STOP_DETECT_INT) ==  true);
    EU_ASSERT(LPI2C_HAL_SlaveGetInt(baseAddr, LPI2C_HAL_SLAVE_REPEATED_START_INT) ==  true);
    EU_ASSERT(LPI2C_HAL_SlaveGetInt(baseAddr, LPI2C_HAL_SLAVE_ADDRESS_VALID_INT) ==  true);
    EU_ASSERT(LPI2C_HAL_SlaveGetInt(baseAddr, LPI2C_HAL_SLAVE_RECEIVE_DATA_INT) ==  true);
    EU_ASSERT(LPI2C_HAL_SlaveGetInt(baseAddr, LPI2C_HAL_SLAVE_TRANSMIT_DATA_INT) ==  true);
    
    /* Finish test case by DeInitialize */
    LPI2C_DRV_SlaveDeinit(FSL_LPI2C2);
}
#ifdef __cplusplus
}
#endif

/** @} */