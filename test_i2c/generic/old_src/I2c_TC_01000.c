/***************************************************************************//*!
*   Freescale Semiconductor Inc.
*   (c) Copyright 2011 Freescale Semiconductor Inc.
*   ALL RIGHTS RESERVED.
*
*   @file           I2c_TC_01000.c
*   @details        Test case I2c_TC_01000.
*   @addtogroup     fsl_I2C_TESTS
*
*******************************************************************************/

/*==================================================================================================
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Nguyen Van Hieu (B46843)
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "I2c_TC_01000.h"

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id          I2C_TC_01000
* @brief            Check functional verification
* @details          Check functions:
*                        ADC_DRV_InitConverterStruct
*                        ADC_DRV_ConfigConverter
*                        ADC_DRV_GetConverterConfig
* @pre              N/A
* @post             N/A
*
* @test_level       ComponentValidation
* @test_type        Functional
* @test_technique   BlackBox
* @test_procedure   Steps:
*                   -# 1. Check ADC_DRV_InitConverterStruct function shall initialize the converter configuration structure
*                   -# 2. Check variables in adc_average_config_t
*                   -# 2.1        Check :
*                                   dmaEnable. 
*                                   continuousConvEnable.
*                   -# 2.2        Check adc_clk_divide_list
*                   -# 2.3        Check adc_resolution_list
*                   -# 2.4        Check adc_input_clock_list
*                   -# 2.5        Check adc_trigger_list
*                   -# 2.5        Check adc_voltage_reference_list
* @pass_criteria    ADC_DRV_InitConverterStruct function shall initialize the converter configuration structure.
*                   Configure correctly the ADC converter with the options provided in the provided structure.
* @requirements     LPI2C_Requirements.xlsx
* @execution_type   Automated
* @hw_depend        N/A
* @sw_depend        N/A
* @defects          N/A
* @test_priority    High
* @note             N/A
*
* @keywords         N/A
*/
void I2c_TC_01000(void)
{
    uint8 flashCallBack = 0;
    flashCallBack= flashCallBack + 1;
    EU_ASSERT(flashCallBack == 1);
}

#ifdef __cplusplus
}
#endif

/** @} */