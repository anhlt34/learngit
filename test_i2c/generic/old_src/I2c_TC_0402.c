/***************************************************************************//*!
*   Freescale Semiconductor Inc.
*   (c) Copyright 2011 Freescale Semiconductor Inc.
*   ALL RIGHTS RESERVED.
*
*   @file             I2c_TC_0402.c
*   @details          Test case 0001.
*   @addtogroup       fsl_I2C_TESTS
*
*******************************************************************************/

/*==================================================================================================
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Nguyen Van Hieu (B46843)
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "I2c_TC_0402.h"

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id          I2c_TC_0402
* @brief            Check functional verification
* @details          Check functions:
*                        LPI2C_DRV_MasterInit
*                        LPI2C_DRV_MasterDeinit
* @pre              N/A
* @post             N/A
*
* @test_level       ComponentValidation
* @test_type        Functional
* @test_technique   BlackBox
* @test_procedure   Steps:
                        -# 1. Initialize for LPI2C0:   
                                1.1 Initialize clock source, open gate in clock manager.
                                1.2 Initialize pins for LPI2C 0,LPI2C 1.
                                1.3 Initialize LPI2C0 operation with Standard mode and Baud-rate is 1200Hz
                        -# 2. Check master after Initialize driver status structure .
                        -# 3. Check baudRate in Standard mode, baud-rate is 1200Hz .
                        -# 4. Check baudRate in Fast mode, baud-rate is 120000Hz  .
                        -# 5. Check baudRate in Fast Plus mode, baud-rate is 500000Hz .
                        -# 6. Check baudRate in High speed mode, baud-rate is 2000000Hz .
                        -# 7. Check baudRate in High speed mode, baud-rate is 5000000Hz .
* @pass_criteria    LPI2C_DRV_MasterInit function shall initialize the I2C component.
* @requirements     LPI2C_Requirements.xlsx file
* @execution_type   Automated
* @hw_depend        N/A
* @sw_depend        N/A
* @defects          N/A
* @test_priority    High
* @note             N/A
*
* @keywords         N/A
*
*/

void I2c_TC_0402(void)
{
    /**************************************
    *I2c_TC_0402: LPI2C_DRV_SlaveInit
    *
    **************************************/
    /* Initialize and configure clocks
    *   -   see clock manager component for details
    */
    CLOCK_SYS_Init(g_clockManConfigsArr, FSL_CLOCK_MANAGER_CONFIG_CNT,
    g_clockManCallbacksArr, FSL_CLOCK_MANAGER_CALLBACK_CNT);
    CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_FORCIBLE);
    /* Initialize pins
    *   -   See PinSettings component for more info
    */
    Pins_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);
    /* Local variable definition */
    /* Declare address of FSL_LPI2C2 */
    LPI2C_Type * const g_lpi2cBase[LPI2C_INSTANCE_COUNT] = LPI2C_BASE_PTRS;
    LPI2C_Type *baseAddr;
    baseAddr = g_lpi2cBase[FSL_LPI2C2];
    
    uint32_t Result_bytesRemaining;
    lpi2c_status_t Result_lpi2c_status;
    /* LPI2C_DRV_MasterSendDataBlocking LPI2C BUSY*/ 
    uint8_t txBuff[3] = {'I', '2', 'C'};

    lpi2c2_SlaveConfig_user.slaveAddress = 123U,
    lpi2c2_SlaveConfig_user.is10bitAddr = true,
    lpi2c2_SlaveConfig_user.slaveListening = true,
    lpi2c2_SlaveConfig_user.operatingMode = LPI2C_STANDARD_MODE,
    lpi2c2_SlaveConfig_user.slaveCallback = lpi2c2_Callback0,
    lpi2c2_SlaveConfig_user.callbackParam = NULL,
    
    /* Slaver try to send data before initialize FSL_LPI2C2 */ 
    Result_lpi2c_status = LPI2C_DRV_SlaveSendDataBlocking(FSL_LPI2C2, txBuff, sizeof(txBuff));
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_BUSY);
    
    /* Initialize for FSL_LPI2C2 */
    Result_lpi2c_status = LPI2C_DRV_SlaveInit(FSL_LPI2C2, &lpi2c2_SlaveConfig_user, &Slave);
    EU_ASSERT(Result_lpi2c_status  ==  LPI2C_STATUS_SUCCESS);
    
    /* Check LPI2C_DRV_SlaveGetTransmitStatus before transmit*/
    Result_lpi2c_status = LPI2C_DRV_SlaveGetTransmitStatus(FSL_LPI2C2, &Result_bytesRemaining);
    EU_ASSERT(Result_bytesRemaining ==  0);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    
    /* Slaver send data after initialize FSL_LPI2C2 */ 
    Result_lpi2c_status = LPI2C_DRV_SlaveSendDataBlocking(FSL_LPI2C2, txBuff, sizeof(txBuff));
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    
    /* Check LPI2C_DRV_SlaveGetTransmitStatus before transmit*/
    Result_lpi2c_status = LPI2C_DRV_SlaveGetTransmitStatus(FSL_LPI2C2, &Result_bytesRemaining);
    EU_ASSERT(Result_bytesRemaining ==  0);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);

    /* Finish test case by DeInitialize */
    LPI2C_DRV_SlaveDeinit(FSL_LPI2C2);
}

#ifdef __cplusplus
}
#endif

/** @} */