/***************************************************************************//*!
*   Freescale Semiconductor Inc.
*   (c) Copyright 2011 Freescale Semiconductor Inc.
*   ALL RIGHTS RESERVED.
*
*   @file             I2c_TC_0602.c
*   @details         Test case I2c_TC_0602.
*   @addtogroup     fsl_I2C_TESTS
*
*******************************************************************************/

/*==================================================================================================
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Nguyen Van Hieu (B46843)
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "I2c_TC_0602.h"

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id          I2c_TC_0602
* @brief            Check functional verification
* @details          Check functions:
*                        LPI2C_DRV_MasterInit
*                        LPI2C_DRV_MasterDeinit
* @pre              N/A
* @post             N/A
*
* @test_level       ComponentValidation
* @test_type        Functional
* @test_technique   BlackBox
* @test_procedure   Steps:
                        -# 1. Initialize for LPI2C0:   
                                1.1 Initialize clock source, open gate in clock manager.
                                1.2 Initialize pins for LPI2C 0,LPI2C 1.
                                1.3 Initialize LPI2C0 operation with Standard mode and Baud-rate is 1200Hz
                        -# 2. Check LPI2C_DRV_MasterSendDataBlocking before initialize driver structure .
                        -# 3. Check LPI2C_DRV_MasterSendDataBlocking after initialize driver structure .
                        -# 4. Check 
                        -# 5. Check 
                        -# 6. Check 
                        -# 7. Check 
* @pass_criteria    LPI2C_DRV_MasterInit function shall initialize the I2C component.
* @requirements     LPI2C_Requirements.xlsx file
* @execution_type   Automated
* @hw_depend        N/A
* @sw_depend        N/A
* @defects          N/A
* @test_priority    High
* @note             N/A
*
* @keywords         N/A
*
*/

void I2c_TC_0602(void)
{    
    /**************************************
    *   I2c_TC_204: LPI2C_DRV_MasterSendDataBlocking
    **************************************/ 

    /* Initialize and configure clocks
    *   -   see clock manager component for details
    */
    CLOCK_SYS_Init(g_clockManConfigsArr, FSL_CLOCK_MANAGER_CONFIG_CNT,
    g_clockManCallbacksArr, FSL_CLOCK_MANAGER_CALLBACK_CNT);
    CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_FORCIBLE);
    /* Initialize pins
    *   -   See PinSettings component for more info
    */
    Pins_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);
    /* Local variable definition */
    
    uint32_t Result_baudRate, Result_baudRateHS, Result_TxFIFOWatermark;
    uint32_t i;
    bool Result_GetInt;
    lpi2c_status_t Result_lpi2c_status;
    /* Declare address of LPI2C_User_Instance */
    LPI2C_Type *reg_lpi2cBase[LPI2C_INSTANCE_COUNT] = LPI2C_BASE_PTRS;
    LPI2C_Type *base_Addr = reg_lpi2cBase[LPI2C_User_Instance];
    /* Change master and slaver to 10 bit address mode*/
    lpi2c1_MasterConfig_user.slaveAddress = 1023U,
    lpi2c1_MasterConfig_user.is10bitAddr = true,
    lpi2c1_MasterConfig_user.operatingMode = LPI2C_STANDARD_MODE,
    lpi2c1_MasterConfig_user.baudRate = 2000U,
    lpi2c1_MasterConfig_user.baudRateHS = 0U,
    lpi2c1_MasterConfig_user.masterCode = 0U,
    
    lpi2c2_SlaveConfig_user.slaveAddress = 1023U,
    lpi2c2_SlaveConfig_user.is10bitAddr = true,
    lpi2c2_SlaveConfig_user.slaveListening = true,
    lpi2c2_SlaveConfig_user.operatingMode = LPI2C_STANDARD_MODE,
    lpi2c2_SlaveConfig_user.slaveCallback = lpi2c2_Callback0,
    lpi2c2_SlaveConfig_user.callbackParam = NULL,

    /* Initialize for LPI2C_User_Instance */    
    Result_lpi2c_status = LPI2C_DRV_MasterInit(LPI2C_User_Instance, &lpi2c1_MasterConfig_user, &master );
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    /* Initialize for FSL_LPI2C2 */    
    Result_lpi2c_status = LPI2C_DRV_SlaveInit(FSL_LPI2C2, &lpi2c2_SlaveConfig_user, &Slave);
    EU_ASSERT(Result_lpi2c_status  ==  LPI2C_STATUS_SUCCESS);
    /* Make sure you connected correctly LPI2C_User_Instance pins to  FSL_LPI2C2 pins*/ 
    /* Change data in masterMuffer to send and clear data in slaverBuffer to receive*/
    for (i = 0; i < 16; i++) {
        masterBuffer[i] = 0;
        slaveBuffer[i] = i;
    }    
    /* Master send data to slaver */  
    Result_lpi2c_status = LPI2C_DRV_SlaveSendDataBlocking(FSL_LPI2C2, slaveBuffer, sizeof(slaveBuffer));
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    /* Slaver receive data from master */ 
    Result_lpi2c_status = LPI2C_DRV_MasterReceiveDataBlocking(LPI2C_User_Instance, masterBuffer, sizeof(masterBuffer),true);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    /* Verify all data received is correct */ 
    Delay(1000);
    for (i = 0; i < 16; i++) {
        EU_ASSERT(slaveBuffer[i] ==  masterBuffer[i]);
    } 
    /* Finish test case by DeInitialize */
    LPI2C_DRV_MasterDeinit(LPI2C_User_Instance);
    LPI2C_DRV_SlaveDeinit(FSL_LPI2C2);
}

#ifdef __cplusplus
}
#endif

/** @} */