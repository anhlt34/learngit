/***************************************************************************//*!
*   Freescale Semiconductor Inc.
*   (c) Copyright 2011 Freescale Semiconductor Inc.
*   ALL RIGHTS RESERVED.
*
*   @file             I2c_TC_0101.c
*   @details          Test case 0001.
*   @addtogroup       fsl_I2C_TESTS
*
*******************************************************************************/

/*==================================================================================================
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Nguyen Van Hieu (B46843)
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "I2c_TC_0101.h"

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id          I2C_TC_0101
* @brief            Check functional verification
* @details          Check version of LPI2C module and RX,TX buffer size
* @pre              N/A
* @post             N/A
*
* @test_level       ComponentValidation
* @test_type        Functional
* @test_technique   BlackBox
* @test_procedure   Steps:
*                       -#  Initialize and configure clocks.
*                       -#  Initialize pins.
*                       -#  Check the version of the LPI2C module.
*                       -#  Check the size of the Master Receive FIFO.
*                       -#  Check the size of the Master Transmit FIFO.
* @pass_criteria    All values are correct.
* @requirements     LPI2C_HAL_001, LPI2C_HAL_002, LPI2C_HAL_003.
* @execution_type   Automated
* @hw_depend        N/A
* @sw_depend        N/A
* @defects          N/A
* @test_priority    High
* @note             N/A
*
* @keywords         N/A
*
*/

void I2c_TC_0101(void)
{
    /*Initialize and configure clocks*/
    CLOCK_SYS_Init(g_clockManConfigsArr, FSL_CLOCK_MANAGER_CONFIG_CNT,
    g_clockManCallbacksArr, FSL_CLOCK_MANAGER_CALLBACK_CNT);
    CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_FORCIBLE);
    /*Initialize pins*/
    Pins_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);
    /* Local variable definition */
    LPI2C_Type *g_lpi2cBase[LPI2C_INSTANCE_COUNT] = LPI2C_BASE_PTRS;
    LPI2C_Type *baseAddr;
    baseAddr = g_lpi2cBase[LPI2C_User_Instance];
    lpi2c_version_info_t lpi2c_version_info_test;
    uint16_t RxFIFOSize;
    uint16_t TxFIFOSize;

    /*Check the version of the LPI2C module*/
    LPI2C_HAL_GetVersion(baseAddr,&lpi2c_version_info_test);
    EU_ASSERT(lpi2c_version_info_test.majorNumber       == 1);
    EU_ASSERT(lpi2c_version_info_test.minorNumber       == 0);
    EU_ASSERT(lpi2c_version_info_test.featureNumber     == 3);
    /*Check the size of the Master Receive FIFO*/
    RxFIFOSize = LPI2C_HAL_GetMasterRxFIFOSize(baseAddr);
    EU_ASSERT(RxFIFOSize == 4);
    /*Check the size of the Master Transmit FIFO*/
    TxFIFOSize = LPI2C_HAL_GetMasterTxFIFOSize(baseAddr);
    EU_ASSERT(TxFIFOSize == 4);
}

#ifdef __cplusplus
}
#endif

/** @} */
