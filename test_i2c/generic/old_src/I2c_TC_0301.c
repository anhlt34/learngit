/***************************************************************************//*!
*   Freescale Semiconductor Inc.
*   (c) Copyright 2011 Freescale Semiconductor Inc.
*   ALL RIGHTS RESERVED.
*
*   @file             I2c_TC_0301.c
*   @details         Test case I2c_TC_0301.
*   @addtogroup     fsl_I2C_TESTS
*
*******************************************************************************/

/*==================================================================================================
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Nguyen Van Hieu (B46843)
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "I2c_TC_0301.h"

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id          I2c_TC_0301
* @brief            Check functional verification
* @details          Check functions:
*                        LPI2C_DRV_MasterInit
*                        LPI2C_DRV_MasterDeinit
* @pre              N/A
* @post             N/A
*
* @test_level       ComponentValidation
* @test_type        Functional
* @test_technique   BlackBox
* @test_procedure   Steps:
                        -# 1. Initialize for LPI2C0:   
                                1.1 Initialize clock source, open gate in clock manager.
                                1.2 Initialize pins for LPI2C 0,LPI2C 1.
                                1.3 Initialize LPI2C0 operation with Standard mode and Baud-rate is 1200Hz
                        -# 2. Check LPI2C_DRV_MasterSendDataBlocking before initialize driver structure .
                        -# 3. Check LPI2C_DRV_MasterSendDataBlocking after initialize driver structure .
                        -# 4. Check 
                        -# 5. Check 
                        -# 6. Check 
                        -# 7. Check 
* @pass_criteria    LPI2C_DRV_MasterInit function shall initialize the I2C component.
* @requirements     LPI2C_Requirements.xlsx file
* @execution_type   Automated
* @hw_depend        N/A
* @sw_depend        N/A
* @defects          N/A
* @test_priority    High
* @note             N/A
*
* @keywords         N/A
*
*/

void I2c_TC_0301(void)
{    
    /**************************************
    *   I2c_TC_204: LPI2C_DRV_MasterSendDataBlocking
    **************************************/ 

    /* Initialize and configure clocks
    *   -   see clock manager component for details
    */
    CLOCK_SYS_Init(g_clockManConfigsArr, FSL_CLOCK_MANAGER_CONFIG_CNT,
    g_clockManCallbacksArr, FSL_CLOCK_MANAGER_CALLBACK_CNT);
    CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_FORCIBLE);
    /* Initialize pins
    *   -   See PinSettings component for more info
    */
    Pins_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);
    /* Local variable definition */
    
    uint32_t Result_baudRate, Result_baudRateHS, Result_TxFIFOWatermark;
    bool Result_GetInt;
    lpi2c_status_t Result_lpi2c_status;
    /* Declare address of LPI2C_User_Instance */
    LPI2C_Type *reg_lpi2cBase[LPI2C_INSTANCE_COUNT] = LPI2C_BASE_PTRS;
    LPI2C_Type *base_Addr = reg_lpi2cBase[LPI2C_User_Instance];
    /* LPI2C_DRV_MasterSendDataBlocking LPI2C BUSY*/ 
    uint8_t txBuff[3] = {'I', '2', 'C'};
    
    master.rxBuff = NULL;                     /* Pointer to receive data buffer */
    master.rxSize = 0;                        /* Size of receive data buffer */
    master.txBuff = NULL;                     /* Pointer to transmit data buffer */
    master.txSize = 0;                        /* Size of transmit data buffer */
    master.status = 0;                        /* Status of last driver operation */
    master.i2cIdle = true;                    /* Idle/busy state of the driver */
    master.operatingMode = LPI2C_STANDARD_MODE;
    master.slaveAddress = 123U;               /* Slave address */
    master.is10bitAddr = false;
    master.highSpeedInProgress = false;
    
    lpi2c1_MasterConfig_user.slaveAddress = 123U;
    lpi2c1_MasterConfig_user.is10bitAddr = true;
    lpi2c1_MasterConfig_user.operatingMode = LPI2C_STANDARD_MODE;
    lpi2c1_MasterConfig_user.baudRate = 1200U;
    lpi2c1_MasterConfig_user.baudRateHS = 500000;
    lpi2c1_MasterConfig_user.masterCode = 0U;


    /* Master try to send data before initialize LPI2C_User_Instance */  
    Result_lpi2c_status = LPI2C_DRV_MasterSendData(LPI2C_User_Instance, txBuff, sizeof(txBuff), true);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_BUSY);

    /* Initialize for LPI2C_User_Instance */    
    Result_lpi2c_status = LPI2C_DRV_MasterInit(LPI2C_User_Instance, &lpi2c1_MasterConfig_user, &master );
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    /* Master try to send data after initialize LPI2C_User_Instance */  
    Result_lpi2c_status = LPI2C_DRV_MasterSendData(LPI2C_User_Instance, txBuff, sizeof(txBuff), true);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);

    /* Check the number of words in the transmit FIFO
    * after using LPI2C_DRV_MasterQueueData then queue data bytes to fill TX_FIFO
    */
    if(master.is10bitAddr){
        EU_ASSERT(base_Addr->MFSR ==  (sizeof(txBuff)+1));
    }else{
        EU_ASSERT(base_Addr->MFSR ==  sizeof(txBuff));
    }

    /* Check MFCR after set tx FIFO watermark */
    Result_TxFIFOWatermark = LPI2C_HAL_MasterGetTxFIFOWatermark(base_Addr);
    EU_ASSERT(Result_TxFIFOWatermark == 0);

    /* Check enable relevant events */
    Result_GetInt = LPI2C_HAL_MasterGetInt(base_Addr,       LPI2C_HAL_MASTER_FIFO_ERROR_INT |
                                                            LPI2C_HAL_MASTER_ARBITRATION_LOST_INT |
                                                            LPI2C_HAL_MASTER_NACK_DETECT_INT |
                                                            LPI2C_HAL_MASTER_TRANSMIT_DATA_INT);
    EU_ASSERT(Result_GetInt == true);
    /* Finish test case by DeInitialize */
    LPI2C_DRV_MasterDeinit(LPI2C_User_Instance);
}

#ifdef __cplusplus
}
#endif

/** @} */