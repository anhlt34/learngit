/***************************************************************************//*!
*   Freescale Semiconductor Inc.
*   (c) Copyright 2011 Freescale Semiconductor Inc.
*   ALL RIGHTS RESERVED.
*
*   @file             I2c_TC_0105.c
*   @details          Test case 0001.
*   @addtogroup       fsl_I2C_TESTS
*
*******************************************************************************/

/*==================================================================================================
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Nguyen Van Hieu (B46843)
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "I2c_TC_0105.h"

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id          I2c_TC_0105
* @brief            Check functional verification
* @details          Check functions:
*                        LPI2C_DRV_MasterInit
*                        LPI2C_DRV_MasterDeinit
* @pre              N/A
* @post             N/A
*
* @test_level       ComponentValidation
* @test_type        Functional
* @test_technique   BlackBox
* @test_procedure   Steps:
                        -# 1. Initialize for LPI2C0:   
                                1.1 Initialize clock source, open gate in clock manager.
                                1.2 Initialize pins for LPI2C 0,LPI2C 1.
                        -# 2. Check master after Initialize driver status structure .
                        -# 3. Check after enable lpi2c clock .
                        -# 4. Check Enable LPI2C master .
                        -# 5. Finish test case by DeInitialize .
* @pass_criteria    LPI2C_DRV_MasterInit function shall initialize the I2C component.
* @requirements     LPI2C_Requirements.xlsx file
* @execution_type   Automated
* @hw_depend        N/A
* @sw_depend        N/A
* @defects          N/A
* @test_priority    High
* @note             N/A
*
* @keywords         N/A
*
*/

void I2c_TC_0105(void)
{
    /**************************************
    *I2c_TC_0105: LPI2C_DRV_MasterInit
    *
    **************************************/
    /* Initialize and configure clocks
    *   -   see clock manager component for details
    */
    CLOCK_SYS_Init(g_clockManConfigsArr, FSL_CLOCK_MANAGER_CONFIG_CNT,
    g_clockManCallbacksArr, FSL_CLOCK_MANAGER_CALLBACK_CNT);
    CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_FORCIBLE);
    /* Initialize pins
    *   -   See PinSettings component for more info
    */
    Pins_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);
    /* Local variable definition */
    /* Declare address of LPI2C_User_Instance */
    LPI2C_Type *g_lpi2cBase[LPI2C_INSTANCE_COUNT] = LPI2C_BASE_PTRS;
    LPI2C_Type *baseAddr;
    baseAddr = g_lpi2cBase[LPI2C_User_Instance];
    
    lpi2c_status_t Result_lpi2c_status;
    /* Initialize for LPI2C0 */
    lpi2c_status_t status = LPI2C_STATUS_FAIL;
    status= LPI2C_DRV_MasterInit(LPI2C_User_Instance, &lpi2c1_MasterConfig_user, &master );
    EU_ASSERT(status ==  LPI2C_STATUS_SUCCESS);

    /* Check all R/W registers to their reset value */
    EU_ASSERT(baseAddr->MCR == 0x0U);
    EU_ASSERT(baseAddr->MIER == 0x0U);
    EU_ASSERT(baseAddr->MDER == 0x0U);
    EU_ASSERT(baseAddr->MCFGR0 == 0x0U);
    EU_ASSERT(baseAddr->MCFGR1 == 0x0U);
    EU_ASSERT(baseAddr->MCFGR2 == 0x0U);
    EU_ASSERT(baseAddr->MCFGR3 == 0x0U);
    EU_ASSERT(baseAddr->MDMR == 0x0U);
    EU_ASSERT(baseAddr->MCCR0 == 0x0U);
    EU_ASSERT(baseAddr->MCCR1 == 0x0U);
    EU_ASSERT(baseAddr->MFCR == 0x0U);
    EU_ASSERT(baseAddr->SCR == 0x0U);
    EU_ASSERT(baseAddr->SIER == 0x0U);
    EU_ASSERT(baseAddr->SDER == 0x0U);
    EU_ASSERT(baseAddr->SCFGR1 == 0x0U);
    EU_ASSERT(baseAddr->SCFGR2 == 0x0U);
    EU_ASSERT(baseAddr->SAMR == 0x0U);
    EU_ASSERT(baseAddr->STAR == 0x0U);

    /* Check reset interrupt flags */
    EU_ASSERT(baseAddr->MSR == (    LPI2C_HAL_MASTER_DATA_MATCH_INT |
                                    LPI2C_HAL_MASTER_PIN_LOW_TIMEOUT_INT |
                                    LPI2C_HAL_MASTER_FIFO_ERROR_INT |
                                    LPI2C_HAL_MASTER_ARBITRATION_LOST_INT |
                                    LPI2C_HAL_MASTER_NACK_DETECT_INT |
                                    LPI2C_HAL_MASTER_STOP_DETECT_INT |
                                    LPI2C_HAL_MASTER_END_PACKET_INT |
                                    LPI2C_HAL_MASTER_RECEIVE_DATA_INT |
                                    LPI2C_HAL_MASTER_TRANSMIT_DATA_INT));
    EU_ASSERT(baseAddr->SSR == (    LPI2C_HAL_SLAVE_SMBUS_ALERT_RESPONSE_INT |
                                    LPI2C_HAL_SLAVE_GENERAL_CALL_INT |
                                    LPI2C_HAL_SLAVE_ADDRESS_MATCH_1_INT |
                                    LPI2C_HAL_SLAVE_ADDRESS_MATCH_0_INT |
                                    LPI2C_HAL_SLAVE_FIFO_ERROR_INT |
                                    LPI2C_HAL_SLAVE_BIT_ERROR_INT |
                                    LPI2C_HAL_SLAVE_STOP_DETECT_INT |
                                    LPI2C_HAL_SLAVE_REPEATED_START_INT |
                                    LPI2C_HAL_SLAVE_TRANSMIT_ACK_INT |
                                    LPI2C_HAL_SLAVE_ADDRESS_VALID_INT |
                                    LPI2C_HAL_SLAVE_RECEIVE_DATA_INT |
                                    LPI2C_HAL_SLAVE_TRANSMIT_DATA_INT));
    /* Finish test case by DeInitialize */
    LPI2C_DRV_MasterDeinit(LPI2C_User_Instance);
}

#ifdef __cplusplus
}
#endif

/** @} */
