/***************************************************************************//*!
*   Freescale Semiconductor Inc.
*   (c) Copyright 2011 Freescale Semiconductor Inc.
*   ALL RIGHTS RESERVED.
*
*   @file             I2c_TC_0108.c
*   @details          Test case I2c_TC_0108.
*   @addtogroup       fsl_I2C_TESTS
*
*******************************************************************************/

/*==================================================================================================
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Nguyen Van Hieu (B46843)
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "I2c_TC_0108.h"

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id          I2c_TC_0108
* @brief            Check functional verification
* @details          Check functions:
*                        ADC_DRV_InitConverterStruct
*                        ADC_DRV_ConfigConverter
*                        ADC_DRV_GetConverterConfig
* @pre              N/A
* @post             N/A
*
* @test_level       ComponentValidation
* @test_type        Functional
* @test_technique   BlackBox
* @test_procedure   Steps:
                        -# 1. Initialize for LPI2C0
                        -# 2. DeInitialize for LPI2C0 &LPI2C1
                        -# 3. Address of LPI2C0
* @pass_criteria    LPI2C_DRV_MasterSetSlaveAddr can set slaver address of LPI2C component.
* @requirements     LPI2C_Requirements.xlsx
* @execution_type   Automated
* @hw_depend        N/A
* @sw_depend        N/A
* @defects          N/A
* @test_priority    High
* @note             N/A
*
* @keywords         N/A
*/


void I2c_TC_0108(void)
{    
    /**************************************
    *   I2c_TC_203: LPI2C_DRV_MasterSetSlaveAddr
    *
    **************************************/ 
    lpi2c_master_state_t  master = {
        .rxBuff = NULL,                         /* Pointer to receive data buffer */
        .rxSize = 0,                            /* Size of receive data buffer */
        .txBuff = NULL,                         /* Pointer to transmit data buffer */
        .txSize = 0,                            /* Size of transmit data buffer */
        .status = 0,                            /* Status of last driver operation */
        .i2cIdle = true,                        /* Idle/busy state of the driver */
        .slaveAddress = 123U,                   /* Slave address */
        .is10bitAddr = false,

    };
    lpi2c_master_user_config_t lpi2c1_MasterConfig_user = {
        .slaveAddress = 123U,
        .is10bitAddr = true,
        .baudRate = 1200U,
    };
    /* Initialize for LPI2C0 */
    lpi2c_status_t lpi2c_status_result = LPI2C_STATUS_FAIL;
    lpi2c_status_result = LPI2C_DRV_MasterInit(LPI2C_User_Instance, &lpi2c1_MasterConfig_user, &master );
    EU_ASSERT(lpi2c_status_result ==  LPI2C_STATUS_SUCCESS);

    /* Check initialization for LPI2C master slaver address LPI2C_User_Instance is correct */
    LPI2C_DRV_MasterSetSlaveAddr(LPI2C_User_Instance, 0x100, false);
    EU_ASSERT(master.slaveAddress ==  0x100);
    EU_ASSERT(master.is10bitAddr ==  true);
    
    /* Finish test case by DeInitialize */
    LPI2C_DRV_MasterDeinit(LPI2C_User_Instance);
}

#ifdef __cplusplus
}
#endif

/** @} */
