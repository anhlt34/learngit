/***************************************************************************//*!
*   Freescale Semiconductor Inc.
*   (c) Copyright 2011 Freescale Semiconductor Inc.
*   ALL RIGHTS RESERVED.
*
*   @file             I2c_TC_0101.c
*   @details          Test case 0001.
*   @addtogroup       fsl_I2C_TESTS
*
*******************************************************************************/

/*==================================================================================================
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Nguyen Van Hieu (B46843)
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "I2c_TC_0101.h"

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id          I2C_TC_0101
* @brief            Check functional verification
* @details          Check functions:
*                        LPI2C_DRV_MasterInit
*                        LPI2C_DRV_MasterDeinit
* @pre              N/A
* @post             N/A
*
* @test_level       ComponentValidation
* @test_type        Functional
* @test_technique   BlackBox
* @test_procedure   Steps:
                        -# 1. Initialize for LPI2C0:   
                                1.1 Initialize clock source, open gate in clock manager.
                                1.2 Initialize pins for LPI2C 0,LPI2C 1.
                                1.3 Initialize LPI2C0 operation with Standard mode and Baud-rate is 1200Hz
                        -# 2. Check master after Initialize driver status structure .
                        -# 3. Check baudRate in Standard mode, baud-rate is 1200Hz .
                        -# 4. Check baudRate in Fast mode, baud-rate is 120000Hz  .
                        -# 5. Check baudRate in Fast Plus mode, baud-rate is 500000Hz .
                        -# 6. Check baudRate in High speed mode, baud-rate is 2000000Hz .
                        -# 7. Check baudRate in High speed mode, baud-rate is 5000000Hz .
* @pass_criteria    LPI2C_DRV_MasterInit function shall initialize the I2C component.
* @requirements     LPI2C_Requirements.xlsx file
* @execution_type   Automated
* @hw_depend        N/A
* @sw_depend        N/A
* @defects          N/A
* @test_priority    High
* @note             N/A
*
* @keywords         N/A
*
*/

void I2c_TC_0101(void)
{
    /**************************************
    *Adc_TC_101: LPI2C_DRV_MasterInit
    *
    **************************************/
    /* Initialize and configure clocks
    *   -   see clock manager component for details
    */
    CLOCK_SYS_Init(g_clockManConfigsArr, FSL_CLOCK_MANAGER_CONFIG_CNT,
    g_clockManCallbacksArr, FSL_CLOCK_MANAGER_CALLBACK_CNT);
    CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_FORCIBLE);
    /* Initialize pins
    *   -   See PinSettings component for more info
    */
    Pins_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);
    /* Local variable definition */
    /* Declare address of LPI2C_User_Instance */
    LPI2C_Type *g_lpi2cBase[LPI2C_INSTANCE_COUNT] = LPI2C_BASE_PTRS;
    LPI2C_Type *baseAddr;
    baseAddr = g_lpi2cBase[LPI2C_User_Instance];
    
    uint32_t Result_baudRate,Result_baudRateHS;
    lpi2c_status_t Result_lpi2c_status;
    /* Initialize for LPI2C0 */
    lpi2c_status_t status = LPI2C_STATUS_FAIL;
    status= LPI2C_DRV_MasterInit(LPI2C_User_Instance, &lpi2c1_MasterConfig_user, &master );
    EU_ASSERT(status ==  LPI2C_STATUS_SUCCESS);

    /* Check master after Initialize driver status structure */
    EU_ASSERT(master.cmdQueue.readIdx ==  0U);
    EU_ASSERT(master.cmdQueue.writeIdx ==  0U);
    EU_ASSERT(master.rxBuff ==  NULL);
    EU_ASSERT(master.rxSize ==  0);
    EU_ASSERT(master.txBuff ==  NULL);
    EU_ASSERT(master.txSize ==  0);
    EU_ASSERT(master.status ==  LPI2C_STATUS_SUCCESS);
    EU_ASSERT(master.i2cIdle ==  true);
    EU_ASSERT(master.slaveAddress ==  lpi2c1_MasterConfig_user.slaveAddress);
    EU_ASSERT(master.is10bitAddr ==  lpi2c1_MasterConfig_user.is10bitAddr);

    /* Check baudRate in Standard mode, baud-rate is 1200Hz */
    Result_lpi2c_status = LPI2C_DRV_MasterGetBaudRate(LPI2C_User_Instance, &Result_baudRate, &Result_baudRateHS);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    EU_ASSERT((Result_baudRate >=  (uint32_t)(lpi2c1_MasterConfig_user.baudRate*0.9))&(Result_baudRate <=  (uint32_t)(lpi2c1_MasterConfig_user.baudRate*1.1)));

    /* Check baudRate in Fast mode, baud-rate is 120000Hz */
    lpi2c1_MasterConfig_user.operatingMode = LPI2C_FAST_MODE;
    lpi2c1_MasterConfig_user.baudRate = 120000U;
    LPI2C_DRV_MasterDeinit(LPI2C_User_Instance);
    status = LPI2C_DRV_MasterInit(LPI2C_User_Instance, &lpi2c1_MasterConfig_user, &master );
    EU_ASSERT(status ==  LPI2C_STATUS_SUCCESS);
    Result_lpi2c_status = LPI2C_DRV_MasterGetBaudRate(LPI2C_User_Instance, &Result_baudRate, &Result_baudRateHS);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    EU_ASSERT((Result_baudRate >=  (uint32_t)(lpi2c1_MasterConfig_user.baudRate*0.9))&(Result_baudRate <=  (uint32_t)(lpi2c1_MasterConfig_user.baudRate*1.1)));
    /* Check baudRate in Fast Plus mode, baud-rate is 500000Hz */
    lpi2c1_MasterConfig_user.operatingMode = LPI2C_FASTPLUS_MODE;
    lpi2c1_MasterConfig_user.baudRate = 500000U;
    LPI2C_DRV_MasterDeinit(LPI2C_User_Instance);
    status = LPI2C_DRV_MasterInit(LPI2C_User_Instance, &lpi2c1_MasterConfig_user, &master );
    EU_ASSERT(status ==  LPI2C_STATUS_SUCCESS);
    Result_lpi2c_status = LPI2C_DRV_MasterGetBaudRate(LPI2C_User_Instance, &Result_baudRate, &Result_baudRateHS);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    EU_ASSERT((Result_baudRate >=  (uint32_t)(lpi2c1_MasterConfig_user.baudRate*0.9))&(Result_baudRate <=  (uint32_t)(lpi2c1_MasterConfig_user.baudRate*1.1)));
    /* Check baudRate in High speed mode, baud-rate is 2000000Hz */
    lpi2c1_MasterConfig_user.operatingMode = LPI2C_HIGHSPEED_MODE;
    lpi2c1_MasterConfig_user.baudRateHS = 2000000U;
    LPI2C_DRV_MasterDeinit(LPI2C_User_Instance);
    status = LPI2C_DRV_MasterInit(LPI2C_User_Instance, &lpi2c1_MasterConfig_user, &master );
    EU_ASSERT(status ==  LPI2C_STATUS_SUCCESS);
    Result_lpi2c_status = LPI2C_DRV_MasterGetBaudRate(LPI2C_User_Instance, &Result_baudRate, &Result_baudRateHS);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    EU_ASSERT((Result_baudRate >=  (uint32_t)(lpi2c1_MasterConfig_user.baudRate*0.9))&(Result_baudRate <=  (uint32_t)(lpi2c1_MasterConfig_user.baudRate*1.1)));
    EU_ASSERT(Result_baudRateHS ==  lpi2c1_MasterConfig_user.baudRateHS);
   /* Check baudRate in High speed mode, baud-rate is 5000000Hz */
    lpi2c1_MasterConfig_user.operatingMode = LPI2C_ULTRAFAST_MODE;
    lpi2c1_MasterConfig_user.baudRateHS = 5000000U;
    LPI2C_DRV_MasterDeinit(LPI2C_User_Instance);
    status = LPI2C_DRV_MasterInit(LPI2C_User_Instance, &lpi2c1_MasterConfig_user, &master );
    EU_ASSERT(status ==  LPI2C_STATUS_SUCCESS);
    Result_lpi2c_status = LPI2C_DRV_MasterGetBaudRate(LPI2C_User_Instance, &Result_baudRate, &Result_baudRateHS);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    EU_ASSERT((Result_baudRate >=  (uint32_t)(lpi2c1_MasterConfig_user.baudRate*0.9))&(Result_baudRate <=  (uint32_t)(lpi2c1_MasterConfig_user.baudRate*1.1)));
    EU_ASSERT(Result_baudRateHS ==  lpi2c1_MasterConfig_user.baudRateHS);
   
    /* Finish test case by DeInitialize */
    LPI2C_DRV_MasterDeinit(LPI2C_User_Instance);
}

#ifdef __cplusplus
}
#endif

/** @} */
