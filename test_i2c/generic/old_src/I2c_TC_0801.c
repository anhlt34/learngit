/***************************************************************************//*!
*   Freescale Semiconductor Inc.
*   (c) Copyright 2011 Freescale Semiconductor Inc.
*   ALL RIGHTS RESERVED.
*
*   @file             I2c_TC_0701.c
*   @details          Test case I2c_TC_0701.
*   @addtogroup       fsl_I2C_TESTS
*
*******************************************************************************/

/*==================================================================================================
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Nguyen Van Hieu (B46843)
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "I2c_TC_0701.h"

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id          I2c_TC_0701
* @brief            Check functional verification
* @details          Check functions:
*                        ADC_DRV_InitConverterStruct
*                        ADC_DRV_ConfigConverter
*                        ADC_DRV_GetConverterConfig
* @pre              N/A
* @post             N/A
*
* @test_level       ComponentValidation
* @test_type        Functional
* @test_technique   BlackBox
* @test_procedure   Steps:
*                   -# 1. Configure I2C Master operate in follow mode:
                                - Operation mode: Standard Mode - Master
                                - Baud-rate: 100000(Hz)
                                - Slave address(7 bit): 0x20 (Because Master transmit address = address<<1 + bit-direction)
*                   -# 2. Connect J58: 1-2  ;   4-5
*                   -# 3. Connect J35
*                   -# 4. Connect J56 to PICkit™ SERIAL I2C DEMO BOARD
*                   -# 5. Run code and check LEDs status: They must blink 10 time.
* @pass_criteria    Check communication of S32K144 to PICkit SERIAL I2C DEMO BOARD( The LEDs must bink 10 time
* @requirements     LPI2C_Requirements.xlsx
* @execution_type   Automated
* @hw_depend        N/A
* @sw_depend        N/A
* @defects          N/A
* @test_priority    High
* @note             N/A
*
* @keywords         N/A
*/

void I2c_TC_0701(void)
{
    /**************************************
    *I2c_TC_0701: Initialization MCP23008 Board
    **************************************/
    /* Write your local variable definition here */
    lpi2c_master_state_t lpi2c0MasterState;
    lpi2c_slave_state_t lpi2c1SlaveState;
    lpi2c_status_t Result_lpi2c_status;
    lpi2c_nack_config_t lpi2c_nack_config_status;
    uint8_t i,Value;
    
    /* Initialize and configure clocks
    *   -   see clock manager component for details
    */
    CLOCK_SYS_Init(g_clockManConfigsArr, FSL_CLOCK_MANAGER_CONFIG_CNT,
    g_clockManCallbacksArr, FSL_CLOCK_MANAGER_CALLBACK_CNT);
    CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_FORCIBLE);
    /* Initialize pins
    *   -   See PinSettings component for more info
    */
    Pins_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);
    /* Change master to Standard Mode, baud-rate is 100k bps*/
    lpi2c1_MasterConfig_user.slaveAddress = 0x20U,
    lpi2c1_MasterConfig_user.is10bitAddr = false,
    lpi2c1_MasterConfig_user.operatingMode = LPI2C_STANDARD_MODE,
    lpi2c1_MasterConfig_user.baudRate = 100000U,
    lpi2c1_MasterConfig_user.baudRateHS = 100000U,
    lpi2c1_MasterConfig_user.masterCode = 0U,
    /* Initialize for LPI2C_User_Instance */    
    Result_lpi2c_status = LPI2C_DRV_MasterInit(LPI2C_User_Instance, &lpi2c1_MasterConfig_user, &master );
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    /* Configure IO DIR for MCP23008 */
    MCP23008_Write(MCP_REG_IODIR, 0x00);
    /* Configure IO GPIO for MCP23008 */
    Value = 0x55 ; 
    for(i=0;i<10;i++)
    {
        MCP23008_Write(MCP_REG_GPIO, Value);
        Value = ~Value;
        Delay(1000);
    }
    /* Configure IO OLAT for MCP23008 */
    Value = 0x55 ; 
    for(i=0;i<10;i++)
    {
        MCP23008_Write(MCP_REG_OLAT, Value);
        Value = ~Value;
        Delay(1000);
    }  
    for (i = 0; i < 16; i++) {
        masterBuffer[i] = 0;
    }    
    Result_lpi2c_status = LPI2C_DRV_MasterReceiveDataBlocking(LPI2C_User_Instance, masterBuffer, 2, true);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
}

#ifdef __cplusplus
}
#endif

/** @} */