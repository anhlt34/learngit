/***************************************************************************//*!
*   Freescale Semiconductor Inc.
*   (c) Copyright 2011 Freescale Semiconductor Inc.
*   ALL RIGHTS RESERVED.
*
*   @file             I2c_TC_0107.c
*   @details          Test case I2c_TC_0107.
*   @addtogroup       fsl_I2C_TESTS
*
*******************************************************************************/

/*==================================================================================================
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Nguyen Van Hieu (B46843)
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "I2c_TC_0107.h"

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id          I2c_TC_0107
* @brief            Check functional verification
* @details          Check functions:
*                        ADC_DRV_InitConverterStruct
*                        ADC_DRV_ConfigConverter
*                        ADC_DRV_GetConverterConfig
* @pre              N/A
* @post             N/A
*
* @test_level       ComponentValidation
* @test_type        Functional
* @test_technique   BlackBox
* @test_procedure   Steps:
                        -# 1. Deinit for LPI2C0
                        -# 2. Deinit with over limit: instance = 2
                        -# 3. Check disable lpi2c clock
                        -# 4. Init LPI2C_User_Instance again to sure Deinit successfully
                        -# 5. Finish test case by DeInitialize
* @pass_criteria    LPI2C_DRV_MasterInit shall install correctly the I2C component.
* @requirements     LPI2C_Requirements.xlsx
* @execution_type   Automated
* @hw_depend        N/A
* @sw_depend        N/A
* @defects          N/A
* @test_priority    High
* @note             N/A
*
* @keywords         N/A
*/
void I2c_TC_0107(void)
{
    /**************************************
    *I2c_TC_0107: LPUART_DRV_Deinit
    **************************************/

    lpi2c_status_t status = LPI2C_STATUS_FAIL;
    status = LPI2C_DRV_MasterInit(LPI2C_User_Instance, &lpi2c1_MasterConfig_user, &master );
    EU_ASSERT(status ==  LPI2C_STATUS_SUCCESS);

    /* Deinit for LPI2C0*/
    status = LPI2C_DRV_MasterDeinit(LPI2C_User_Instance);
    EU_ASSERT(status == LPI2C_STATUS_SUCCESS);

    /* Check that Deinit function shall disable lpi2c clock */
    const uint16_t g_lpi2cPccPeriph[LPI2C_INSTANCE_COUNT] = {PCC_LPI2C0_INDEX, PCC_LPI2C1_INDEX};
    bool reGetClock = PCC_HAL_GetClockMode(PCC,g_lpi2cPccPeriph[LPI2C_User_Instance]);
    EU_ASSERT(reGetClock == false);

    /* Init LPI2C_User_Instance again to sure Deinit successfully */
    status = LPI2C_DRV_MasterInit(LPI2C_User_Instance, &lpi2c1_MasterConfig_user, &master );
    EU_ASSERT(status ==  LPI2C_STATUS_SUCCESS);

    /* Finish test case by DeInitialize */
    LPI2C_DRV_MasterDeinit(LPI2C_User_Instance);
}
#ifdef __cplusplus
}
#endif

/** @} */
