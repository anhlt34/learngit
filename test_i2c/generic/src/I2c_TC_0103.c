/***************************************************************************//*!
*   Freescale Semiconductor Inc.
*   (c) Copyright 2011 Freescale Semiconductor Inc.
*   ALL RIGHTS RESERVED.
*
*   @file             I2c_TC_0103.c
*   @details          Test case 0001.
*   @addtogroup       fsl_I2C_TESTS
*
*******************************************************************************/

/*==================================================================================================
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Nguyen Van Hieu (B46843)
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "I2c_TC_0103.h"

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id          I2c_TC_0103
* @brief            Check functional verification
* @details          Check LPI2C_HAL_Init function shall initializes all the registers of the LPI2C module to the reset value
* @pre              N/A
* @post             N/A
*
* @test_level       ComponentValidation
* @test_type        Functional
* @test_technique   BlackBox
* @test_procedure   Steps:
*                       -#  Initialize and configure clocks.
*                       -#  Initialize pins.
*                       -#  Initializes the LPI2C module to reset state.
*                       -#  Verify all R/W registers to their reset value.
*                       -#  Verify interrupt flags are reseted.
*                       -#  Reset master Queue status.
*                       -#  Verify that all master queue status are reseted.
* @pass_criteria    The registers of the LPI2C module are the reset value.
* @requirements     N/A
* @execution_type   Automated
* @hw_depend        N/A
* @sw_depend        N/A
* @defects          N/A
* @test_priority    High
* @note             N/A
*
* @keywords         N/A
*
*/

void I2c_TC_0103(void)
{
    /*Initialize and configure clocks*/
    CLOCK_SYS_Init(g_clockManConfigsArr, FSL_CLOCK_MANAGER_CONFIG_CNT,
            g_clockManCallbacksArr, FSL_CLOCK_MANAGER_CALLBACK_CNT);
    CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_FORCIBLE);

    /*Initialize pins*/
    Pins_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);

    /* Local variable definition */
    /*Initializes the LPI2C module to reset state*/
    LPI2C_HAL_Init(LPI2C_base_user);
    /*Verify all R/W registers to their reset value*/
    EU_ASSERT(LPI2C_base_user->MCR       == 0x0U);
    EU_ASSERT(LPI2C_base_user->MIER      == 0x0U);
    EU_ASSERT(LPI2C_base_user->MDER      == 0x0U);
    EU_ASSERT(LPI2C_base_user->MCFGR0    == 0x0U);
    EU_ASSERT(LPI2C_base_user->MCFGR1    == 0x0U);
    EU_ASSERT(LPI2C_base_user->MCFGR2    == 0x0U);
    EU_ASSERT(LPI2C_base_user->MCFGR3    == 0x0U);
    EU_ASSERT(LPI2C_base_user->MDMR      == 0x0U);
    EU_ASSERT(LPI2C_base_user->MCCR0     == 0x0U);
    EU_ASSERT(LPI2C_base_user->MCCR1     == 0x0U);
    EU_ASSERT(LPI2C_base_user->MFCR      == 0x0U);
    EU_ASSERT(LPI2C_base_user->SCR       == 0x0U);
    EU_ASSERT(LPI2C_base_user->SIER      == 0x0U);
    EU_ASSERT(LPI2C_base_user->SDER      == 0x0U);
    EU_ASSERT(LPI2C_base_user->SCFGR1    == 0x0U);
    EU_ASSERT(LPI2C_base_user->SCFGR2    == 0x0U);
    EU_ASSERT(LPI2C_base_user->SAMR      == 0x0U);
    EU_ASSERT(LPI2C_base_user->STAR      == 0x0U);

    /* Verify interrupt flags are reseted*/
    EU_ASSERT(LPI2C_base_user->MSR == 0x00000001);
    EU_ASSERT(LPI2C_base_user->SSR == 0x00000000);
}

/** @} */
