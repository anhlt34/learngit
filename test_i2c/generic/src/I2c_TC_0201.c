/***************************************************************************//*!
*   Freescale Semiconductor Inc.
*   (c) Copyright 2011 Freescale Semiconductor Inc.
*   ALL RIGHTS RESERVED.
*
*   @file             I2c_TC_0201.c
*   @details          Test case 0001.
*   @addtogroup       fsl_I2C_TESTS
*
*******************************************************************************/

/*==================================================================================================
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Nguyen Van Hieu (B46843)
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "I2c_TC_0201.h"

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id          I2c_TC_0201
* @brief            Check functional verification
* @details          Check function LPI2C_DRV_SlaveInit shall configure correctly driver status.
* @pre              N/A
* @post             N/A
*
* @test_level       ComponentValidation
* @test_type        Functional
* @test_technique   BlackBox
* @test_procedure   Steps:
*                       -#  Initialize and configure clocks.
*                       -#  Initialize pins.
*                       -#  Initialize for LPI2C module.
*                       -#  Check Slave state after Initialize driver status structure.
* @pass_criteria    All driver status of LPI2C module is correct.
* @requirements     Driver: LPI2C_DRV_027. HAL:
* @execution_type   Automated
* @hw_depend        N/A
* @sw_depend        N/A
* @defects          N/A
* @test_priority    High
* @note             N/A
*
* @keywords         N/A
*
*/

void I2c_TC_0201(void)
{
    /*Initialize and configure clocks*/
    CLOCK_SYS_Init(g_clockManConfigsArr, FSL_CLOCK_MANAGER_CONFIG_CNT,
            g_clockManCallbacksArr, FSL_CLOCK_MANAGER_CALLBACK_CNT);
    CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_FORCIBLE);
    /*Initialize pins*/
    Pins_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);
    /* Local variable definition */
    uint32_t Result_baudRate,Result_baudRateHS;
    lpi2c_status_t Result_lpi2c_status;
    /* Initialize for LPI2C module */
    Result_lpi2c_status = LPI2C_DRV_SlaveInit(LPI2C_User_Instance, &lpi2c2_SlaveConfig_user, &Slave);
    EU_ASSERT(Result_lpi2c_status  ==  LPI2C_STATUS_SUCCESS);

    /* Check Slave state after Initialize driver status structure */
    EU_ASSERT(Slave.status ==  LPI2C_STATUS_SUCCESS);
    EU_ASSERT(Slave.txBuff == NULL);
    EU_ASSERT(Slave.rxBuff == NULL);
    EU_ASSERT(Slave.txSize == 0U);
    EU_ASSERT(Slave.rxSize == 0U);
    EU_ASSERT(Slave.txBusy == false);
    EU_ASSERT(Slave.rxBusy == false);

    /* Finish test case by DeInitialize */
    LPI2C_DRV_SlaveDeinit(LPI2C_User_Instance);
}

#ifdef __cplusplus
}
#endif

/** @} */
