/***************************************************************************//*!
*   Freescale Semiconductor Inc.
*   (c) Copyright 2011 Freescale Semiconductor Inc.
*   ALL RIGHTS RESERVED.
*
*   @file             I2c_TC_0107.c
*   @details          Test case I2c_TC_0107.
*   @addtogroup       fsl_I2C_TESTS
*
*******************************************************************************/

/*==================================================================================================
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Nguyen Van Hieu (B46843)
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "I2c_TC_0107.h"

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id          I2c_TC_0107
* @brief            Check functional verification
* @details          Check function LPI2C_DRV_MasterInit cannot configure LPI2C module when it was installed.
* @pre              N/A
* @post             N/A
*
* @test_level       ComponentValidation
* @test_type        Functional
* @test_technique   BlackBox
* @test_procedure   Steps:
                        -# 1. Initialize for LPI2C0 
                        -# 2. Initialize for LPI2C0 again 
                        -# 3. Initialize with instance =2 
                        -# 4. Finish test case by DeInitialize 
* @pass_criteria    LPI2C_DRV_MasterInit cannot configure LPI2C module when it was installed.
* @requirements     Driver: LPI2C_DRV_001.
* @execution_type   Automated
* @hw_depend        N/A
* @sw_depend        N/A
* @defects          N/A
* @test_priority    High
* @note             N/A
*
* @keywords         N/A
*
*/
void I2c_TC_0107(void)
{
    /* Local variable definition */
    lpi2c_status_t status = LPI2C_STATUS_FAIL;

    /* Initialize for LPI2C_User_Instance */
    status= LPI2C_DRV_MasterInit(LPI2C_User_Instance, &lpi2c1_MasterConfig_user, &master );
    EU_ASSERT(status ==  LPI2C_STATUS_SUCCESS);

    /* Initialize for LPI2C_User_Instance again */
    status = LPI2C_DRV_MasterInit(LPI2C_User_Instance, &lpi2c1_MasterConfig_user, &master );
    EU_ASSERT(status == LPI2C_STATUS_FAIL);

    /* Initialize with instance =2 */ 
    status = LPI2C_DRV_MasterInit(2, &lpi2c1_MasterConfig_user, &master );
    EU_ASSERT(status ==  LPI2C_STATUS_FAIL);

    /* Finish test case by DeInitialize */
    LPI2C_DRV_MasterDeinit(LPI2C_User_Instance);
}
#ifdef __cplusplus
}
#endif

/** @} */
