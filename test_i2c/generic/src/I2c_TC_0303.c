/***************************************************************************//*!
*   Freescale Semiconductor Inc.
*   (c) Copyright 2011 Freescale Semiconductor Inc.
*   ALL RIGHTS RESERVED.
*
*   @file             I2c_TC_0303.c
*   @details         Test case I2c_TC_0303.
*   @addtogroup     fsl_I2C_TESTS
*
*******************************************************************************/

/*==================================================================================================
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Nguyen Van Hieu (B46843)
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "I2c_TC_0303.h"

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id          I2c_TC_0303
* @brief            Check functional verification
* @details          Check functions
* @pre              N/A
* @post             N/A
*
* @test_level       ComponentValidation
* @test_type        Functional
* @test_technique   BlackBox
* @test_procedure   Steps:
*                       -#  .
*                       -#  .
*                       -#  .
*                       -#  .
*                       -#  .
*                       -#  .
*                       -#  .
*                       -#  .
*                       -#  .
*                       -#  .
*                       -#  .
* @pass_criteria    .
* @requirements     Driver: HAL: .
* @execution_type   Automated
* @hw_depend        N/A
* @sw_depend        N/A
* @defects          N/A
* @test_priority    High
* @note             N/A
*
* @keywords         N/A
*
*/

void I2c_TC_0303(void)
{
    /* Initialize and configure clocks
    *   -   see clock manager component for details
    */
    CLOCK_SYS_Init(g_clockManConfigsArr, FSL_CLOCK_MANAGER_CONFIG_CNT,
    g_clockManCallbacksArr, FSL_CLOCK_MANAGER_CALLBACK_CNT);
    CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_FORCIBLE);
    /* Initialize pins
    *   -   See PinSettings component for more info
    */
    Pins_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);
    
    /* Local variable definition */
    uint32_t Result_baudRate, Result_baudRateHS, Result_TxFIFOWatermark;
    bool Result_GetInt;
    lpi2c_status_t Result_lpi2c_status;
    /* LPI2C_DRV_MasterSendDataBlocking LPI2C BUSY*/ 
    uint8_t txBuff[3] = {'I', '2', 'C'};
    /* LPI2C bytes remaining*/
    uint32_t Result_bytesRemaining;
    
    master.rxBuff = NULL;                     /* Pointer to receive data buffer */
    master.rxSize = 0;                        /* Size of receive data buffer */
    master.txBuff = NULL;                     /* Pointer to transmit data buffer */
    master.txSize = 0;                        /* Size of transmit data buffer */
    master.status = 0;                        /* Status of last driver operation */
    master.i2cIdle = true;                    /* Idle/busy state of the driver */
    master.operatingMode = LPI2C_STANDARD_MODE;
    master.slaveAddress = 123U;               /* Slave address */
    master.is10bitAddr = false;
    master.highSpeedInProgress = false;
    
    lpi2c1_MasterConfig_user.slaveAddress = 123U;
    lpi2c1_MasterConfig_user.is10bitAddr = true;
    lpi2c1_MasterConfig_user.operatingMode = LPI2C_STANDARD_MODE;
    lpi2c1_MasterConfig_user.baudRate = 1200U;
    lpi2c1_MasterConfig_user.baudRateHS = 500000;
    lpi2c1_MasterConfig_user.masterCode = 0U;

    /* Initialize for LPI2C0 */    
    Result_lpi2c_status = LPI2C_DRV_MasterInit(LPI2C_User_Instance, &lpi2c1_MasterConfig_user, &master );
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    /* Reset FIFOs */
    LPI2C_HAL_MasterTxFIFOResetCmd(LPI2C_base_user);
    LPI2C_HAL_MasterRxFIFOResetCmd(LPI2C_base_user);
    /* Check LPI2C_DRV_MasterGetSendStatus before transmit*/
    Result_lpi2c_status = LPI2C_DRV_MasterGetSendStatus(LPI2C_User_Instance, &Result_bytesRemaining);
    EU_ASSERT(Result_bytesRemaining ==  0);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    /* Transmit data by LPI2C_DRV_MasterSendData function*/
    Result_lpi2c_status = LPI2C_DRV_MasterSendData(LPI2C_User_Instance, txBuff, sizeof(txBuff), true);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    /* Check LPI2C_DRV_MasterGetSendStatus after transmit*/
    Result_lpi2c_status = LPI2C_DRV_MasterGetSendStatus(LPI2C_User_Instance, &Result_bytesRemaining);
    EU_ASSERT(Result_bytesRemaining ==  5);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_BUSY);
    /* using LPI2C_DRV_MasterAbortTransferData to send stop command, then master.i2cIdle is true*/
    Result_lpi2c_status = LPI2C_DRV_MasterAbortTransferData(LPI2C_User_Instance);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    /* Check Master send status after Abort transfer data*/
    Result_lpi2c_status = LPI2C_DRV_MasterGetSendStatus(LPI2C_User_Instance, &Result_bytesRemaining);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_ABORTED);
    /* Using LPI2C_DRV_MasterAbortTransferData to send stop command, master.i2cIdle is true so result must return LPI2C_STATUS_NO_TRANSFER*/
    Result_lpi2c_status = LPI2C_DRV_MasterAbortTransferData(LPI2C_User_Instance);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_NO_TRANSFER);
    
    /* Finish test case by DeInitialize */
    LPI2C_DRV_MasterDeinit(LPI2C_User_Instance);
    LPI2C_HAL_Init(LPI2C_base_user);
}

#ifdef __cplusplus
}
#endif

/** @} */
