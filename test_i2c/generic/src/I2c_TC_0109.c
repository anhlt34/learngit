/***************************************************************************//*!
*   Freescale Semiconductor Inc.
*   (c) Copyright 2011 Freescale Semiconductor Inc.
*   ALL RIGHTS RESERVED.
*
*   @file             I2c_TC_0109.c
*   @details          Test case I2c_TC_0109.
*   @addtogroup       fsl_I2C_TESTS
*
*******************************************************************************/

/*==================================================================================================
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Nguyen Van Hieu (B46843)
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "I2c_TC_0109.h"

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id          I2c_TC_0109
* @brief            Check functional verification
* @details          Check function LPI2C_DRV_MasterInit set correctly the slave address for any
*                   subsequent I2C communication.
* @pre              N/A
* @post             N/A
*
* @test_level       ComponentValidation
* @test_type        Functional
* @test_technique   BlackBox
* @test_procedure   Steps:
*                       -#  Initialize for LPI2C module.
*                       -#  Check initialization for LPI2C master slaver address of LPI2C module is correct.
*                       -#  Finish test case by DeInitialize.
* @pass_criteria    LPI2C_DRV_MasterSetSlaveAddr can set slaver address of LPI2C component.
* @requirements     Driver: LPI2C_DRV_001, LPI2C_DRV_009.
* @execution_type   Automated
* @hw_depend        N/A
* @sw_depend        N/A
* @defects          N/A
* @test_priority    High
* @note             N/A
*
* @keywords         N/A
*/


void I2c_TC_0109(void)
{
    /* Local variable definition */
    lpi2c_master_state_t  master = {
        .rxBuff = NULL,                         /* Pointer to receive data buffer */
        .rxSize = 0,                            /* Size of receive data buffer */
        .txBuff = NULL,                         /* Pointer to transmit data buffer */
        .txSize = 0,                            /* Size of transmit data buffer */
        .status = 0,                            /* Status of last driver operation */
        .i2cIdle = true,                        /* Idle/busy state of the driver */
        .slaveAddress = 123U,                   /* Slave address */
        .is10bitAddr = false,
    };
    lpi2c_master_user_config_t lpi2c1_MasterConfig_user = {
        .slaveAddress = 123U,
        .is10bitAddr = true,
        .baudRate = 1200U,
    };
    /* Initialize for LPI2C0 module*/
    lpi2c_status_t lpi2c_status_result = LPI2C_STATUS_FAIL;
    lpi2c_status_result = LPI2C_DRV_MasterInit(LPI2C_User_Instance, &lpi2c1_MasterConfig_user, &master );
    EU_ASSERT(lpi2c_status_result ==  LPI2C_STATUS_SUCCESS);

    /* Check initialization for LPI2C master slaver address of LPI2C module is correct */
    LPI2C_DRV_MasterSetSlaveAddr(LPI2C_User_Instance, 0x100, false);
    EU_ASSERT(master.slaveAddress ==  0x100);
    EU_ASSERT(master.is10bitAddr ==  true);
    
    /* Finish test case by DeInitialize */
    LPI2C_DRV_MasterDeinit(LPI2C_User_Instance);
}

#ifdef __cplusplus
}
#endif

/** @} */
