/***************************************************************************//*!
*   Freescale Semiconductor Inc.
*   (c) Copyright 2011 Freescale Semiconductor Inc.
*   ALL RIGHTS RESERVED.
*
*   @file             I2c_TC_0702.c
*   @details          Test case I2c_TC_0702.
*   @addtogroup       fsl_I2C_TESTS
*
*******************************************************************************/

/*==================================================================================================
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Nguyen Van Hieu (B46843)
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "I2c_TC_0702.h"

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id          I2c_TC_0702
* @brief            Check functional verification
* @details          Check functions LPI2C_DRV_SlaveGetTransmitStatus and LPI2C_DRV_MasterGetReceiveStatus
*                   after slaver transmit and master receive.
* @pre              N/A
* @post             N/A
*
* @test_level       ComponentValidation
* @test_type        Functional
* @test_technique   BlackBox
* @test_procedure   Steps:
*                       -#  Initialize and configure clocks.
*                       -#  Initialize pins.
*                       -#  Initialize LPI2C_Instance_0 is slaver .
*                       -#  Initialize LPI2C_Instance_1 is master.
*                       -#  Change data in slaverBuffer to transmit and clear data in masterBuffer to receive.
*                       -#  slaver send data to Master.
*                       -#  master receive data from Slaver.
*                       -#  Verify all data received is correct.
*                       -#  Check the status of slaver after transmission.
*                       -#  Check the status of master after reception.
*                       -#  Finish test case by DeInitialize.
* @pass_criteria    The LPI2C_DRV_SlaveGetTransmitStatus function can get the slaver transmit status correctly and
*                   the LPI2C_DRV_MasterGetReceiveStatus function can get the master status receive correctly.
* @requirements     Driver:LPI2C_DRV_039, LPI2C_DRV_019. HAL:LPI2C_HAL_259, LPI2C_HAL_219, LPI2C_HAL_004.
* @execution_type   Automated
* @hw_depend        Pins of LPI2C0 module connect to pins of LPI2C1 module.
* @sw_depend        N/A
* @defects          N/A
* @test_priority    High
* @note             N/A
*
* @keywords         N/A
*/

void I2c_TC_0702(void)
{
    /*Initialize and configure clocks*/
    CLOCK_SYS_Init(g_clockManConfigsArr, FSL_CLOCK_MANAGER_CONFIG_CNT,
            g_clockManCallbacksArr, FSL_CLOCK_MANAGER_CALLBACK_CNT);
    CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_FORCIBLE);
    /*Initialize pins*/
    Pins_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);
    /* Local variable definition */
    uint32_t Result_bytesRemaining;
    uint32_t i;
    bool Result_GetInt;
    lpi2c_status_t Result_lpi2c_status;
    /* Declare address of LPI2C_User_Instance */
    LPI2C_Type *reg_lpi2cBase[LPI2C_INSTANCE_COUNT] = LPI2C_BASE_PTRS;
    LPI2C_Type *base_Addr = reg_lpi2cBase[LPI2C_User_Instance];
    /* LPI2C_DRV_MasterSendDataBlocking LPI2C BUSY*/ 
    
    lpi2c1_MasterConfig_user.slaveAddress = 37U,
    lpi2c1_MasterConfig_user.is10bitAddr = false,
    lpi2c1_MasterConfig_user.operatingMode = LPI2C_STANDARD_MODE,
    lpi2c1_MasterConfig_user.baudRate = 2000U,
    lpi2c1_MasterConfig_user.baudRateHS = 0U,
    lpi2c1_MasterConfig_user.masterCode = 0U,
    
    lpi2c2_SlaveConfig_user.slaveAddress = 37U,
    lpi2c2_SlaveConfig_user.is10bitAddr = false,
    lpi2c2_SlaveConfig_user.slaveListening = true,
    lpi2c2_SlaveConfig_user.operatingMode = LPI2C_STANDARD_MODE,
    lpi2c2_SlaveConfig_user.slaveCallback = lpi2c2_Callback0,
    lpi2c2_SlaveConfig_user.callbackParam = NULL,

    /* Initialize LPI2C_Instance_0 is slaver*/
    Result_lpi2c_status = LPI2C_DRV_SlaveInit(LPI2C_Instance_0, &lpi2c2_SlaveConfig_user, &Slave);
    EU_ASSERT(Result_lpi2c_status  ==  LPI2C_STATUS_SUCCESS);
    /* Initialize LPI2C_Instance_1 is master*/
    Result_lpi2c_status = LPI2C_DRV_MasterInit(LPI2C_Instance_1, &lpi2c1_MasterConfig_user, &master );
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    /* Make sure you connected correctly LPI2C_User_Instance pins to  LPI2C_User_Instance pins*/ 
    /**                                  SCL             SDA                    */
    /**                   LPI2C0      PTA3(J58-1)     PTA2(J58-3)               */
    /**                     ||            ||              ||                    */
    /**                   LPI2C1      PTE1(J58-1)     PTE0(J58-6)               */
    /* Change data in masterBuffer to transmit and clear data in slaverBuffer to receive*/
    for (i = 0; i < 16; i++) {
        masterBuffer[i] = 0;
        slaveBuffer[i] = i;
    }    
    /* slaver send data to Master */
    Result_lpi2c_status = LPI2C_DRV_SlaveSendDataBlocking(LPI2C_Instance_0, slaveBuffer, sizeof(slaveBuffer));
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    /* master receive data from Slaver */
    Result_lpi2c_status = LPI2C_DRV_MasterReceiveDataBlocking(LPI2C_Instance_1, masterBuffer, sizeof(masterBuffer),true);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    /* Verify all data received is correct */ 
    Delay_ms(1);
    for (i = 0; i < 16; i++) {
        EU_ASSERT(slaveBuffer[i] ==  masterBuffer[i]);
    }
    /* Check the status of slaver after transmission */
    Result_lpi2c_status = LPI2C_DRV_SlaveGetTransmitStatus(LPI2C_Instance_0,&Result_bytesRemaining);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    EU_ASSERT(Result_bytesRemaining ==  0);
    /* Check the status of master after reception */
    Result_lpi2c_status = LPI2C_DRV_MasterGetReceiveStatus(LPI2C_Instance_1,&Result_bytesRemaining);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    EU_ASSERT(Result_bytesRemaining ==  0);
    /* Finish test case by DeInitialize */
    LPI2C_DRV_SlaveDeinit(LPI2C_Instance_0);
    LPI2C_DRV_MasterDeinit(LPI2C_Instance_1);
}

#ifdef __cplusplus
}
#endif

/** @} */
