/***************************************************************************//*!
*   Freescale Semiconductor Inc.
*   (c) Copyright 2011 Freescale Semiconductor Inc.
*   ALL RIGHTS RESERVED.
*
*   @file             I2c_TC_0105.c
*   @details          Test case 0001.
*   @addtogroup       fsl_I2C_TESTS
*
*******************************************************************************/

/*==================================================================================================
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Nguyen Van Hieu (B46843)
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "I2c_TC_0105.h"

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id          I2c_TC_0105
* @brief            Check functional verification
* @details          Check function LPI2C_DRV_MasterInit shall configure correctly the baud-rate of LPI2C module
* @pre              N/A
* @post             N/A
*
* @test_level       ComponentValidation
* @test_type        Functional
* @test_technique   BlackBox
* @test_procedure   Steps:
*                       -#  Initialize and configure clocks.
*                       -#  Initialize pins.
*                       -#  Initialize for LPI2C.
*                       -#  Check master status after initialize driver status structure.
*                       -#  Check LPI2C baudRate in Standard mode, the baud-rate is 1200Hz.
*                       -#  Check baudRate in Fast mode, the baud-rate is 120000Hz.
*                       -#  Configure FIRC clock source.
*                       -#  Configure LPI2C clock source is FIRC.
*                       -#  Check baudRate in Fast Plus mode, baud-rate is 2MHz.
*                       -#  Check baudRate in High speed mode, the baud-rate is 4MHz.
*                       -#  Check baudRate in Ultra Fast Mode, the baud-rate is 6MHz.
*                       -#  Finish test case by DeInitialize.
* @pass_criteria    All get baud-rate result values is correct.
* @requirements     Driver: LPI2C_DRV_001, LPI2C_DRV_005.
*                   HAL: LPI2C_HAL_018, LPI2C_HAL_123, LPI2C_HAL_152, LPI2C_HAL_156, LPI2C_HAL_160,
*                   LPI2C_HAL_164, LPI2C_HAL_168, LPI2C_HAL_172, LPI2C_HAL_176, LPI2C_HAL_180,
*                   LPI2C_HAL_126, LPI2C_HAL_162, LPI2C_HAL_166, LPI2C_HAL_178, LPI2C_HAL_182.
* @execution_type   Automated
* @hw_depend        N/A
* @sw_depend        N/A
* @defects          N/A
* @test_priority    High
* @note             N/A
*
* @keywords         N/A
*
*/

void I2c_TC_0105(void)
{
    /*Initialize and configure clocks*/
    CLOCK_SYS_Init(g_clockManConfigsArr, FSL_CLOCK_MANAGER_CONFIG_CNT,
            g_clockManCallbacksArr, FSL_CLOCK_MANAGER_CALLBACK_CNT);
    CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_FORCIBLE);
    /*Initialize pins*/
    Pins_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);
    /* Local variable definition */
    const clock_names_t g_lpi2cClock[LPI2C_INSTANCE_COUNT] = {PCC_LPI2C0_CLOCK, PCC_LPI2C1_CLOCK};
    /* Local variable definition */
    uint32_t Result_baudRate,Result_baudRateHS;
    lpi2c_status_t Result_lpi2c_status;
    scg_firc_config_t scg_firc_config_test;
    uint32_t scg_firc_freq;
    scg_firc_config_t scg_firc_config_result;

    scg_spll_config_t scg_spll_config_test;
    uint32_t scg_spll_freq;
    /* Initialize for LPI2C */
    lpi2c_status_t status = LPI2C_STATUS_FAIL;
    status= LPI2C_DRV_MasterInit(LPI2C_User_Instance, &lpi2c1_MasterConfig_user, &master );
    EU_ASSERT(status ==  LPI2C_STATUS_SUCCESS);

    /* Check master status after initialize driver status structure */
    EU_ASSERT(master.cmdQueue.readIdx ==  0U);
    EU_ASSERT(master.cmdQueue.writeIdx ==  0U);
    EU_ASSERT(master.rxBuff ==  NULL);
    EU_ASSERT(master.rxSize ==  0);
    EU_ASSERT(master.txBuff ==  NULL);
    EU_ASSERT(master.txSize ==  0);
    EU_ASSERT(master.status ==  LPI2C_STATUS_SUCCESS);
    EU_ASSERT(master.i2cIdle ==  true);
    EU_ASSERT(master.slaveAddress ==  lpi2c1_MasterConfig_user.slaveAddress);
    EU_ASSERT(master.is10bitAddr ==  lpi2c1_MasterConfig_user.is10bitAddr);

    /* Check LPI2C baudRate in Standard mode, the baud-rate is 1200Hz */
    Result_lpi2c_status = LPI2C_DRV_MasterGetBaudRate(LPI2C_User_Instance, &Result_baudRate, &Result_baudRateHS);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    EU_ASSERT((Result_baudRate >=  (uint32_t)(lpi2c1_MasterConfig_user.baudRate*0.9))&(Result_baudRate <=  (uint32_t)(lpi2c1_MasterConfig_user.baudRate*1.1)));

    /* Check baudRate in Fast mode, the baud-rate is 120000Hz */
    lpi2c1_MasterConfig_user.operatingMode = LPI2C_FAST_MODE;
    lpi2c1_MasterConfig_user.baudRate = 120000U;
    LPI2C_DRV_MasterDeinit(LPI2C_User_Instance);
    status = LPI2C_DRV_MasterInit(LPI2C_User_Instance, &lpi2c1_MasterConfig_user, &master );
    EU_ASSERT(status ==  LPI2C_STATUS_SUCCESS);
    Result_lpi2c_status = LPI2C_DRV_MasterGetBaudRate(LPI2C_User_Instance, &Result_baudRate, &Result_baudRateHS);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    EU_ASSERT((Result_baudRate >=  (uint32_t)(lpi2c1_MasterConfig_user.baudRate*0.9))&(Result_baudRate <=  (uint32_t)(lpi2c1_MasterConfig_user.baudRate*1.1)));
    /*Configure FIRC clock source*/
    scg_firc_config_test.enableInStop     = false;                               /*!< FIRCSTEN   */
    scg_firc_config_test.enableInLowPower = false;                               /*!< FIRCLPEN   */
    scg_firc_config_test.regulator        = true;                                /*!< FIRCREGOFF */
    scg_firc_config_test.locked           = false;                               /*!< LK         */
    scg_firc_config_test.range            = SCG_FIRC_RANGE_52M;                  /*!< RANGE      */
    scg_firc_config_test.div1             = SCG_ASYNC_CLOCK_DIV_BY_1;            /*!< FIRCDIV1   */
    scg_firc_config_test.div2             = SCG_ASYNC_CLOCK_DIV_BY_1;            /*!< FIRCDIV2   */
    scg_firc_config_test.enableTrim       = false;                               /*!< FIRCTREN   */
    scg_firc_config_test.updateTrim       = false;                               /*!< FIRCTRUP   */
    SCG_HAL_DeinitFirc(SCG);
    SCG_HAL_InitFirc(SCG,&scg_firc_config_test);
    scg_firc_freq = SCG_HAL_GetFircFreq(SCG);
    /*Configure LPI2C clock source is FIRC*/
    PCC_HAL_SetClockMode(PCC,g_lpi2cClock[LPI2C_Instance_0],false);
    PCC_HAL_SetClockSourceSel(PCC,g_lpi2cClock[LPI2C_Instance_0],CLK_SRC_FIRC);
    EU_ASSERT(PCC_HAL_GetClockSourceSel(PCC,g_lpi2cClock[LPI2C_Instance_0]) == CLK_SRC_FIRC);
    PCC_HAL_SetClockMode(PCC,g_lpi2cClock[LPI2C_Instance_0],true);
    /* Check baudRate in Fast Plus mode, the baud-rate is 2MHz */
    lpi2c1_MasterConfig_user.operatingMode = LPI2C_FASTPLUS_MODE;
    lpi2c1_MasterConfig_user.baudRate = 2000000U;
    LPI2C_DRV_MasterDeinit(LPI2C_User_Instance);
    status = LPI2C_DRV_MasterInit(LPI2C_User_Instance, &lpi2c1_MasterConfig_user, &master );
    EU_ASSERT(status ==  LPI2C_STATUS_SUCCESS);
    Result_lpi2c_status = LPI2C_DRV_MasterGetBaudRate(LPI2C_User_Instance, &Result_baudRate, &Result_baudRateHS);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    EU_ASSERT((Result_baudRate >=  (uint32_t)(lpi2c1_MasterConfig_user.baudRate*0.9))&(Result_baudRate <=  (uint32_t)(lpi2c1_MasterConfig_user.baudRate*1.1)));
    /* Check baudRate in High speed mode, the baud-rate is 4MHz */
    lpi2c1_MasterConfig_user.operatingMode = LPI2C_HIGHSPEED_MODE;
    lpi2c1_MasterConfig_user.baudRateHS = 4000000U;
    lpi2c1_MasterConfig_user.baudRate = 4000000U;
    LPI2C_DRV_MasterDeinit(LPI2C_User_Instance);
    status = LPI2C_DRV_MasterInit(LPI2C_User_Instance, &lpi2c1_MasterConfig_user, &master );
    EU_ASSERT(status ==  LPI2C_STATUS_SUCCESS);
    Result_lpi2c_status = LPI2C_DRV_MasterGetBaudRate(LPI2C_User_Instance, &Result_baudRate, &Result_baudRateHS);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    EU_ASSERT((Result_baudRate >=  (uint32_t)(lpi2c1_MasterConfig_user.baudRate*0.9))&(Result_baudRate <=  (uint32_t)(lpi2c1_MasterConfig_user.baudRate*1.1)));
    EU_ASSERT((Result_baudRateHS >=  (uint32_t)(lpi2c1_MasterConfig_user.baudRateHS*0.9))&(Result_baudRateHS <=  (uint32_t)(lpi2c1_MasterConfig_user.baudRateHS*1.1)));
   /* Check baudRate in Ultra Fast Mode, the baud-rate is 6MHz */
    lpi2c1_MasterConfig_user.operatingMode = LPI2C_ULTRAFAST_MODE;
    lpi2c1_MasterConfig_user.baudRate = 6000000U;
    LPI2C_DRV_MasterDeinit(LPI2C_User_Instance);
    status = LPI2C_DRV_MasterInit(LPI2C_User_Instance, &lpi2c1_MasterConfig_user, &master );
    EU_ASSERT(status ==  LPI2C_STATUS_SUCCESS);
    Result_lpi2c_status = LPI2C_DRV_MasterGetBaudRate(LPI2C_User_Instance, &Result_baudRate, &Result_baudRateHS);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    EU_ASSERT((Result_baudRate >=  (uint32_t)(lpi2c1_MasterConfig_user.baudRate*0.9))&(Result_baudRate <=  (uint32_t)(lpi2c1_MasterConfig_user.baudRate*1.1)));

    /* Finish test case by DeInitialize */
    LPI2C_DRV_MasterDeinit(LPI2C_User_Instance);
}

#ifdef __cplusplus
}
#endif

/** @} */
