/***************************************************************************//*!
*   Freescale Semiconductor Inc.
*   (c) Copyright 2011 Freescale Semiconductor Inc.
*   ALL RIGHTS RESERVED.
*
*   @file             I2c_TC_0102.c
*   @details          Test case I2c_TC_0102.
*   @addtogroup       fsl_I2C_TESTS
*
*******************************************************************************/

/*==================================================================================================
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Nguyen Van Hieu (B46843)
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "I2c_TC_0102.h"

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id          I2c_TC_0102
* @brief            Check functional verification
* @details          Check clock source for all device of LPI2C module
* @pre              N/A
* @post             N/A
*
* @test_level       ComponentValidation
* @test_type        Functional
* @test_technique   BlackBox
* @test_procedure   Steps:
*                       -#  Initialize and configure clocks.
*                       -#  Initialize pins.
*                       -#  Verify that clock gate of all I2C device is opened.
*                       -#  Verify that clock of LPI2C module is SOSC.
* @pass_criteria    LPI2C module has opened Clock gate and clock source is SOSC.
* @requirements     N/A
* @execution_type   Automated
* @hw_depend        N/A
* @sw_depend        N/A
* @defects          N/A
* @test_priority    High
* @note             N/A
*
* @keywords         N/A
*
*/

void I2c_TC_0102(void)
{
    /*Initialize and configure clocks*/
    CLOCK_SYS_Init(g_clockManConfigsArr, FSL_CLOCK_MANAGER_CONFIG_CNT,
            g_clockManCallbacksArr, FSL_CLOCK_MANAGER_CALLBACK_CNT);
    CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_FORCIBLE);
    /*Initialize pins*/
    Pins_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);
    /* Local variable definition */
    const clock_names_t g_lpi2cClock[LPI2C_INSTANCE_COUNT] = {PCC_LPI2C0_CLOCK, PCC_LPI2C1_CLOCK};
    bool reGetClock;
    peripheral_clock_source_t peripheral_clock_source_test;
    /* Verify that clock gate of all I2C device is opened*/
    reGetClock = PCC_HAL_GetClockMode(PCC,g_lpi2cClock[LPI2C_Instance_0]);
    EU_ASSERT(reGetClock == true);
    reGetClock = PCC_HAL_GetClockMode(PCC,g_lpi2cClock[LPI2C_Instance_1]);
    EU_ASSERT(reGetClock == true);
    /* Verify that clock of LPI2C module is SOSC*/
    peripheral_clock_source_test = PCC_HAL_GetClockSourceSel(PCC,g_lpi2cClock[LPI2C_Instance_0]);
    EU_ASSERT(peripheral_clock_source_test == CLK_SRC_SOSC);
    peripheral_clock_source_test = PCC_HAL_GetClockSourceSel(PCC,g_lpi2cClock[LPI2C_Instance_1]);
    EU_ASSERT(peripheral_clock_source_test == CLK_SRC_SOSC);
}

#ifdef __cplusplus
}
#endif

/** @} */
