/***************************************************************************//*!
*   Freescale Semiconductor Inc.
*   (c) Copyright 2011 Freescale Semiconductor Inc.
*   ALL RIGHTS RESERVED.
*
*   @file             I2c_TC_0801.c
*   @details          Test case I2c_TC_0801.
*   @addtogroup       fsl_I2C_TESTS
*
*******************************************************************************/

/*==================================================================================================
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Nguyen Van Hieu (B46843)
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "I2c_TC_0801.h"

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id          I2c_TC_0701
* @brief            Check functional verification
* @details          Check function LPI2C_DRV_MasterSendDataBlocking can communicate with
*                   PICkit SERIAL I2C DEMO BOARD.
* @pre              N/A
* @post             N/A
*
* @test_level       ComponentValidation
* @test_type        Functional
* @test_technique   BlackBox
* @test_procedure   Steps:
*                   -#  Configure I2C Master operate in Standard mode, Baud-rate is 100000(Hz), Slave address(7 bit) is 0x20.
*                   -#  Connect J58: 1-2  ;   4-5
*                   -#  Connect J35
*                   -#  Connect J56 to PICkit SERIAL I2C DEMO BOARD
*                   -#  Run code and check LEDs status: They must blink 10 time.
*                   -#  Initialize clocks.
*                   -#  Initialize pins.
*                   -#  Initialize LPI2C module.
*                   -#  Configure IO DIR for MCP23008.
*                   -#  Configure IO GPIO for MCP23008.
*                   -#  Configure IO OLAT for MCP23008.
* @pass_criteria    Check communication of S32K144 to PICkit SERIAL I2C DEMO BOARD( The LEDs must bink 10 time
* @requirements     Driver:LPI2C_DRV_011. HAL: LPI2C_HAL_188, LPI2C_HAL_072, LPI2C_HAL_196.
* @execution_type   Automated
* @hw_depend        Connection between S32K144 board and PICkit SERIAL I2C DEMO BOARD must be correctly.
* @sw_depend        N/A
* @defects          N/A
* @test_priority    High
* @note             N/A
*
* @keywords         N/A
*/

void I2c_TC_0801(void)
{
    /*Initialize clocks*/
    CLOCK_SYS_Init(g_clockManConfigsArr, FSL_CLOCK_MANAGER_CONFIG_CNT,
            g_clockManCallbacksArr, FSL_CLOCK_MANAGER_CALLBACK_CNT);
    CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_FORCIBLE);
    /*Initialize pins*/
    Pins_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);
    /* Local variable definition */
    lpi2c_master_state_t lpi2c0MasterState;
    lpi2c_slave_state_t lpi2c1SlaveState;
    lpi2c_status_t Result_lpi2c_status;
    lpi2c_nack_config_t lpi2c_nack_config_status;
    uint8_t i, dataWrite;

    /* Change master to Standard Mode, baud-rate is 100k bps*/
    lpi2c1_MasterConfig_user.slaveAddress = 0x20U,
    lpi2c1_MasterConfig_user.is10bitAddr = false,
    lpi2c1_MasterConfig_user.operatingMode = LPI2C_STANDARD_MODE,
    lpi2c1_MasterConfig_user.baudRate = 100000U,
    lpi2c1_MasterConfig_user.baudRateHS = 0U,
    lpi2c1_MasterConfig_user.masterCode = 0U,
    /* Initialize LPI2C module */
    Result_lpi2c_status = LPI2C_DRV_MasterInit(LPI2C_User_Instance, &lpi2c1_MasterConfig_user, &master );
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    /* Configure IO DIR for MCP23008 */
    MCP23008_Write(MCP_REG_IODIR, 0x00);
    /* Configure IO GPIO for MCP23008 */
    dataWrite = 0xF0;

    /* clear masterBuffer */
    for (i = 0; i < 16; i++) {
        masterBuffer[i] = 0;
    }

    for(i = 0; i < 10; i++)
    {
        MCP23008_Write(MCP_REG_GPIO, dataWrite);
        Delay_ms(1000);
        /* Read data from slave */
        masterBuffer[i] = MCP23008_Read(MCP_REG_OLAT);
        EU_ASSERT(masterBuffer[i] == dataWrite);
        dataWrite = ~dataWrite;
    }
}

#ifdef __cplusplus
}
#endif

/** @} */
