#include "I2c_Common.h"


uint8_t masterBuffer[16];
uint8_t slaveBuffer[16];

lpi2c_master_state_t master = {
    .rxBuff = NULL,                     /* Pointer to receive data buffer */
    .rxSize = 0,                        /* Size of receive data buffer */
    .txBuff = NULL,                     /* Pointer to transmit data buffer */
    .txSize = 0,                        /* Size of transmit data buffer */
    .status = 0,                        /* Status of last driver operation */
    .i2cIdle = true,                    /* Idle/busy state of the driver */
    .operatingMode = LPI2C_STANDARD_MODE,
    .slaveAddress = 123U,               /* Slave address */
    .is10bitAddr = false,
    .highSpeedInProgress = false,
};

lpi2c_master_user_config_t lpi2c1_MasterConfig_user = {
    .slaveAddress = 123U,
    .is10bitAddr = true,
    .operatingMode = LPI2C_STANDARD_MODE,
    .baudRate = 1200U,
    .baudRateHS = 500000,
    .masterCode = 0U,
};

lpi2c_slave_state_t Slave = {
    .status = LPI2C_STATUS_FAIL,            /* The I2C slave status */
    .txBusy = false,                        /* Transmitter is busy */
    .rxBusy = false,                        /* Receiver is busy */
    .txSize = 0,                            /* Size of the TX buffer */
    .rxSize = 0,                            /* Size of the RX buffer */
    .txBuff = NULL,                         /* Pointer to TX Buffer */
    .rxBuff = NULL,                         /* Pointer to RX Buffer */
    .operatingMode = LPI2C_STANDARD_MODE,   /* I2C Operating mode */
};

lpi2c_slave_user_config_t lpi2c2_SlaveConfig_user = {
  .slaveAddress = 37U,
  .is10bitAddr = false,
  .slaveListening = true,
  .operatingMode = LPI2C_STANDARD_MODE,
  .slaveCallback = lpi2c2_Callback0,
  .callbackParam = NULL,
};

void lpi2c2_Callback0(uint8_t instance, lpi2c_slave_event_t slaveEvent, void *userData) {
    /* Check the event type:
    *   - set RX or TX buffers depending on the master request type
    */
    if (slaveEvent == LPI2C_SLAVE_EVENT_RX_REQ)
    LPI2C_DRV_SlaveSetRxBuffer(instance, slaveBuffer, 16);
    if (slaveEvent == LPI2C_SLAVE_EVENT_TX_REQ)
    LPI2C_DRV_SlaveSetTxBuffer(instance, slaveBuffer, 16);
}

void Compare_InitConverterStruct(adc_converter_config_t* config)
{
   EU_ASSERT(config->clockDivide == ADC_CLK_DIVIDE_1);
   EU_ASSERT(config->sampleTime == 0x0CU);
   EU_ASSERT(config->resolution == ADC_RESOLUTION_8BIT);
   EU_ASSERT(config->inputClock == ADC_CLK_ALT_1);
   EU_ASSERT(config->trigger == 0);
   EU_ASSERT(config->dmaEnable == false);
   EU_ASSERT(config->voltageRef == ADC_VOLTAGEREF_VREF);
   EU_ASSERT(config->continuousConvEnable == false);
}

void Delay_ms(uint32_t Time)
{
    uint32_t Time2=1000;
    while(Time--)
    {
        Time2 = 1000;
        while(Time2--);
    }
}

void MCP23008_Write(uint8_t Address_Reg, uint8_t Value)
{
    /* Local variables used for data transceiver */
    uint8_t Master_transmit[2];
    lpi2c_status_t lpi2c_status_result;
    /* Set up sequence to transmit */
    Master_transmit[0] = Address_Reg;
    Master_transmit[1] = Value;
    lpi2c_status_result = LPI2C_DRV_MasterSendDataBlocking(LPI2C_User_Instance, Master_transmit, 2, true);
    EU_ASSERT(lpi2c_status_result == LPI2C_STATUS_SUCCESS);
}

uint8_t MCP23008_Read(uint8_t Address_Reg)
{
    /* Local variables used for data transceiver */
    uint8_t Master_Receive[1];
    lpi2c_status_t lpi2c_status_result;
    /* Set up sequence to receiver */
    Master_Receive[0] = Address_Reg;
    lpi2c_status_result = LPI2C_DRV_MasterReceiveDataBlocking(LPI2C_User_Instance, Master_Receive, 1, true);
    EU_ASSERT(lpi2c_status_result == LPI2C_STATUS_SUCCESS);
    return Master_Receive[0];
}
