/***************************************************************************//*!
*   Freescale Semiconductor Inc.
*   (c) Copyright 2011 Freescale Semiconductor Inc.
*   ALL RIGHTS RESERVED.
*
*   @file             I2c_TC_0204.c
*   @details          Test case I2c_TC_0204.
*   @addtogroup       fsl_I2C_TESTS
*
*******************************************************************************/

/*==================================================================================================
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Nguyen Van Hieu (B46843)
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "I2c_TC_0204.h"

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id          I2c_TC_0204
* @brief            Check functional verification
* @details          Check function LPI2C_DRV_SlaveInit shall enable all interrupt flags.
* @pre              N/A
* @post             N/A
*
* @test_level       ComponentValidation
* @test_type        Functional
* @test_technique   BlackBox
* @test_procedure   Steps:
*                       -#  Initialize and configure clocks.
*                       -#  Initialize pins.
*                       -#  Initialize for LPI2C module.
*                       -#  Check all interrupt flags were enable.
*                       -#  Finish test case by DeInitialize.
* @pass_criteria    All interrupt flags are enable.
* @requirements     Driver: LPI2C_DRV_027. HAL: LPI2C_HAL_261, LPI2C_HAL_259.
* @execution_type   Automated
* @hw_depend        N/A
* @sw_depend        N/A
* @defects          N/A
* @test_priority    High
* @note             N/A
*
* @keywords         N/A
*
*/
void I2c_TC_0204(void)
{
    /*Initialize and configure clocks*/
    CLOCK_SYS_Init(g_clockManConfigsArr, FSL_CLOCK_MANAGER_CONFIG_CNT,
            g_clockManCallbacksArr, FSL_CLOCK_MANAGER_CALLBACK_CNT);
    CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_FORCIBLE);
    /*Initialize pins*/
    Pins_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);
    /* Local variable definition */
    uint32_t Result_baudRate,Result_baudRateHS;
    lpi2c_status_t Result_lpi2c_status;
    /* Initialize for LPI2C module */
    Result_lpi2c_status = LPI2C_DRV_SlaveInit(LPI2C_User_Instance, &lpi2c2_SlaveConfig_user, &Slave);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    
    /* Check all interrupt flags were enable */
    EU_ASSERT(LPI2C_HAL_SlaveGetInt(LPI2C_base_user, LPI2C_HAL_SLAVE_BIT_ERROR_INT)         ==  true);
    EU_ASSERT(LPI2C_HAL_SlaveGetInt(LPI2C_base_user, LPI2C_HAL_SLAVE_FIFO_ERROR_INT)        ==  true);
    EU_ASSERT(LPI2C_HAL_SlaveGetInt(LPI2C_base_user, LPI2C_HAL_SLAVE_STOP_DETECT_INT)       ==  true);
    EU_ASSERT(LPI2C_HAL_SlaveGetInt(LPI2C_base_user, LPI2C_HAL_SLAVE_REPEATED_START_INT)    ==  true);
    EU_ASSERT(LPI2C_HAL_SlaveGetInt(LPI2C_base_user, LPI2C_HAL_SLAVE_ADDRESS_VALID_INT)     ==  true);
    EU_ASSERT(LPI2C_HAL_SlaveGetInt(LPI2C_base_user, LPI2C_HAL_SLAVE_RECEIVE_DATA_INT)      ==  true);
    EU_ASSERT(LPI2C_HAL_SlaveGetInt(LPI2C_base_user, LPI2C_HAL_SLAVE_TRANSMIT_DATA_INT)     ==  true);
    
    /* Finish test case by DeInitialize */
    LPI2C_DRV_SlaveDeinit(LPI2C_User_Instance);
}
#ifdef __cplusplus
}
#endif

/** @} */
