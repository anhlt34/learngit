/***************************************************************************//*!
*   Freescale Semiconductor Inc.
*   (c) Copyright 2011 Freescale Semiconductor Inc.
*   ALL RIGHTS RESERVED.
*
*   @file             I2c_TC_0106.c
*   @details          Test case 0001.
*   @addtogroup       fsl_I2C_TESTS
*
*******************************************************************************/

/*==================================================================================================
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Nguyen Van Hieu (B46843)
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "I2c_TC_0106.h"

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id          I2c_TC_0106
* @brief            Test functional
* @details          Check function LPI2C_DRV_MasterInit shall enable interrupt.
* @pre              N/A
* @post             N/A
*
* @test_level       ComponentValidation
* @test_type        Functional
* @test_technique   BlackBox
* @test_procedure   Steps:
*                       -#Initialize and configure clocks.
*                       -#Initialize pins.
*                       -#Initialize LPI2C module.
*                       -#Check Enable interrupt after initialize LPI2C module.
*                       -#Finish test case by DeInitialize.
* @pass_criteria    All interrupt flags related to mater mode of LPI2C module are enable.
* @requirements     Driver: LPI2C_DRV_001.
* @execution_type   Automated
* @hw_depend        N/A
* @sw_depend        N/A
* @defects          N/A
* @test_priority    High
* @note             N/A
*
* @keywords         N/A
*
*/

void I2c_TC_0106(void)
{
    /*Initialize and configure clocks*/
    CLOCK_SYS_Init(g_clockManConfigsArr, FSL_CLOCK_MANAGER_CONFIG_CNT,
            g_clockManCallbacksArr, FSL_CLOCK_MANAGER_CALLBACK_CNT);
    CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_FORCIBLE);
    /*Initialize pins*/
    Pins_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);
    /* Local variable definition */
    const IRQn_Type g_lpi2cIrqId[LPI2C_INSTANCE_COUNT] = {LPI2C0_IRQn, LPI2C1_IRQn};
    
    bool Result_interrupt;
    lpi2c_status_t Result_lpi2c_status;
    /* Initialize LPI2C module*/
    lpi2c_status_t status = LPI2C_STATUS_FAIL;
    status= LPI2C_DRV_MasterInit(LPI2C_User_Instance, &lpi2c1_MasterConfig_user, &master );
    EU_ASSERT(status ==  LPI2C_STATUS_SUCCESS);

    /* Check Enable interrupt after initialize LPI2C module */
    Result_interrupt = (bool)((FSL_NVIC->ISER[(uint32_t)(g_lpi2cIrqId[LPI2C_User_Instance]) >> 5U] >> (uint32_t)(g_lpi2cIrqId[LPI2C_User_Instance])) & 1U);
    EU_ASSERT(Result_interrupt == true);
    /* Finish test case by DeInitialize */
    LPI2C_DRV_MasterDeinit(LPI2C_User_Instance);
}

#ifdef __cplusplus
}
#endif

/** @} */
