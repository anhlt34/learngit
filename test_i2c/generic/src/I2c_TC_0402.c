/***************************************************************************//*!
*   Freescale Semiconductor Inc.
*   (c) Copyright 2011 Freescale Semiconductor Inc.
*   ALL RIGHTS RESERVED.
*
*   @file             I2c_TC_0402.c
*   @details          Test case 0001.
*   @addtogroup       fsl_I2C_TESTS
*
*******************************************************************************/

/*==================================================================================================
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Nguyen Van Hieu (B46843)
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "I2c_TC_0402.h"

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id          I2c_TC_0402
* @brief            Check functional verification
* @details          Check functions LPI2C_DRV_SlaveSendDataBlocking shall the number of data bytes.
* @pre              N/A
* @post             N/A
*
* @test_level       ComponentValidation
* @test_type        Functional
* @test_technique   BlackBox
* @test_procedure   Steps:
*                       -#  Initialize and configure clocks.
*                       -#  Initialize pins.
*                       -#  Slaver try to send data before initialize LPI2C_User_Instance.
*                       -#  Initialize for LPI2C_User_Instance.
*                       -#  Check LPI2C_DRV_SlaveGetTransmitStatus before transmit.
*                       -#  Slaver send data after initialize LPI2C_User_Instance.
*                       -#  Finish test case by DeInitialize.
* @pass_criteria    The number of data bytes is 0 before transceiver and after is 0..
* @requirements     Driver: LPI2C_DRV_039, LPI2C_DRV_027. HAL: LPI2C_HAL_259, LPI2C_HAL_219.
* @execution_type   Automated
* @hw_depend        N/A
* @sw_depend        N/A
* @defects          N/A
* @test_priority    High
* @note             N/A
*
* @keywords         N/A
*
*/

void I2c_TC_0402(void)
{
    /*Initialize and configure clocks*/
    CLOCK_SYS_Init(g_clockManConfigsArr, FSL_CLOCK_MANAGER_CONFIG_CNT,
            g_clockManCallbacksArr, FSL_CLOCK_MANAGER_CALLBACK_CNT);
    CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_FORCIBLE);
    /*Initialize pins*/
    Pins_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);
    /* Local variable definition */
    
    uint32_t Result_bytesRemaining;
    lpi2c_status_t Result_lpi2c_status;
    /* LPI2C_DRV_MasterSendDataBlocking LPI2C BUSY*/ 
    uint8_t txBuff[3] = {'I', '2', 'C'};

    lpi2c2_SlaveConfig_user.slaveAddress = 123U,
    lpi2c2_SlaveConfig_user.is10bitAddr = true,
    lpi2c2_SlaveConfig_user.slaveListening = true,
    lpi2c2_SlaveConfig_user.operatingMode = LPI2C_STANDARD_MODE,
    lpi2c2_SlaveConfig_user.slaveCallback = lpi2c2_Callback0,
    lpi2c2_SlaveConfig_user.callbackParam = NULL,
    
    /* Slaver try to send data before initialize LPI2C_User_Instance */ 
    Result_lpi2c_status = LPI2C_DRV_SlaveSendDataBlocking(LPI2C_User_Instance, txBuff, sizeof(txBuff));
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_BUSY);
    
    /* Initialize for LPI2C_User_Instance */
    Result_lpi2c_status = LPI2C_DRV_SlaveInit(LPI2C_User_Instance, &lpi2c2_SlaveConfig_user, &Slave);
    EU_ASSERT(Result_lpi2c_status  ==  LPI2C_STATUS_SUCCESS);
    
    /* Check LPI2C_DRV_SlaveGetTransmitStatus before transmit*/
    Result_lpi2c_status = LPI2C_DRV_SlaveGetTransmitStatus(LPI2C_User_Instance, &Result_bytesRemaining);
    EU_ASSERT(Result_bytesRemaining ==  0);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    
    /* Slaver send data after initialize LPI2C_User_Instance */ 
    Result_lpi2c_status = LPI2C_DRV_SlaveSendDataBlocking(LPI2C_User_Instance, txBuff, sizeof(txBuff));
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    
    /* Check LPI2C_DRV_SlaveGetTransmitStatus before transmit*/
    Result_lpi2c_status = LPI2C_DRV_SlaveGetTransmitStatus(LPI2C_User_Instance, &Result_bytesRemaining);
    EU_ASSERT(Result_bytesRemaining ==  0);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);

    /* Finish test case by DeInitialize */
    LPI2C_DRV_SlaveDeinit(LPI2C_User_Instance);
}

#ifdef __cplusplus
}
#endif

/** @} */
