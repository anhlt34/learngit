/***************************************************************************//*!
*   Freescale Semiconductor Inc.
*   (c) Copyright 2011 Freescale Semiconductor Inc.
*   ALL RIGHTS RESERVED.
*
*   @file             I2c_TC_0203.c
*   @details          Test case 0001.
*   @addtogroup       fsl_I2C_TESTS
*
*******************************************************************************/

/*==================================================================================================
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Nguyen Van Hieu (B46843)
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "I2c_TC_0203.h"

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id          I2c_TC_0203
* @brief            Check functional verification
* @details          Check function LPI2C_DRV_SlaveInit shall configure correctly the signals of LPI2C module.
* @pre              N/A
* @post             N/A
*
* @test_level       ComponentValidation
* @test_type        Functional
* @test_technique   BlackBox
* @test_procedure   Steps:
*                       -#  Initialize and configure clocks.
*                       -#  Initialize pins.
*                       -#  Initialize LPI2C module with Standard-mode.
*                       -#  Check slave configurations following: IgnoreNACK, TransmitNACK, ACKStall, TXDStall, RXStall, AddrStall, HighSpeedModeDetect.
*                       -#  Deinitialize LPI2C module.
*                       -#  Initialize LPI2C module with Fast-mode.
*                       -#  Check slave configurations following: IgnoreNACK, TransmitNACK, ACKStall, TXDStall, RXStall, AddrStall, HighSpeedModeDetect.
*                       -#  Deinitialize LPI2C module.
*                       -#  Initialize LPI2C module with Fast-mode Plus.
*                       -#  Check slave configurations following: IgnoreNACK, TransmitNACK, ACKStall, TXDStall, RXStall, AddrStall, HighSpeedModeDetect.
*                       -#  Deinitialize LPI2C module.
*                       -#  Initialize LPI2C module with High-speed Mode.
*                       -#  Check slave configurations following: IgnoreNACK, TransmitNACK, ACKStall, TXDStall, RXStall, AddrStall, HighSpeedModeDetect.
*                       -#  Deinitialize LPI2C module.
*                       -#  Initialize LPI2C module with Ultra Fast Mode.
*                       -#  Check slave configurations following: IgnoreNACK, TransmitNACK, ACKStall, TXDStall, RXStall, AddrStall, HighSpeedModeDetect.
*                       -#  Deinitialize LPI2C module.
* @pass_criteria    All signals are configured correctly.
* @requirements     Driver: LPI2C_DRV_027. HAL:LPI2C_HAL_287, LPI2C_HAL_284, LPI2C_HAL_355, LPI2C_HAL_352, LPI2C_HAL_309, LPI2C_HAL_307, LPI2C_HAL_313, LPI2C_HAL_311, LPI2C_HAL_317, LPI2C_HAL_315, LPI2C_HAL_321, LPI2C_HAL_319, LPI2C_HAL_282, LPI2C_HAL_280.
* @execution_type   Automated
* @hw_depend        N/A
* @sw_depend        N/A
* @defects          N/A
* @test_priority    High
* @note             N/A
*
* @keywords         N/A
*
*/

void I2c_TC_0203(void)
{
    /*Initialize and configure clocks*/
    CLOCK_SYS_Init(g_clockManConfigsArr, FSL_CLOCK_MANAGER_CONFIG_CNT,
            g_clockManCallbacksArr, FSL_CLOCK_MANAGER_CALLBACK_CNT);
    CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_FORCIBLE);
    /*Initialize pins*/
    Pins_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);
    /* Local variable definition */
    uint32_t                    Result_baudRate,Result_baudRateHS;
    lpi2c_status_t              Result_lpi2c_status;
    lpi2c_slave_nack_config_t   lpi2c_slave_nack_config_result;
    lpi2c_slave_nack_transmit_t lpi2c_slave_nack_transmit_result;

    /* Initialize LPI2C module with Standard-mode*/
    lpi2c2_SlaveConfig_user.slaveAddress        = 37U;
    lpi2c2_SlaveConfig_user.is10bitAddr         = false;
    lpi2c2_SlaveConfig_user.slaveListening      = true;
    lpi2c2_SlaveConfig_user.operatingMode       = LPI2C_STANDARD_MODE;
    lpi2c2_SlaveConfig_user.slaveCallback       = lpi2c2_Callback0;
    lpi2c2_SlaveConfig_user.callbackParam       = NULL;
    Result_lpi2c_status = LPI2C_DRV_SlaveInit(LPI2C_User_Instance, &lpi2c2_SlaveConfig_user, &Slave);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    /*Check slave configurations following: IgnoreNACK, TransmitNACK, ACKStall, TXDStall, RXStall, AddrStall, HighSpeedModeDetect.*/
    EU_ASSERT(LPI2C_HAL_SlaveGetIgnoreNACK(LPI2C_base_user)     == LPI2C_SLAVE_NACK_END_TRANSFER);
    EU_ASSERT(LPI2C_HAL_SlaveGetTransmitNACK(LPI2C_base_user)   == LPI2C_SLAVE_TRANSMIT_ACK);
    EU_ASSERT(LPI2C_HAL_SlaveGetACKStall(LPI2C_base_user)       == false);
    EU_ASSERT(LPI2C_HAL_SlaveGetTXDStall(LPI2C_base_user)       == true);
    EU_ASSERT(LPI2C_HAL_SlaveGetRXStall(LPI2C_base_user)        == true);
    EU_ASSERT(LPI2C_HAL_SlaveGetAddrStall(LPI2C_base_user)      == true);
    EU_ASSERT(LPI2C_HAL_SlaveGetHighSpeedModeDetect(LPI2C_base_user) == false);
    /*Deinitialize LPI2C module*/
    LPI2C_DRV_SlaveDeinit(LPI2C_User_Instance);

    /* Initialize LPI2C module with Fast-mode*/
    lpi2c2_SlaveConfig_user.slaveAddress        = 37U;
    lpi2c2_SlaveConfig_user.is10bitAddr         = false;
    lpi2c2_SlaveConfig_user.slaveListening      = true;
    lpi2c2_SlaveConfig_user.operatingMode       = LPI2C_FAST_MODE;
    lpi2c2_SlaveConfig_user.slaveCallback       = lpi2c2_Callback0;
    lpi2c2_SlaveConfig_user.callbackParam       = NULL;
    Result_lpi2c_status = LPI2C_DRV_SlaveInit(LPI2C_User_Instance, &lpi2c2_SlaveConfig_user, &Slave);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    /*Check slave configurations following: IgnoreNACK, TransmitNACK, ACKStall, TXDStall, RXStall, AddrStall, HighSpeedModeDetect.*/
    EU_ASSERT(LPI2C_HAL_SlaveGetIgnoreNACK(LPI2C_base_user)     == LPI2C_SLAVE_NACK_END_TRANSFER);
    EU_ASSERT(LPI2C_HAL_SlaveGetTransmitNACK(LPI2C_base_user)   == LPI2C_SLAVE_TRANSMIT_ACK);
    EU_ASSERT(LPI2C_HAL_SlaveGetACKStall(LPI2C_base_user)       == false);
    EU_ASSERT(LPI2C_HAL_SlaveGetTXDStall(LPI2C_base_user)       == true);
    EU_ASSERT(LPI2C_HAL_SlaveGetRXStall(LPI2C_base_user)        == true);
    EU_ASSERT(LPI2C_HAL_SlaveGetAddrStall(LPI2C_base_user)      == true);
    EU_ASSERT(LPI2C_HAL_SlaveGetHighSpeedModeDetect(LPI2C_base_user) == false);
    /*Deinitialize LPI2C module*/
    LPI2C_DRV_SlaveDeinit(LPI2C_User_Instance);
    
    /* Initialize LPI2C module with Fast-mode Plus*/
    lpi2c2_SlaveConfig_user.slaveAddress        = 37U;
    lpi2c2_SlaveConfig_user.is10bitAddr         = false;
    lpi2c2_SlaveConfig_user.slaveListening      = true;
    lpi2c2_SlaveConfig_user.operatingMode       = LPI2C_FASTPLUS_MODE;
    lpi2c2_SlaveConfig_user.slaveCallback       = lpi2c2_Callback0;
    lpi2c2_SlaveConfig_user.callbackParam       = NULL;
    Result_lpi2c_status = LPI2C_DRV_SlaveInit(LPI2C_User_Instance, &lpi2c2_SlaveConfig_user, &Slave);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    /*Check slave configurations following: IgnoreNACK, TransmitNACK, ACKStall, TXDStall, RXStall, AddrStall, HighSpeedModeDetect.*/
    EU_ASSERT(LPI2C_HAL_SlaveGetIgnoreNACK(LPI2C_base_user)     == LPI2C_SLAVE_NACK_END_TRANSFER);
    EU_ASSERT(LPI2C_HAL_SlaveGetTransmitNACK(LPI2C_base_user)   == LPI2C_SLAVE_TRANSMIT_ACK);
    EU_ASSERT(LPI2C_HAL_SlaveGetACKStall(LPI2C_base_user)       == false);
    EU_ASSERT(LPI2C_HAL_SlaveGetTXDStall(LPI2C_base_user)       == true);
    EU_ASSERT(LPI2C_HAL_SlaveGetRXStall(LPI2C_base_user)        == true);
    EU_ASSERT(LPI2C_HAL_SlaveGetAddrStall(LPI2C_base_user)      == true);
    EU_ASSERT(LPI2C_HAL_SlaveGetHighSpeedModeDetect(LPI2C_base_user) == false);
    /*Deinitialize LPI2C module*/
    LPI2C_DRV_SlaveDeinit(LPI2C_User_Instance);

    /* Initialize LPI2C module with High-speed Mode*/
    lpi2c2_SlaveConfig_user.slaveAddress        = 37U;
    lpi2c2_SlaveConfig_user.is10bitAddr         = false;
    lpi2c2_SlaveConfig_user.slaveListening      = true;
    lpi2c2_SlaveConfig_user.operatingMode       = LPI2C_HIGHSPEED_MODE;
    lpi2c2_SlaveConfig_user.slaveCallback       = lpi2c2_Callback0;
    lpi2c2_SlaveConfig_user.callbackParam       = NULL;
    Result_lpi2c_status = LPI2C_DRV_SlaveInit(LPI2C_User_Instance, &lpi2c2_SlaveConfig_user, &Slave);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    /*Check slave configurations following: IgnoreNACK, TransmitNACK, ACKStall, TXDStall, RXStall, AddrStall, HighSpeedModeDetect.*/
    EU_ASSERT(LPI2C_HAL_SlaveGetIgnoreNACK(LPI2C_base_user)     == LPI2C_SLAVE_NACK_END_TRANSFER);
    EU_ASSERT(LPI2C_HAL_SlaveGetTransmitNACK(LPI2C_base_user)   == LPI2C_SLAVE_TRANSMIT_ACK);
    EU_ASSERT(LPI2C_HAL_SlaveGetACKStall(LPI2C_base_user)       == false);
    EU_ASSERT(LPI2C_HAL_SlaveGetTXDStall(LPI2C_base_user)       == true);
    EU_ASSERT(LPI2C_HAL_SlaveGetRXStall(LPI2C_base_user)        == true);
    EU_ASSERT(LPI2C_HAL_SlaveGetAddrStall(LPI2C_base_user)      == true);
    EU_ASSERT(LPI2C_HAL_SlaveGetHighSpeedModeDetect(LPI2C_base_user) == true);
    /*Deinitialize LPI2C module*/
    LPI2C_DRV_SlaveDeinit(LPI2C_User_Instance);

    /* Initialize LPI2C module with Ultra Fast Mode*/
    lpi2c2_SlaveConfig_user.slaveAddress        = 37U;
    lpi2c2_SlaveConfig_user.is10bitAddr         = false;
    lpi2c2_SlaveConfig_user.slaveListening      = true;
    lpi2c2_SlaveConfig_user.operatingMode       = LPI2C_ULTRAFAST_MODE;
    lpi2c2_SlaveConfig_user.slaveCallback       = lpi2c2_Callback0;
    lpi2c2_SlaveConfig_user.callbackParam       = NULL;
    Result_lpi2c_status = LPI2C_DRV_SlaveInit(LPI2C_User_Instance, &lpi2c2_SlaveConfig_user, &Slave);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);
    /*Check slave configurations following: IgnoreNACK, TransmitNACK, ACKStall, TXDStall, RXStall, AddrStall, HighSpeedModeDetect.*/
    EU_ASSERT(LPI2C_HAL_SlaveGetIgnoreNACK(LPI2C_base_user)     == LPI2C_SLAVE_NACK_CONTINUE_TRANSFER);
    EU_ASSERT(LPI2C_HAL_SlaveGetTransmitNACK(LPI2C_base_user)   == LPI2C_SLAVE_TRANSMIT_NACK);
    EU_ASSERT(LPI2C_HAL_SlaveGetACKStall(LPI2C_base_user)       == false);
    EU_ASSERT(LPI2C_HAL_SlaveGetTXDStall(LPI2C_base_user)       == false);
    EU_ASSERT(LPI2C_HAL_SlaveGetRXStall(LPI2C_base_user)        == false);
    EU_ASSERT(LPI2C_HAL_SlaveGetAddrStall(LPI2C_base_user)      == false);
    EU_ASSERT(LPI2C_HAL_SlaveGetHighSpeedModeDetect(LPI2C_base_user) == false);

    /* Finish test case by DeInitialize */
    LPI2C_DRV_SlaveDeinit(LPI2C_User_Instance);
}

#ifdef __cplusplus
}
#endif

/** @} */
