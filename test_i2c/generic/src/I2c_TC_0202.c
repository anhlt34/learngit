/***************************************************************************//*!
*   Freescale Semiconductor Inc.
*   (c) Copyright 2011 Freescale Semiconductor Inc.
*   ALL RIGHTS RESERVED.
*
*   @file             I2c_TC_0202.c
*   @details          Test case I2c_TC_0202.
*   @addtogroup       fsl_I2C_TESTS
*
*******************************************************************************/

/*==================================================================================================
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Nguyen Van Hieu (B46843)
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "I2c_TC_0202.h"

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id          I2c_TC_0202
* @brief            Check functional verification
* @details          Check function LPI2C_DRV_SlaveInit configure correctly all possible Slave address values.
* @pre              N/A
* @post             N/A
*
* @test_level       ComponentValidation
* @test_type        Functional
* @test_technique   BlackBox
* @test_procedure   Steps:
*                       -#  Initialize and configure clocks.
*                       -#  Initialize pins.
*                       -#  Initialize LPI2C module with 7 bit address mode.
*                       -#  Check Slave address with 7-bit mode.
*                       -#  Deinitialize LPI2C module.
*                       -#  Initialize LPI2C module with 10 bit address mode.
*                       -#  Check Slave address with 10-bit mode.
*                       -#  Finish test case by DeInitialize.
* @pass_criteria    All getting slave address values is correct.
* @requirements     Driver: LPI2C_DRV_027. HAL: LPI2C_HAL_343, LPI2C_HAL_345.
* @execution_type   Automated
* @hw_depend        N/A
* @sw_depend        N/A
* @defects          N/A
* @test_priority    High
* @note             N/A
*
* @keywords         N/A
*
*/
void I2c_TC_0202(void)
{
    /*Initialize and configure clocks*/
    CLOCK_SYS_Init(g_clockManConfigsArr, FSL_CLOCK_MANAGER_CONFIG_CNT,
            g_clockManCallbacksArr, FSL_CLOCK_MANAGER_CALLBACK_CNT);
    CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_FORCIBLE);
    /*Initialize pins*/
    Pins_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);
    /* Local variable definition */
    uint32_t Result_baudRate,Result_baudRateHS;
    uint16_t Result_GetAddr;
    lpi2c_status_t Result_lpi2c_status;
    /* Initialize LPI2C module with 7 bit address mode*/
    lpi2c2_SlaveConfig_user.is10bitAddr = false;
    lpi2c2_SlaveConfig_user.slaveAddress = 0x37U;
    Result_lpi2c_status = LPI2C_DRV_SlaveInit(LPI2C_User_Instance, &lpi2c2_SlaveConfig_user, &Slave);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);

    /* Check Slave address with 7-bit mode*/
    Result_GetAddr = LPI2C_HAL_SlaveGetAddr0(LPI2C_base_user);
    EU_ASSERT(Result_GetAddr ==  lpi2c2_SlaveConfig_user.slaveAddress);
    /*Deinitialize LPI2C module*/
    LPI2C_DRV_SlaveDeinit(LPI2C_User_Instance);

    /* Initialize LPI2C module with 10 bit address mode*/
    lpi2c2_SlaveConfig_user.is10bitAddr = true;
    lpi2c2_SlaveConfig_user.slaveAddress = 0x337U;
    Result_lpi2c_status = LPI2C_DRV_SlaveInit(LPI2C_User_Instance, &lpi2c2_SlaveConfig_user, &Slave);
    EU_ASSERT(Result_lpi2c_status ==  LPI2C_STATUS_SUCCESS);

    /* Check Slave address with 10-bit mode*/
    Result_GetAddr = LPI2C_HAL_SlaveGetAddr0(LPI2C_base_user);
    EU_ASSERT(Result_GetAddr ==  lpi2c2_SlaveConfig_user.slaveAddress);

    /* Finish test case by DeInitialize */
    LPI2C_DRV_SlaveDeinit(LPI2C_User_Instance);
}

#ifdef __cplusplus
}
#endif

/** @} */
