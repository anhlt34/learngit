/***************************************************************************//*!
*   Freescale Semiconductor Inc.
*   (c) Copyright 2011 Freescale Semiconductor Inc.
*   ALL RIGHTS RESERVED.
*
*   @file             I2c_TC_0108.c
*   @details          Test case I2c_TC_0108.
*   @addtogroup       fsl_I2C_TESTS
*
*******************************************************************************/

/*==================================================================================================
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Nguyen Van Hieu (B46843)
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "I2c_TC_0108.h"

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id          I2c_TC_0108
* @brief            Check functional verification
* @details          Check function LPI2C_DRV_MasterDeinit shall deinitialize correctly LPI2C module.
* @pre              N/A
* @post             N/A
*
* @test_level       ComponentValidation
* @test_type        Functional
* @test_technique   BlackBox
* @test_procedure   Steps:
*                       -#  Initialize LPI2C module.
*                       -#  Check pointer to runtime state structure is NULL.
*                       -#  Check master is disable.
*                       -#  Initialize LPI2C_User_Instance again to ensure deinitialize function configure successfully.
*                       -#  Finish test case by DeInitialize.
* @pass_criteria    LPI2C_DRV_MasterDeinit function shall deinitialize correctly the LPI2C module.
* @requirements     Driver: LPI2C_DRV_003.
* @execution_type   Automated
* @hw_depend        N/A
* @sw_depend        N/A
* @defects          N/A
* @test_priority    High
* @note             N/A
*
* @keywords         N/A
*/
void I2c_TC_0108(void)
{
    /* Local variable definition */
    lpi2c_status_t status = LPI2C_STATUS_FAIL;
    bool Result_interrupt;
    /*Initialize LPI2C module*/
    status = LPI2C_DRV_MasterInit(LPI2C_User_Instance, &lpi2c1_MasterConfig_user, &master );
    EU_ASSERT(status ==  LPI2C_STATUS_SUCCESS);

    /* Deinitialize for LPI2C0*/
    status = LPI2C_DRV_MasterDeinit(LPI2C_User_Instance);
    EU_ASSERT(status == LPI2C_STATUS_SUCCESS);
    /*Check pointer to runtime state structure is NULL*/
    EU_ASSERT(g_lpi2cStatePtr[LPI2C_User_Instance] == NULL);
    /*Check master is disable*/
    EU_ASSERT(LPI2C_HAL_MasterGetEnable(LPI2C_base_user) == false);
    /* Initialize LPI2C_User_Instance again to ensure deinitialize function configure successfully */
    status = LPI2C_DRV_MasterInit(LPI2C_User_Instance, &lpi2c1_MasterConfig_user, &master );
    EU_ASSERT(status ==  LPI2C_STATUS_SUCCESS);

    /* Finish test case by DeInitialize */
    LPI2C_DRV_MasterDeinit(LPI2C_User_Instance);
}
#ifdef __cplusplus
}
#endif

/** @} */
