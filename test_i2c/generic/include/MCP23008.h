#ifndef _MCP23008_H
#define _MCP23008_H

#define MCP_ADDRESS     0x20
#define MCP_OPCODE_W    0x40

#define MCP_REG_IODIR   0x00
#define MCP_REG_IPOL    0x01
#define MCP_REG_GPINTEN 0x02
#define MCP_REG_DEFVAL  0x03
#define MCP_REG_INTCON  0x04
#define MCP_REG_IOCON   0x05
#define MCP_REG_GPPU    0x06
#define MCP_REG_INTF    0x07
#define MCP_REG_INTCAP  0x08
#define MCP_REG_GPIO    0x09
#define MCP_REG_OLAT    0x0A

#define MCP_IOCON_SEQOP     (1<<5)
#define MCP_IOCON_DISSLW    (1<<4)
#define MCP_IOCON_ODR       (1<<2)
#define MCP_IOCON_INTPOL    (1<<1)

void Demo_MCP23008(void);


#endif
