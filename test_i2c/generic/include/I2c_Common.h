
#ifndef I2c_Comon_H
#define I2C_Comon_H

#include "EUnit_Api.h"
#include "EUnit_Types.h"
#include "EUnit.h"
#include "fsl_flash_driver_c90tfs.h"
#include "fsl_clock_S32K144.h"

#include "clockMan1.h"
#include "lpi2c1.h"
#include "lpi2c2.h"
#include "adConv1.h"
#include "lpuart1.h"
#include "pin_mux.h"
#include "fsl_sim_hal.h"

#include "MCP23008.h"


#define LPI2C_User_Instance 0
#if(LPI2C_User_Instance == 0)
#define LPI2C_base_user LPI2C0
#define PCC_LPI2C_CLOCK PCC_LPI2C0_CLOCK
#else
#define LPI2C_base_user LPI2C1
#define PCC_LPI2C_CLOCK PCC_LPI2C1_CLOCK
#endif
#define LPI2C_Instance_0    0
#define LPI2C_Instance_1    1

extern const IRQn_Type g_lpi2cIrqId[LPI2C_INSTANCE_COUNT];
extern void * g_lpi2cStatePtr[LPI2C_INSTANCE_COUNT];
extern uint8_t masterBuffer[16];
extern uint8_t slaveBuffer[16];
extern lpi2c_master_state_t master;
extern lpi2c_master_user_config_t lpi2c1_MasterConfig_user;
extern lpi2c_slave_state_t Slave;
extern lpi2c_slave_user_config_t lpi2c2_SlaveConfig_user;

void lpi2c2_Callback0(uint8_t instance, lpi2c_slave_event_t slaveEvent, void *userData);
void print(const char* sourceStr);
void printHexData(const uint8_t *source, uint8_t arrayLen);
void Compare_InitConverterStruct(adc_converter_config_t* config);
void Delay(uint32_t Time);
void MCP23008_Write(uint8_t Address_Reg, uint8_t Value);
uint8_t MCP23008_Read(uint8_t Address_Reg);

#endif
/* I2c_Comon_H */
/** @} */
