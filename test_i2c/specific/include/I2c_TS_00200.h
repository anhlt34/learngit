/**
*   @file    I2c_TS_00200.h
*   @version 
*
*   @brief   AUTOSAR M4_SRC_MODULE_NAME - Test case header file .
*   @details Contain tests case declaration
*
*   @addtogroup I2c_TESTS
*   @{
*/
/*==================================================================================================
*   Project              : 
*   Platform             : 
*   Peripheral           : 
*   Dependencies         : 
*
*   Autosar Version      : 
*   Autosar Revision     : 
*   Autosar Conf.Variant :
*   SW Version           : 
*   Build Version        : 
*
*   (c) Copyright  
*   All Rights Reserved.
==================================================================================================*/
/*==================================================================================================
ifdef(`M4_SRC_KEEP_REVISION_HISTORY', `dnl - DO NOT modify this M4 line!
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------

---------------------------   ----------    ------------  ------------------------------------------
',)dnl - DO NOT modify this M4 line!
==================================================================================================*/

#ifndef I2c_TS_00200_H
#define I2c_TS_00200_H

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/
#include "EUnit.h"

/*==================================================================================================
*                                    FUNCTION PROTOTYPES
==================================================================================================*/
/* Functional Test */
void I2c_TC_0201(void);
void I2c_TC_0202(void);
void I2c_TC_0203(void);
void I2c_TC_0204(void);
void I2c_TC_0205(void);
void I2c_TC_0206(void);
void I2c_TC_0207(void);
void I2c_TC_0208(void);

#ifdef __cplusplus
}
#endif

#endif  /* I2c_TS_00200_H */

/** @} */

