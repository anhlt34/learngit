/**
*   @file    I2c_TS_00100.h
*   @version 
*
*   @brief   AUTOSAR M4_SRC_MODULE_NAME - Test case header file .
*   @details Contain tests case declaration
*
*   @addtogroup I2c_TESTS
*   @{
*/
/*==================================================================================================
*   Project              : 
*   Platform             : 
*   Peripheral           : 
*   Dependencies         : 
*
*   Autosar Version      : 
*   Autosar Revision     : 
*   Autosar Conf.Variant :
*   SW Version           : 
*   Build Version        : 
*
*   (c) Copyright  
*   All Rights Reserved.
==================================================================================================*/
/*==================================================================================================
ifdef(`M4_SRC_KEEP_REVISION_HISTORY', `dnl - DO NOT modify this M4 line!
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------

---------------------------   ----------    ------------  ------------------------------------------
',)dnl - DO NOT modify this M4 line!
==================================================================================================*/

#ifndef I2c_TS_00100_H
#define I2c_TS_00100_H

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/
#include "EUnit.h"

/*==================================================================================================
*                                    FUNCTION PROTOTYPES
==================================================================================================*/
/* Functional Test */
void I2c_TC_0101(void);
void I2c_TC_0102(void);
void I2c_TC_0103(void);
void I2c_TC_0104(void);
void I2c_TC_0105(void);
void I2c_TC_0106(void);
void I2c_TC_0107(void);
void I2c_TC_0108(void);

#ifdef __cplusplus
}
#endif

#endif  /* I2c_TS_00100_H */

/** @} */

