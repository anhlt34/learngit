/**
*   @file    I2c_TS_00600.c
*   @version
*
*   @brief  
*   @details Execute a check functionality.
*
*   @addtogroup I2c_TEST
*   @{
*/
/*==================================================================================================
*   Project              :
*   Platform             :
*   Peripheral           :
*   Dependencies         :
*
*   Autosar Version      :
*   Autosar Revision     :
*   Autosar Conf.Variant :
*   SW Version           :
*   Build Version        :
*
*   (c) Copyright 
*   All Rights Reserved.
==================================================================================================*/
/*==================================================================================================
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Nguyen Van Hieu (B46843)         
---------------------------   ----------    ------------  ------------------------------------------
',)dnl - DO NOT modify this M4 line!
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/
#include "I2c_TS_00600.h"
/*==================================================================================================
*                                       DEFINES
==================================================================================================*/

/*==================================================================================================
                           REGISTRATION OF THE TEST CASES
==================================================================================================*/

EU_TEST_SUITE_BEGIN(I2c_TS_00600)
    EU_TEST_CASE_ADD(I2c_TC_0601, "Check function LPI2C_DRV_SlaveSendDataBlocking shall transmit correctly data and function LPI2C_DRV_MasterReceiveDataBlocking receive correctly data in STANDARD_MODE"),
    EU_TEST_CASE_ADD(I2c_TC_0602, "Check function LPI2C_DRV_SlaveSendDataBlocking shall transmit correctly data and function LPI2C_DRV_MasterReceiveDataBlocking receive correctly data in 10 bit mode"),
    EU_TEST_CASE_ADD(I2c_TC_0603, "Check function LPI2C_DRV_SlaveSendDataBlocking shall transmit correctly data and function LPI2C_DRV_MasterReceiveDataBlocking receive correctly data in FAST_MODE"),
    EU_TEST_CASE_ADD(I2c_TC_0604, "Check function LPI2C_DRV_SlaveSendDataBlocking shall transmit correctly data and function LPI2C_DRV_MasterReceiveDataBlocking receive correctly data in FASTPLUS_MODE"),
    EU_TEST_CASE_ADD(I2c_TC_0605, "Check function LPI2C_DRV_SlaveSendDataBlocking shall transmit correctly data and function LPI2C_DRV_MasterReceiveDataBlocking receive correctly data in HIGHSPEED_MODE"),
    EU_TEST_CASE_ADD(I2c_TC_0606, "Check function LPI2C_DRV_SlaveSendDataBlocking shall transmit correctly data and function LPI2C_DRV_MasterReceiveDataBlocking receive correctly data in ULTRAFAST_MODE")
EU_TEST_SUITE_END(I2c_TS_00600)
/*==================================================================================================
                           REGISTRATION OF THE TEST PATTERN
==================================================================================================*/
EU_TEST_REGISTRY_BEGIN()
    EU_TEST_SUITE_ADD(I2c_TS_00600, "Check slave transmit and master receive")
EU_TEST_REGISTRY_END()

/*==================================================================================================
*                                       LOCAL FUNCTION PROTOTYPES
==================================================================================================*/

/*==================================================================================================
*                                       LOCAL FUNCTIONS
==================================================================================================*/


/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/
#define I2C_START_SEC_CODE
/**
* @brief          functional test.
* @details        Execute a check functionality
*
*
* @return         int
* @retval RETURNED_VALUE_1
*
* @pre            DevErrorDetect_F
*                 AcLoadOnJobStart_F
*/

int main(void)
{
    EU_RUN_ALL_TESTS(VV_RESULT_ADDRESS);
    return 0;    
}



#define I2C_STOP_SEC_CODE

#ifdef __cplusplus
}
#endif

/** @} */