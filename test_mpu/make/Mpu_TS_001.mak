#
#   Copyright (c) 2016, Freescale Semiconductor, Inc.
#   ALL RIGHTS RESERVED.
#   @file    Mpu_TS_001.mak
#   @version M4_SDK_TEST_VERSION_MAJOR.M4_SDK_TEST_VERSION_MINOR.M4_SDK_TEST_VERSION_PATCH
#
#   @brief   Makefile for Mpu test suite 001
#   @details Makefile for Mpu test suite 001
#
#===================================================================================================
#ifdef(`M4_SRC_KEEP_REVISION_HISTORY', `dnl - DO NOT modify this M4 line!
#Revision History:
#                             Modification     Tracking
#Author (core ID)              Date D/M/Y       Number     Description of Changes
#---------------------------   ----------    ------------  ------------------------------------------
#Anh Le - B57183               01/07/2016     ASDK-1341     Initial version for SDK
#---------------------------   ----------    ------------  ------------------------------------------
#',)dnl - DO NOT modify this M4 line!
#==================================================================================================*/
#
################################################################################
#  Include MSN generic definitions
################################################################################
include $(CFG_DIR)/make/Mpu_TS_Common.mk

################################################################################
#  Configuration folder where epc files are stored.
################################################################################
CONFIG_USED       := specific/cfg/cfg_01/generate

################################################################################
# List of Test Case source files
################################################################################
SRC_FILES += $(CFG_DIR)/generic/src/Mpu_TC_Common.c \
             $(CFG_DIR)/generic/src/Mpu_TC_0101.c \
             $(CFG_DIR)/generic/src/Mpu_TC_0102.c
################################################################################
# List of specific source files needed by the test suite
################################################################################

################################################################################
# Test Suite source file

################################################################################
SRC_FILES +=  $(CFG_DIR)/specific/src/Mpu_TS_001.c \
              $(CFG_DIR)/specific/src/Mpu_Cfg_Common.c \
              $(CFG_DIR)/specific/src/Mpu_Cfg_001.c

################################################################################
# Specific vector_vle.s file

################################################################################

#DEFAULT_VECTOR_CORE_TBL =$(CFG_DIR)/specific/src/Vector_core.s

################################################################################
# List of asm source files (Cosmic require different files)
################################################################################

################################################################################
# List of specific include directories (test)

################################################################################

################################################################################
# Additional test specific linker options
################################################################################
LDOPT            +=
CCOPT            +=  -DVV_RESULT_ADDRESS=$(TEST_BASE_ADDR)

################################################################################
# Custom libraries
################################################################################
LIBS            +=

################################################################################
# Test execution timeout in tenth of seconds
################################################################################
# 4hours, i.e. 240minutes
ifeq ($(CCOV_ENABLE), ON)
TEST_TIMEOUT    := 144000
else
TEST_TIMEOUT    := 200
endif

################################################################################
# For MISRA generate
################################################################################
