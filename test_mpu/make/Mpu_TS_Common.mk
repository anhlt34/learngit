#
#   Copyright (c) 2016, Freescale Semiconductor, Inc.
#   ALL RIGHTS RESERVED.
#   @file    Mpu_TS_Common.mak
#   @version M4_SDK_TEST_VERSION_MAJOR.M4_SDK_TEST_VERSION_MINOR.M4_SDK_TEST_VERSION_PATCH
#
#   @brief   Makefile common for Mpu tests
#   @details Makefile common for Mpu tests
#
#===================================================================================================
#ifdef(`M4_SRC_KEEP_REVISION_HISTORY', `dnl - DO NOT modify this M4 line!
#Revision History:
#                             Modification     Tracking
#Author (core ID)              Date D/M/Y       Number     Description of Changes
#---------------------------   ----------    ------------  ------------------------------------------
#Anh Le - B57183               01/07/2016     ASDK-1341     Initial version for SDK
#---------------------------   ----------    ------------  ------------------------------------------
#',)dnl - DO NOT modify this M4 line!
#==================================================================================================*/
#
################################################################################
# Test execution control, if enabled the user controls the execution
# if disabled the wait user block is removed
################################################################################
# TEST_WAIT_USER  := FALSE
################################################################################

################################################################################
# List of all source .C  of Generate_code
################################################################################
SRC_DIRS     += $(EUNIT_SRC_DIRS)
INCLUDE_DIRS += $(EUNIT_INCLUDE_DIRS)

CFG_FILE = $(wildcard $(CFG_DIR)/$(CONFIG_USED)/*.c)

################################################################################
# List of all head file of Generate_code
################################################################################

HEAD_FILE = $(wildcard $(CFG_DIR)/$(CONFIG_USED)/*.h)


# ifeq ($(CCOV_ENABLE),ON)
# INCLUDE_DIRS += $(DEV_DIR)/vnv_config/include_LDRA
# endif
################################################################################
# List of all source directories (test, dependency, and generated .c files)
################################################################################

SRC_DIRS += $(SDK32_DIR)/platform/hal/src/mpu \
            $(SDK32_DIR)/platform/hal/src/pcc \
            $(SDK32_DIR)/platform/hal/src/scg \
            $(SDK32_DIR)/platform/hal/src/sim \
            $(SDK32_DIR)/platform/hal/src/smc \
            $(SDK32_DIR)/platform/drivers/src/clock \
            $(SDK32_DIR)/platform/drivers/src/mpu \
            $(SDK32_DIR)/platform/drivers/src/clock/$(DERIVATIVE) \
            $(SDK32_DIR)/platform/drivers/src/interrupt \
            $(SDK32_DIR)/platform/devices \
            $(SDK32_DIR)/platform/devices/$(DERIVATIVE)/startup \
            $(GENERATE_DIR)/src \
            $(SDK32_DIR)/platform/hal/src/sim/S32K144 \

################################################################################
# List of source files
################################################################################

################################################################################
# List of all include directories (test, dependency, and generated .h files)
################################################################################

INCLUDE_DIRS+= $(GENERATE_DIR)/include \
               $(CFG_DIR)/generic/include \
               $(CFG_DIR)/specific/include \
               $(SDK32_DIR)/platform/devices \
               $(SDK32_DIR)/platform/devices/$(DERIVATIVE)/include \
               $(SDK32_DIR)/platform/devices/$(DERIVATIVE)/startup \
               $(SDK32_DIR)/platform/drivers/inc \
               $(SDK32_DIR)/platform/hal/inc \
               $(SDK32_DIR)/platform/drivers/src/clock/S32K144 \
               $(SDK32_DIR)/platform/drivers/inc \
               $(SDK32_DIR)/platform/hal/src/sim/S32K144


#MCU Lite
INCLUDE_DIRS += $(CFG_DIR)/../test/include

################################################################################
# For MISRA generate
################################################################################
DRIVER_SRC_FILES        += $(wildcard $(SDK32_DIR)/platform/drivers/src/mpu/*.c)
DRIVER_SRC_FILES        += $(wildcard $(SDK32_DIR)/platform/hal/src/mpu/*.c)
CODE_GENERATE_SRC_FILES += $(GENERATE_DIR)/src/memProtect1.c
