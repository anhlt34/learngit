/**
*   Copyright (c) 2016, Freescale Semiconductor, Inc.
*   ALL RIGHTS RESERVED.
*   @file    Mpu_Cfg_002.h
*   @version M4_SDK_TEST_VERSION_MAJOR.M4_SDK_TEST_VERSION_MINOR.M4_SDK_TEST_VERSION_PATCH
*
*   @brief   Configure 002 header file
*   @details Configure 002 header file
*
*   @addtogroup [MPU_TESTS]
*   @{
*/
/*==================================================================================================
ifdef(`M4_SRC_KEEP_REVISION_HISTORY', `dnl - DO NOT modify this M4 line!
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Anh Le - B57183               01/07/2016     ASDK-1341     Initial version for SDK
---------------------------   ----------    ------------  ------------------------------------------
',)dnl - DO NOT modify this M4 line!
==================================================================================================*/

#ifndef MPU_Cfg_002_H
#define MPU_Cfg_002_H

#ifdef __cplusplus
extern "C" {
#endif


/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "Mpu_TC_Common.h"
#include "Mpu_Cfg_Common.h"

/*==================================================================================================
*                              SOURCE FILE VERSION INFORMATION
==================================================================================================*/


/*==================================================================================================
*                                     FILE VERSION CHECKS
==================================================================================================*/


/*==================================================================================================
*                                          CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      DEFINES AND MACROS
==================================================================================================*/

#define T_ARR_SIZE                  (10U)
#define MPU_TIMEOUT_TEST            (0xFFFFU)
#define T_FAULT_TIMEOUT             (0xFFFFU)
#define T_ACCESS_RIGHTS_NUM         (2U) /* READ and WRITR, EXECUTE pending */
#define T_REGION_NUM                (4U)
#define T_SRAML_REGION_TEST         (2U)
#define T_MODE_NUM                  (2U)  /* Supervisor and user modes */
#define T_USER_MODE                 (1U)
#define T_ERR_TYPE_NUM              (2U)  /* Read and write error types */
#define T_WRITE_DENY                (0U)
#define T_READ_DENY                 (1U)
#define T_EXECUTE_DENY              (2U)
#define T_CORE_BUS_ID               (0U)
#define T_MEM_START_ADDR            (0x00000000U)
#define T_MEM_END_ADDR              (0xFFFFFFFFU)
#define ACTUAL_REGION_END_ADDR(x)   ((uint32_t)(x) | (0x1FU))
#define ACTUAL_REGION_START_ADDR(x) ((uint32_t)(x) & (~0x1FU))
#define CPU_REG_NVIC_SHCSR (*((uint32_t *)(0xE000ED24U)))
#define CPU_REG_NVIC_SHCSR_BUSFAULTENA (0x00020000U)
#define CPU_REG_SCnSCB_ACTLR (*((uint32_t *)(0xE000E008U)))
#define CPU_REG_SCnSCB_ACTLR_DISDEFWBUF (0x00000002U)

#define T_MPU_FLASH_NOT_READ_ADD        0x00008020U
/*==================================================================================================
*                                             ENUMS
==================================================================================================*/


/*==================================================================================================
*                                STRUCTURES AND OTHER TYPEDEFS
==================================================================================================*/

extern const mpu_master_access_right_t AccessRightRegion0Config[];
extern const mpu_master_access_right_t AccessRightFullConfig[];
extern const mpu_master_access_right_t AccessRightConfig[][3];
extern const mpu_master_access_right_t AccessRightRXConfig[];
extern const mpu_master_access_right_t AccessRightWXConfig[];
extern const mpu_master_access_right_t AccessRightRWConfig[];

extern const mpu_master_access_right_t AccessRightConfig10[];
extern const mpu_master_access_right_t AccessRightConfig20[];
extern const mpu_user_config_t memProtect_UserConfig[];
extern mpu_access_err_info_t mpuErrInfoSramL[T_MODE_NUM][T_ERR_TYPE_NUM];

/*==================================================================================================
*                                GLOBAL VARIABLE DECLARATIONS
==================================================================================================*/

/*==================================================================================================
*                                    FUNCTION PROTOTYPES
==================================================================================================*/
extern void swUserMode(void);
extern void swSupervisorMode(void);
extern void T_mpuDelay(void);
extern uint8_t T_executeAccessRight(void);
extern void EnableBusFaultIrq(void);
extern void DisableWritebuffer(void);
#ifdef __cplusplus
}
#endif

#endif /* Mpu_Cfg_002_H */

/** @} */
