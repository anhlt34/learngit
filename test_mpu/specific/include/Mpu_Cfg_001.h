/**
*   Copyright (c) 2016, Freescale Semiconductor, Inc.
*   ALL RIGHTS RESERVED.
*   @file    Mpu_Cfg_001.h
*   @version M4_SDK_TEST_VERSION_MAJOR.M4_SDK_TEST_VERSION_MINOR.M4_SDK_TEST_VERSION_PATCH
*
*   @brief   Configure 001 header file
*   @details Configure 001 header file
*
*   @addtogroup [MPU_TESTS]
*   @{
*/
/*==================================================================================================
ifdef(`M4_SRC_KEEP_REVISION_HISTORY', `dnl - DO NOT modify this M4 line!
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Anh Le - B57183               01/07/2016     ASDK-1341     Initial version for SDK
---------------------------   ----------    ------------  ------------------------------------------
',)dnl - DO NOT modify this M4 line!
==================================================================================================*/

#ifndef MPU_Cfg_001_H
#define MPU_Cfg_001_H

#ifdef __cplusplus
extern "C" {
#endif


/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "Mpu_TC_Common.h"

/*==================================================================================================
*                              SOURCE FILE VERSION INFORMATION
==================================================================================================*/


/*==================================================================================================
*                                     FILE VERSION CHECKS
==================================================================================================*/


/*==================================================================================================
*                                          CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      DEFINES AND MACROS
==================================================================================================*/

#define T_REGION_COUNT              (0x2U)
#define T_REGION_INIT_COUNT         (0x8U)
#define T_MASTER_NUM                (0x8U)
#define T_MASTER_BUS_ID_4           (0x4U)
#define T_MASTER_BUS_ID_5           (0x5U)
#define T_MASTER_BUS_ID_6           (0x6U)
#define T_MASTER_BUS_ID_7           (0x7U)
#define T_MPU_HARDWARE_INFO         (0x01U)

/*==================================================================================================
*                                             ENUMS
==================================================================================================*/

/* Describes the number of MPU regions */
typedef enum _mpu_region_total_num
{
    MPU_8REGIONS = 0x0U,  /* MPU supports 8 regions */
    MPU_12REGIONS = 0x1U, /* MPU supports 12 regions */
    MPU_16REGIONS = 0x2U /* MPU supports 16 regions */
} mpu_region_total_num_t;

/*==================================================================================================
*                                STRUCTURES AND OTHER TYPEDEFS
==================================================================================================*/

/* MPU hardware basic information */
typedef struct _mpu_hardware_info
{
    uint8_t hardwareRevisionLevel; /* Specifies the MPU's hardware and definition reversion level */
    uint8_t slavePortsNumbers; /* Specifies the number of slave ports connected to MPU */
    mpu_region_total_num_t regionsNumbers; /* Number of region descriptors implemented */
} mpu_hardware_info_t;


extern const mpu_user_config_t memProtect1_InitConfig[];
extern const mpu_user_config_t memProtect1_ErrConfig[];

extern const mpu_user_config_t mpu_Diff_Region_Config[];
extern mpu_high_masters_access_rights_t mpuHighMasterAccessRightsConfig[];
extern mpu_master_access_right_t AccessRightConfig00[];
extern mpu_master_access_right_t AccessRightConfig01[];
extern mpu_master_access_right_t AccessRightConfig02[];
extern mpu_master_access_right_t AccessRightConfig03[];
extern mpu_master_access_right_t AccessRightConfig04[];

extern const mpu_master_access_right_t AccessRightConfig[];
extern const mpu_master_access_right_t AccessErrConfig[];

/*==================================================================================================
*                                GLOBAL VARIABLE DECLARATIONS
==================================================================================================*/



/*==================================================================================================
*                                    FUNCTION PROTOTYPES
==================================================================================================*/


#ifdef __cplusplus
}
#endif

#endif /* Mpu_Cfg_001_H */

/** @} */
