/**
*   Copyright (c) 2016, Freescale Semiconductor, Inc.
*   ALL RIGHTS RESERVED.
*   @file    Mpu_Cfg_002.c
*   @version M4_SDK_TEST_VERSION_MAJOR.M4_SDK_TEST_VERSION_MINOR.M4_SDK_TEST_VERSION_PATCH
*
*   @brief   Configure 002 source file
*   @details Configure 002 source file
*
*   @addtogroup [MPU_TESTS]
*   @{
*/
/*==================================================================================================
ifdef(`M4_SRC_KEEP_REVISION_HISTORY', `dnl - DO NOT modify this M4 line!
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Anh Le - B57183               01/07/2016     ASDK-1341     Initial version for SDK
---------------------------   ----------    ------------  ------------------------------------------
',)dnl - DO NOT modify this M4 line!
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/
#include "Mpu_Cfg_002.h"

/*==================================================================================================
*                          LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
==================================================================================================*/

/* Master access rights configuration for region 0 */
const mpu_master_access_right_t AccessRightRegion0Config[] =
{
    /* CORE_0 */
    {
        .masterNum = FSL_FEATURE_MPU_MASTER_CORE,
        .accessRight = MPU_SUPERVISOR_USER_NONE,
        .processIdentifierEnable = false
    },

    /* DEBUGGER_0 */
    {
        .masterNum = FSL_FEATURE_MPU_MASTER_DEBUGGER,
        .accessRight = MPU_SUPERVISOR_RWX_USER_RWX,
        .processIdentifierEnable = false
    },

    /* DMA_0 */
    {
        .masterNum = FSL_FEATURE_MPU_MASTER_DMA,
        .accessRight = MPU_SUPERVISOR_RWX_USER_RWX,
        .processIdentifierEnable = false
    }
};

/* Master full access rights configuration */
const mpu_master_access_right_t AccessRightFullConfig[] =
{
    /* CORE_0 */
    {
        .masterNum = FSL_FEATURE_MPU_MASTER_CORE,
        .accessRight = MPU_SUPERVISOR_RWX_USER_RWX,
        .processIdentifierEnable = false
    },

    /* DEBUGGER_0 */
    {
        .masterNum = FSL_FEATURE_MPU_MASTER_DEBUGGER,
        .accessRight = MPU_SUPERVISOR_RWX_USER_RWX,
        .processIdentifierEnable = false
    },

    /* DMA_0 */
    {
        .masterNum = FSL_FEATURE_MPU_MASTER_DMA,
        .accessRight = MPU_SUPERVISOR_RWX_USER_RWX,
        .processIdentifierEnable = false
    }
};

const mpu_master_access_right_t AccessRightConfig[][3] =
{
    /* Read, execute access right */
    {
        /* CORE */
        {
            .masterNum = FSL_FEATURE_MPU_MASTER_CORE,
            .accessRight = MPU_SUPERVISOR_USER_RX,
            .processIdentifierEnable = false
        },

        /* DEBUGGER */
        {
            .masterNum = FSL_FEATURE_MPU_MASTER_DEBUGGER,
            .accessRight = MPU_SUPERVISOR_RWX_USER_RWX,
            .processIdentifierEnable = false
        },

        /* DMA */
        {
            .masterNum = FSL_FEATURE_MPU_MASTER_DMA,
            .accessRight = MPU_SUPERVISOR_RWX_USER_RWX,
            .processIdentifierEnable = false
        }
    },
    /* Write, execute access right */
    {
        /* CORE */
        {
            .masterNum = FSL_FEATURE_MPU_MASTER_CORE,
            .accessRight = MPU_SUPERVISOR_USER_WX,
            .processIdentifierEnable = false
        },

        /* DEBUGGER */
        {
            .masterNum = FSL_FEATURE_MPU_MASTER_DEBUGGER,
            .accessRight = MPU_SUPERVISOR_RWX_USER_RWX,
            .processIdentifierEnable = false
        },

        /* DMA */
        {
            .masterNum = FSL_FEATURE_MPU_MASTER_DMA,
            .accessRight = MPU_SUPERVISOR_RWX_USER_RWX,
            .processIdentifierEnable = false
        }
    },
    /* Write, Read access right */
    {
        /* CORE */
        {
            .masterNum = FSL_FEATURE_MPU_MASTER_CORE,
            .accessRight = MPU_SUPERVISOR_USER_RW,
            .processIdentifierEnable = false
        },

        /* DEBUGGER */
        {
            .masterNum = FSL_FEATURE_MPU_MASTER_DEBUGGER,
            .accessRight = MPU_SUPERVISOR_RWX_USER_RWX,
            .processIdentifierEnable = false
        },

        /* DMA */
        {
            .masterNum = FSL_FEATURE_MPU_MASTER_DMA,
            .accessRight = MPU_SUPERVISOR_RWX_USER_RWX,
            .processIdentifierEnable = false
        }
    },
};

/* MPU access error configuration 2 */
mpu_access_err_info_t mpuErrInfoSramL[T_MODE_NUM][T_ERR_TYPE_NUM] =
{
    /* Supervisor mode */
    {
        {
            .master = 0U,
            .attributes = MPU_DATA_ACCESS_IN_SUPERVISOR_MODE,
            .accessType = MPU_ERR_TYPE_WRITE,
            .accessCtr = 0xA000U,
            .addr = 0,
            .processorIdentification = 0
        },

        {
            .master = 0U,
            .attributes = MPU_DATA_ACCESS_IN_SUPERVISOR_MODE,
            .accessType = MPU_ERR_TYPE_READ,
            .accessCtr = 0xA000U,
            .addr = 0,
            .processorIdentification = 0
        },
    },
    /* User mode */
    {
        {
            .master = 0U,
            .attributes = MPU_DATA_ACCESS_IN_USER_MODE,
            .accessType = MPU_ERR_TYPE_WRITE,
            .accessCtr = 0xA000U,
            .addr = 0,
            .processorIdentification = 0
        },

        {
            .master = 0U,
            .attributes = MPU_DATA_ACCESS_IN_USER_MODE,
            .accessType = MPU_ERR_TYPE_READ,
            .accessCtr = 0xA000U,
            .addr = 0,
            .processorIdentification = 0
        },
    },
};

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/


/*==================================================================================================
*                                      LOCAL CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      LOCAL VARIABLES
==================================================================================================*/


/*==================================================================================================
*                                      GLOBAL CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      GLOBAL VARIABLES
==================================================================================================*/

volatile bool g_hardFaultFlag = false;
uint32_t count;
mpu_access_err_info_t mpuSramLAccessErrInfo;

/*==================================================================================================
*                                   LOCAL FUNCTION PROTOTYPES
==================================================================================================*/

/*==================================================================================================
*                                       LOCAL FUNCTIONS
==================================================================================================*/


/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*!
 * @brief The function is used to enable bus fault
 */
void EnableBusFaultIrq(void)
{
    CPU_REG_NVIC_SHCSR |= CPU_REG_NVIC_SHCSR_BUSFAULTENA;
}

/* Switch to user mode */
void swUserMode(void)
{
    __asm (" MOVS R0, #0x5 ");
    __asm (" MSR CONTROL, R0 ");
    // __asm ("DSB");
    __asm ("ISB");
}

/* Switch to supervisor mode */
void swSupervisorMode(void)
{
    __asm (" MOVS R0, #0x4 ");
    __asm (" MSR CONTROL, R0 ");
    // __asm ("DSB");
    __asm ("ISB");
}

// void BusFault_Handler(void)
// {
//     MPU_DRV_GetDetailErrorAccessInfo(0, 2, &mpuSramLAccessErrInfo);
//     (void)MPU_DRV_SetMasterAccessRights(MPU_INSTANCE, T_MPU_REGION0, AccessRightFullConfig);
//     g_hardFaultFlag = true;
//     swSupervisorMode();

//     __asm (" MOV R0, SP ");
//     __asm (" LDR R1, [R0, #24] ");
//     __asm (" ADDS R1, #2 ");
//     __asm (" STR R1, [R0, #24] ");
//     MPU_DRV_Deinit(MPU_INSTANCE);
// }

void HardFault_Handler(void)
{
    // swSupervisorMode();
    T_mpuDelay();
    (void)MPU_DRV_SetMasterAccessRights(MPU_INSTANCE, T_MPU_REGION0, AccessRightFullConfig);
    T_mpuDelay();
    (void)MPU_DRV_SetMasterAccessRights(MPU_INSTANCE, T_SRAML_REGION_TEST, AccessRightFullConfig);
    T_mpuDelay();
    MPU_HAL_Disable(T_g_mpuBase[MPU_INSTANCE]);
    MPU_DRV_Deinit(MPU_INSTANCE);
    T_mpuDelay();
    // MPU_DRV_GetDetailErrorAccessInfo(0, 0, &mpuSramLAccessErrInfo);
    // g_hardFaultFlag = true;
    EU_ASSERT(true);

    // __asm (" MOV R0, SP ");
    // __asm (" LDR R1, [R0, #24] ");
    // __asm (" ADDS R1, #2 ");
    // __asm (" STR R1, [R0, #24] ");
    // while(1);
    // MPU_DRV_Deinit(MPU_INSTANCE);
}
/*!
 * @brief The function is used to disable write buffer
 */
void DisableWritebuffer(void)
{
    CPU_REG_SCnSCB_ACTLR |= CPU_REG_SCnSCB_ACTLR_DISDEFWBUF;
}
uint8_t T_executeAccessRight(void)
{
    return 2U;
}

void T_mpuDelay(void)
{
    for (count = 0; count < T_FAULT_TIMEOUT; count++)
    {
        __asm ("nop");
    }
}

#ifdef __cplusplus
}
#endif

/** @} */
