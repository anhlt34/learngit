/**
*   Copyright (c) 2016, Freescale Semiconductor, Inc.
*   ALL RIGHTS RESERVED.
*   @file    Mpu_Cfg_001.c
*   @version M4_SDK_TEST_VERSION_MAJOR.M4_SDK_TEST_VERSION_MINOR.M4_SDK_TEST_VERSION_PATCH
*
*   @brief   Configure 001 source file
*   @details Configure 001 source file
*
*   @addtogroup [MPU_TESTS]
*   @{
*/
/*==================================================================================================
ifdef(`M4_SRC_KEEP_REVISION_HISTORY', `dnl - DO NOT modify this M4 line!
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Anh Le - B57183               01/07/2016     ASDK-1341     Initial version for SDK
---------------------------   ----------    ------------  ------------------------------------------
',)dnl - DO NOT modify this M4 line!
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/
#include "Mpu_Cfg_001.h"

/*==================================================================================================
*                          LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
==================================================================================================*/

const mpu_master_access_right_t AccessRightErrConfig[] =
{
    /* CORE_0 */
    {
        .masterNum = T_MASTER_NUM,
        .accessRight = MPU_SUPERVISOR_USER_NONE,
#if FSL_FEATURE_MPU_HAS_PROCESS_IDENTIFIER
        .processIdentifierEnable = false
#endif /* FSL_FEATURE_MPU_HAS_PROCESS_IDENTIFIER */
    },

    /* DEBUGGER_0 */
    {
        .masterNum = FSL_FEATURE_MPU_MASTER_DEBUGGER,
        .accessRight = MPU_SUPERVISOR_RWX_USER_RWX,
#if FSL_FEATURE_MPU_HAS_PROCESS_IDENTIFIER
        .processIdentifierEnable = false
#endif /* FSL_FEATURE_MPU_HAS_PROCESS_IDENTIFIER */
    },

    /* DMA_0 */
    {
        .masterNum = FSL_FEATURE_MPU_MASTER_DMA,
        .accessRight = MPU_SUPERVISOR_RWX_USER_RWX,
#if FSL_FEATURE_MPU_HAS_PROCESS_IDENTIFIER
        .processIdentifierEnable = false
#endif /* FSL_FEATURE_MPU_HAS_PROCESS_IDENTIFIER */
    }
};

const mpu_master_access_right_t AccessRightConfig[] =
{
    /* CORE */
    {
        .masterNum = FSL_FEATURE_MPU_MASTER_CORE,
        .accessRight = MPU_SUPERVISOR_RWX_USER_RWX,
#if FSL_FEATURE_MPU_HAS_PROCESS_IDENTIFIER
        .processIdentifierEnable = false
#endif /* FSL_FEATURE_MPU_HAS_PROCESS_IDENTIFIER */
    },

    /* DEBUGGER */
    {
        .masterNum = FSL_FEATURE_MPU_MASTER_DEBUGGER,
        .accessRight = MPU_SUPERVISOR_RWX_USER_RWX,
#if FSL_FEATURE_MPU_HAS_PROCESS_IDENTIFIER
        .processIdentifierEnable = false
#endif /* FSL_FEATURE_MPU_HAS_PROCESS_IDENTIFIER */
    },

    /* DMA */
    {
        .masterNum = FSL_FEATURE_MPU_MASTER_DMA,
        .accessRight = MPU_SUPERVISOR_RWX_USER_RWX,
#if FSL_FEATURE_MPU_HAS_PROCESS_IDENTIFIER
        .processIdentifierEnable = false
#endif /* FSL_FEATURE_MPU_HAS_PROCESS_IDENTIFIER */
    }
};

const mpu_user_config_t memProtect1_ErrConfig[] =
{
    /* Region 0 */
    {
        .startAddr       = 0x00000000U,
        .endAddr         = 0xFFFFFFFFU,
        .masterAccRight  = AccessRightErrConfig,
#if FSL_FEATURE_MPU_HAS_PROCESS_IDENTIFIER
        .processIdentifier = 0x00U,
        .processIdMask   = 0x00U
#endif /* FSL_FEATURE_MPU_HAS_PROCESS_IDENTIFIER */
    },

    /* Region 1 */
    {
        .startAddr       = 0x00000000U,
        .endAddr         = 0x0007FEFFU,
        .masterAccRight  = AccessRightErrConfig,
#if FSL_FEATURE_MPU_HAS_PROCESS_IDENTIFIER
        .processIdentifier = 0x00U,
        .processIdMask   = 0x00U
#endif /* FSL_FEATURE_MPU_HAS_PROCESS_IDENTIFIER */
    },
};

const mpu_user_config_t memProtect1_InitConfig[] =
{
    /* Region 0 */
    {
        .startAddr       = 0x00000000U,
        .endAddr         = 0xFFFFFFFFU,
        .masterAccRight  = AccessRightConfig,
#if FSL_FEATURE_MPU_HAS_PROCESS_IDENTIFIER
        .processIdentifier = 0x00U,
        .processIdMask   = 0x00U
#endif /* FSL_FEATURE_MPU_HAS_PROCESS_IDENTIFIER */
    },

    /* Region 1 */
    {
        .startAddr       = 0x00000000U,
        .endAddr         = 0x0007FEFFU,
        .masterAccRight  = AccessRightConfig,
#if FSL_FEATURE_MPU_HAS_PROCESS_IDENTIFIER
        .processIdentifier = 0x00U,
        .processIdMask   = 0x00U
#endif /* FSL_FEATURE_MPU_HAS_PROCESS_IDENTIFIER */
    },
};


/* User configuration with difference setting */
const mpu_user_config_t mpu_Diff_Region_Config[] =
{

    /* Region 0 */
    {
        .startAddr       = 0x00000000U,        /* Memory region start address  */
        .endAddr         = 0xFFFFFFFFU,        /* Memory region end address    */
        .masterAccRight  = AccessRightConfig00, /* Master access right          */
        .processIdentifier = 0x00U,            /* Process identifier           */
        .processIdMask   = 0x00U               /* Process identifier mask      */
    },

    /* Region 1 */
    {
        .startAddr       = 0x0007FF00U,        /* Memory FLASH region start address  */
        .endAddr         = 0x0007FF20U,        /* Memory FLASH region end address    */
        .masterAccRight  = AccessRightConfig01, /* Master access right          */
        .processIdentifier = 0x00U,            /* Process identifier           */
        .processIdMask   = 0x00U               /* Process identifier mask      */
    },

    /* Region 2 */
    {
        .startAddr       = 0x1FFFFFC0U,        /* Memory SRAM_L region start address  */
        .endAddr         = 0x1FFFFFE0U,        /* Memory SRAM_L region end address    */
        .masterAccRight  = AccessRightConfig02, /* Master access right          */
        .processIdentifier = 0x33U,            /* Process identifier           */
        .processIdMask   = 0x77U               /* Process identifier mask      */
    },

    /* Region 3 */
    {
        .startAddr       = 0x20006F00U,        /* Memory SRAM_U region start address  */
        .endAddr         = 0x20006FE0U,        /* Memory SRAM_U region end address    */
        .masterAccRight  = AccessRightConfig03, /* Master access right          */
        .processIdentifier = 0x11U,            /* Process identifier           */
        .processIdMask   = 0x22U               /* Process identifier mask      */
    },

    /* Region 4 */
    {
        .startAddr       = 0x0007FF60U,        /* Memory FLASH region start address  */
        .endAddr         = 0x0007FF80U,        /* Memory FLASH region end address    */
        .masterAccRight  = AccessRightConfig01, /* Master access right          */
        .processIdentifier = 0x00U,            /* Process identifier           */
        .processIdMask   = 0x00U               /* Process identifier mask      */
    },

    /* Region 5 */
    {
        .startAddr       = 0x1FFFFFA0U,        /* Memory SRAM_L region start address  */
        .endAddr         = 0x1FFFFFE0U,        /* Memory SRAM_L region end address    */
        .masterAccRight  = AccessRightConfig02, /* Master access right          */
        .processIdentifier = 0x44U,            /* Process identifier           */
        .processIdMask   = 0xCCU               /* Process identifier mask      */
    },

    /* Region 6 */
    {
        .startAddr       = 0x20006F20U,        /* Memory SRAM_U region start address  */
        .endAddr         = 0x20006F60U,        /* Memory SRAM_U region end address    */
        .masterAccRight  = AccessRightConfig04, /* Master access right          */
        .processIdentifier = 0x0FU,            /* Process identifier           */
        .processIdMask   = 0xF0U               /* Process identifier mask      */
    },

    /* Region 7 */
    {
        .startAddr       = 0x20006F20U,        /* Memory SRAM_U region start address  */
        .endAddr         = 0x20006F40U,        /* Memory SRAM_U region end address    */
        .masterAccRight  = AccessRightConfig01, /* Master access right          */
        .processIdentifier = 0x00U,            /* Process identifier           */
        .processIdMask   = 0x00U               /* Process identifier mask      */
    }
};


/* MPU high master access rights configuration */
mpu_high_masters_access_rights_t mpuHighMasterAccessRightsConfig[] =
{
    /* access rights 1: w r */
    {
        .writeEnable = true,
        .readEnable = true
    },

    /* access rights 2: w - */
    {
        .writeEnable = true,
        .readEnable = false
    },

    /* access rights 3: - r */
    {
        .writeEnable = false,
        .readEnable = true
    },

    /* access rights 4: - - */
    {
        .writeEnable = false,
        .readEnable = false
    }
};

/* Master access rights configuration 0 for region 0 */
mpu_master_access_right_t AccessRightConfig00[] =
{

    /* CORE_0 */
    {
        .masterNum = FSL_FEATURE_MPU_MASTER_CORE,
        .accessRight = MPU_SUPERVISOR_RWX_USER_RWX,
        .processIdentifierEnable = true
    },

    /* BUS MASTER 4 */
    {
        .masterNum = T_MASTER_BUS_ID_4,
        .accessRight = MPU_W,
        .processIdentifierEnable = true
    },

    /* DMA_0 */
    {
        .masterNum = FSL_FEATURE_MPU_MASTER_DMA,
        .accessRight = MPU_SUPERVISOR_USER_RW,
        .processIdentifierEnable = true
    }
};

/* Master access rights configuration 1 for region 1, region 4 and region 7 */
mpu_master_access_right_t AccessRightConfig01[] =
{

    /* BUS MASTER 5 */
    {
        .masterNum = T_MASTER_BUS_ID_5,
        .accessRight = MPU_R,
        .processIdentifierEnable = false
    },

    /* DEBUGGER_1 */
    {
        .masterNum = FSL_FEATURE_MPU_MASTER_DEBUGGER,
        .accessRight = MPU_SUPERVISOR_USER_RWX,
        .processIdentifierEnable = true
    },

    /* DMA_1 */
    {
        .masterNum = FSL_FEATURE_MPU_MASTER_DMA,
        .accessRight = MPU_SUPERVISOR_USER_RW,
        .processIdentifierEnable = true
    }
};

/* Master access rights configuration 2 for region 2 and region 5 */
mpu_master_access_right_t AccessRightConfig02[] =
{

    /* BUS MASTER 7 */
    {
        .masterNum = T_MASTER_BUS_ID_7,
        .accessRight = MPU_RW,
        .processIdentifierEnable = false
    },

    /* DEBUGGER_2 */
    {
        .masterNum = FSL_FEATURE_MPU_MASTER_DEBUGGER,
        .accessRight = MPU_SUPERVISOR_USER_RWX,
        .processIdentifierEnable = false
    },

    /* DMA_2 */
    {
        .masterNum = FSL_FEATURE_MPU_MASTER_DMA,
        .accessRight = MPU_SUPERVISOR_USER_RW,
        .processIdentifierEnable = true
    }
};

/* Master access rights configuration 3 for region 3 */
mpu_master_access_right_t AccessRightConfig03[] =
{

    /* CORE_3 */
    {
        .masterNum = FSL_FEATURE_MPU_MASTER_CORE,
        .accessRight = MPU_SUPERVISOR_USER_RW,
        .processIdentifierEnable = false
    },

    /* DEBUGGER_3 */
    {
        .masterNum = FSL_FEATURE_MPU_MASTER_DEBUGGER,
        .accessRight = MPU_SUPERVISOR_USER_RWX,
        .processIdentifierEnable = true
    },

    /* DMA_3 */
    {
        .masterNum = FSL_FEATURE_MPU_MASTER_DMA,
        .accessRight = MPU_SUPERVISOR_USER_RW,
        .processIdentifierEnable = false
    }
};

/* Master access rights configuration 4 for region 6 */
mpu_master_access_right_t AccessRightConfig04[] =
{

    /* CORE_4 */
    {
        .masterNum = FSL_FEATURE_MPU_MASTER_CORE,
        .accessRight = MPU_SUPERVISOR_USER_RX,
        .processIdentifierEnable = false
    },

    /* DEBUGGER_4 */
    {
        .masterNum = FSL_FEATURE_MPU_MASTER_DEBUGGER,
        .accessRight = MPU_SUPERVISOR_USER_RWX,
        .processIdentifierEnable = false
    },

    /* DMA_4 */
    {
        .masterNum = FSL_FEATURE_MPU_MASTER_DMA,
        .accessRight = MPU_SUPERVISOR_USER_RW,
        .processIdentifierEnable = false
    }
};

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/


/*==================================================================================================
*                                      LOCAL CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      LOCAL VARIABLES
==================================================================================================*/


/*==================================================================================================
*                                      GLOBAL CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      GLOBAL VARIABLES
==================================================================================================*/


/*==================================================================================================
*                                   LOCAL FUNCTION PROTOTYPES
==================================================================================================*/

/*==================================================================================================
*                                       LOCAL FUNCTIONS
==================================================================================================*/


/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

#ifdef __cplusplus
}
#endif

/** @} */
