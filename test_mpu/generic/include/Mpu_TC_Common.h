/**
*   Copyright (c) 2016, Freescale Semiconductor, Inc.
*   ALL RIGHTS RESERVED.
*   @file    Mpu_TC_Common.h
*   @version M4_SDK_TEST_VERSION_MAJOR.M4_SDK_TEST_VERSION_MINOR.M4_SDK_TEST_VERSION_PATCH
*
*   @brief   Test case common header file
*   @details Test case common header file
*
*   @addtogroup [MPU_TESTS]
*   @{
*/
/*==================================================================================================
ifdef(`M4_SRC_KEEP_REVISION_HISTORY', `dnl - DO NOT modify this M4 line!
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Anh Le - B57183               01/07/2016     ASDK-1341     Initial version for SDK
---------------------------   ----------    ------------  ------------------------------------------
',)dnl - DO NOT modify this M4 line!
==================================================================================================*/

#ifndef MPU_TC_Common_H
#define MPU_TC_Common_H
#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "clockMan1.h"
#include "memProtect1.h"

#include "fsl_mpu_driver.h"
#include "fsl_mpu_hal.h"
#include "fsl_clock_manager.h"
#include "fsl_interrupt_manager.h"

#include "Std_Types.h"
#include "EUnit_Types.h"
#include "EUnit_Api.h"
#include "EUnit.h"
#ifdef CCOV_ENABLE
#include "ccov_main.h"
#endif
/*==================================================================================================
*                              SOURCE FILE VERSION INFORMATION
==================================================================================================*/


/*==================================================================================================
*                                     FILE VERSION CHECKS
==================================================================================================*/


/*==================================================================================================
*                                          CONSTANTS
==================================================================================================*/

/*==================================================================================================
*                                      DEFINES AND MACROS
==================================================================================================*/

#define ENABLED                                  (0x1U)
#define DISABLED                                 (0x0U)
#define MPU_INSTANCE                             (0x0U)
#define T_MPU_REGION0                            (0U)
#define T_SLAVE_PORT_0                           (0U)
#define T_SLAVE_PORT_1                           (1U)
#define T_SLAVE_PORT_2                           (2U)
#define T_SLAVE_PORT_3                           (3U)

/*==================================================================================================
*                                             ENUMS
==================================================================================================*/

/*==================================================================================================
*                                STRUCTURES AND OTHER TYPEDEFS
==================================================================================================*/

/*==================================================================================================
*                                GLOBAL VARIABLE DECLARATIONS
==================================================================================================*/

/*==================================================================================================
*                                    FUNCTION PROTOTYPES
==================================================================================================*/

#endif /* MPU_TC_Common_H */

/** @} */
