/**
*   Copyright (c) 2016, Freescale Semiconductor, Inc.
*   ALL RIGHTS RESERVED.
*   @file    Mpu_TC_0203.c
*   @version M4_SDK_TEST_VERSION_MAJOR.M4_SDK_TEST_VERSION_MINOR.M4_SDK_TEST_VERSION_PATCH
*
*   @brief   Test case 0203
*   @details Test case 0203
*
*   @addtogroup [MPU_TESTS]
*   @{
*/
/*==================================================================================================
ifdef(`M4_SRC_KEEP_REVISION_HISTORY', `dnl - DO NOT modify this M4 line!
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Anh Le - B57183               01/07/2016     ASDK-1341     Initial version for SDK
---------------------------   ----------    ------------  ------------------------------------------
',)dnl - DO NOT modify this M4 line!
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "Mpu_TC_0203.h"

/*==================================================================================================
*                          LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
==================================================================================================*/


/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/


/*==================================================================================================
*                                      LOCAL CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      LOCAL VARIABLES
==================================================================================================*/


/*==================================================================================================
*                                      GLOBAL CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      GLOBAL VARIABLES
==================================================================================================*/

extern volatile uint32_t g_hardFaultFlag;

/*==================================================================================================
*                                   LOCAL FUNCTION PROTOTYPES
==================================================================================================*/


/*==================================================================================================
*                                       LOCAL FUNCTIONS
==================================================================================================*/
/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id        Mpu_TC_0203
* @brief          Check MPU driver with difference regions configuration
* @details        This test case is used to check setting in MPU_DRV_Init() and related functions
*                 with difference regions configuration:
*                   - Configure for FLASH region
*                   - Configure for SRAM_L region
*                   - Configure for SRAM_U region
*                   - Configure with overlap region
*                   - Configure with low bus master
*                   - Configure with high bus master
*
* @pre            - The test code shall enable clock the all MPU modules
*                 - The MPU module shall initialize configuration structure pointer.
* @post           N/A
*
* @test_level     Integration
* @test_type      Functional
* @test_technique BlackBox
* @test_procedure Steps:
*                   -# Initialize clock for all MPU modules
*                   -# Initialize the MPU module by calling the MPU_DRV_Init() function
*                   -# Verification Point:
*                           -# MPU module shall enable
*                           -# MPU module shall disable
*                           -# Verify access rights for high master of region 0
*                           -# Verify access rights for low master of region 0
*                           -# Check process identifier for low master
*                           -# Check start address
*                           -# Check end address
*                           -# Check PID setting of regionNum
*                           -# Check PID mask setting of regionNum
*                           -# Verify access rights for high master of region 0
*                           -# Verify access rights for low master of region 0
*                           -# Check process identifier for low master
*                   -# Disable clock for all MPU modules
* @pass_criteria  Verification Points are successful
*
* @requirements   MPU_001_001, MPU_003_001, MPU_004_001, MPU_005_001, MPU_005_004, MPU_007_001,
*                 MPU_007_002, MPU_008_001, MPU_008_003, MPU_009_001, MPU_009_003, MPU_009_004,
*                 MPU_010_001, MPU_012_001, MPU_013_001, MPU_014_001, MPU_015_001, MPU_016_001,
*                 MPU_017_001, MPU_018_001, MPU_019_001, MPU_020_001, MPU_021_001, MPU_022_001,
*                 MPU_023_001, MPU_024_001, MPU_026_001. MPU_027_001, MPU_028_001, MPU_029_001,
*                 MPU_030_001, MPU_031_001, MPU_032_001, MPU_033_001, MPU_034_001, MPU_035_001,
*                 MPU_036_001
*
* @traceability   N/A
* @execution_type Automated
* @hw_depend      N/A
* @sw_depend      N/A
* @boundary_test  N/A
* @defects
* @test_priority  High
* @note
* @keywords
*/

void Mpu_TC_0203(void)
{
    /* Local variables */
    mpu_status_t T_status = MPU_STATUS_SUCCESS;
    volatile uint32_t sramUArray[T_ARR_SIZE] = {0};
    uint32_t count;
    uint8_t T_instance;
    uint32_t T_mode;
    uint32_t T_access;
    uint32_t T_temp;
    bool T_slavePortErr;

    mpu_access_err_info_t mpuSramUAccessErrInfo;
    mpu_user_config_t mpu_Region_Config[T_REGION_NUM];

    mpu_Region_Config[0].startAddr       = T_MEM_START_ADDR;
    mpu_Region_Config[0].endAddr         = T_MEM_END_ADDR;
    mpu_Region_Config[0].masterAccRight  = AccessRightRegion0Config;
    mpu_Region_Config[0].processIdentifier = 0x00U;
    mpu_Region_Config[0].processIdMask   = 0x00U;

    mpu_Region_Config[1].startAddr       = T_MEM_START_ADDR;
    mpu_Region_Config[1].endAddr      = ACTUAL_REGION_START_ADDR((uint32_t)&sramUArray[0]) - 1;
    mpu_Region_Config[1].masterAccRight  = AccessRightFullConfig;
    mpu_Region_Config[1].processIdentifier = 0x00U;
    mpu_Region_Config[1].processIdMask   = 0x00U;

    mpu_Region_Config[2].startAddr       = ACTUAL_REGION_START_ADDR((uint32_t)&sramUArray[0]);
    mpu_Region_Config[2].endAddr   = ACTUAL_REGION_END_ADDR((uint32_t)&sramUArray[T_ARR_SIZE - 1]);
    mpu_Region_Config[2].masterAccRight  = AccessRightConfig[T_WRITE_DENY];
    mpu_Region_Config[2].processIdentifier = 0x00U;
    mpu_Region_Config[2].processIdMask   = 0x00U;

    mpu_Region_Config[3].startAddr       = ACTUAL_REGION_START_ADDR(mpu_Region_Config[2].endAddr + 1U);
    mpu_Region_Config[3].endAddr         = T_MEM_END_ADDR;
    mpu_Region_Config[3].masterAccRight  = AccessRightFullConfig;
    mpu_Region_Config[3].processIdentifier = 0x00U;
    mpu_Region_Config[3].processIdMask   = 0x00U;


    /* Initialize and configure clocks for MPU module */
    T_Mpu_Init_Clock();

    for (T_instance = 0; T_instance < MPU_INSTANCE_COUNT; T_instance++)
    {
        (void)MPU_DRV_Init(T_instance, T_REGION_NUM, mpu_Region_Config);
        __asm ("DSB"); /* Memory barriers to ensure subsequence data and instruction */
        __asm ("ISB"); /* Transfers using updated MPU settings */

        for (T_mode = 0; T_mode < T_MODE_NUM; T_mode++)
        {
            for (T_access = 0; T_access < T_ACCESS_RIGHTS_NUM; T_access++)
            {
                T_slavePortErr = false;
                g_hardFaultFlag = false;
                switch (T_access)
                {
                    case T_WRITE_DENY:
                        /* Try to write to Sram_U memory, can not write, hard fault occur */
                        sramUArray[0] = 0;
                        __asm ("DSB");
                        __asm ("ISB");
                        break;
                    case T_READ_DENY:
                        /* Try to read to Sram_U memory, can not write, hard fault occur */
                        T_temp = *((uint32_t*) &sramUArray[0]);
                        __asm ("DSB");
                        __asm ("ISB");
                        break;
                    case T_EXECUTE_DENY:
                        __asm ("DSB");
                        __asm ("ISB");
                        break;
                    default:
                        break;
                }
                /* Add delay to avoid the check instruction to be done
                 * during the hard fault handler response period
                 */
                T_mpuDelay();

                if (g_hardFaultFlag)
                {
                    T_slavePortErr = MPU_HAL_GetSlavePortErrorStatus(T_g_mpuBase[T_instance], T_SLAVE_PORT_3);
                    /* Verification Point: An error has occurred for slave port 0 */
                    EU_ASSERT(T_slavePortErr == true);
                    /* Get detail error access info in slave port 0 */
                    MPU_DRV_GetDetailErrorAccessInfo(T_instance, T_SLAVE_PORT_3, &mpuSramUAccessErrInfo);
                    /* Change access right of region to initialize status */
                    (void)MPU_DRV_SetMasterAccessRights(T_instance, T_MPU_REGION0, AccessRightRegion0Config);
                    /* Change access right T_access: 0, 1, 2(Write, Read, Execute deny) */
                    (void)MPU_DRV_SetMasterAccessRights(T_instance, T_SRAML_REGION_TEST, AccessRightConfig[T_access + 1]);

                    if (T_mode == 1)
                    {
                        /* switch to user mode */
                        swUserMode();
                        __asm ("DSB");
                        __asm ("ISB");
                    }
                }
            } /* End of test access right */
            /* Change access right to read and execute */
            (void)MPU_DRV_SetMasterAccessRights(T_instance, T_SRAML_REGION_TEST, AccessRightConfig[T_WRITE_DENY]);
            /* switch to user mode */
            swUserMode();
            __asm ("DSB");
            __asm ("ISB");

        } /* End of test mode */
        MPU_DRV_Deinit(T_instance);
        __asm ("DSB");
        __asm ("ISB");
    }

    /* Disable clock the all MPU modules */
    T_Mpu_Set_Clock(DISABLED);
}

#ifdef __cplusplus
}
#endif

/** @} */
