/**
*   Copyright (c) 2016, Freescale Semiconductor, Inc.
*   ALL RIGHTS RESERVED.
*   @file    Mpu_TC_0102.c
*   @version M4_SDK_TEST_VERSION_MAJOR.M4_SDK_TEST_VERSION_MINOR.M4_SDK_TEST_VERSION_PATCH
*
*   @brief   Test case 0102
*   @details Test case 0102
*
*   @addtogroup [MPU_TESTS]
*   @{
*/
/*==================================================================================================
ifdef(`M4_SRC_KEEP_REVISION_HISTORY', `dnl - DO NOT modify this M4 line!
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Anh Le - B57183               01/07/2016     ASDK-1341     Initial version for SDK
---------------------------   ----------    ------------  ------------------------------------------
',)dnl - DO NOT modify this M4 line!
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "Mpu_TC_0102.h"

/*==================================================================================================
*                          LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
==================================================================================================*/


/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/


/*==================================================================================================
*                                      LOCAL CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      LOCAL VARIABLES
==================================================================================================*/


/*==================================================================================================
*                                      GLOBAL CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      GLOBAL VARIABLES
==================================================================================================*/


/*==================================================================================================
*                                   LOCAL FUNCTION PROTOTYPES
==================================================================================================*/


/*==================================================================================================
*                                       LOCAL FUNCTIONS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id        Mpu_TC_0102
* @brief          Check MPU driver with difference regions configuration
* @details        This test case is used to check setting in MPU_DRV_Init() and related functions
*                 with difference regions configuration:
*                   - Configure for FLASH region
*                   - Configure for SRAM_L region
*                   - Configure for SRAM_U region
*                   - Configure with overlap region
*                   - Configure with low bus master
*                   - Configure with high bus master
*
* @pre            - The test code shall enable clock the all MPU modules
*                 - The MPU module shall initialize configuration structure pointer.
* @post           N/A
*
* @test_level     Integration
* @test_type      Functional
* @test_technique WhiteBox
* @test_procedure Steps:
*                   -# Initialize clock for all MPU modules
*                   -# Initialize the MPU module by calling the MPU_DRV_Init() function
*                   -# Verification Point:
*                           -# MPU module shall enable
*                           -# MPU module shall disable
*                           -# Verify access rights for high master of region 0
*                           -# Verify access rights for low master of region 0
*                           -# Check process identifier for low master
*                           -# Check start address
*                           -# Check end address
*                           -# Check PID setting of regionNum
*                           -# Check PID mask setting of regionNum
*                           -# Verify access rights for high master of region 0
*                           -# Verify access rights for low master of region 0
*                           -# Check process identifier for low master
*                   -# Disable clock for all MPU modules
* @pass_criteria  Verification Points are successful
*
* @requirements   MPU_001_001, MPU_003_001, MPU_004_001, MPU_005_001, MPU_005_004, MPU_007_001,
*                 MPU_007_002, MPU_008_001, MPU_008_003, MPU_009_001, MPU_009_003, MPU_009_004,
*                 MPU_012_001, MPU_013_001, MPU_017_001, MPU_018_001, MPU_021_001, MPU_022_001,
*                 MPU_023_001, MPU_024_001, MPU_028_001, MPU_029_001, MPU_030_001, MPU_031_001,
*                 MPU_032_001, MPU_033_001, MPU_034_001, MPU_035_001, MPU_036_001
*
* @traceability   N/A
* @execution_type Automated
* @hw_depend      N/A
* @sw_depend      N/A
* @boundary_test  N/A
* @defects
* @test_priority  High
* @note
* @keywords
*/

void Mpu_TC_0102(void)
{
    /* Local variables */
    mpu_status_t T_status = MPU_STATUS_SUCCESS;
    uint32_t regionNum;
    uint32_t T_masterNum;
    uint8_t T_instance;
    uint32_t T_startAddr;
    uint32_t T_endAddr;
    uint32_t T_accRights;
    uint32_t T_accShift;
    uint32_t T_accMask;
    uint32_t T_pidMask;
    uint32_t T_pid;
    uint32_t T_temp;
    bool T_ret = false;
    uint32_t count;

    /* Initialize and configure clocks for MPU module */
    T_Mpu_Init_Clock();

    for (T_instance = 0; T_instance < MPU_INSTANCE_COUNT; T_instance++)
    {
        /* Check status when initialize for MPU module */
        T_status = MPU_DRV_Init(T_instance, T_REGION_INIT_COUNT, mpu_Diff_Region_Config);
        /* Verification Point: The initialization status shall be MPU_STATUS_SUCCESS */
        EU_ASSERT(T_status == MPU_STATUS_SUCCESS);

        T_ret = MPU_HAL_IsEnable(T_g_mpuBase[T_instance]);
        /* Verification Point: MPU module shall enable */
        EU_ASSERT(T_ret == true);

        /* Disable MPU module */
        MPU_HAL_Disable(T_g_mpuBase[T_instance]);
        T_ret = MPU_HAL_IsEnable(T_g_mpuBase[T_instance]);
        /* Verification Point: MPU module shall disable */
        EU_ASSERT(T_ret == false);

        for (regionNum = 0; regionNum < T_REGION_INIT_COUNT; regionNum++)
        {
            if (regionNum == 0)
            {
                for (count = 0; count < FSL_FEATURE_MPU_MASTER_COUNT; count++)
                {
                    /* Do not check for DEBUGGER bus master */
                    if (count == 1)
                    {
                        continue;
                    }
                    T_masterNum = mpu_Diff_Region_Config[regionNum]\
                                 .masterAccRight[count].masterNum;
                    if (T_masterNum > FSL_FEATURE_MPU_MAX_LOW_MASTER_NUMBER)
                    {
                        T_accRights = (mpu_Diff_Region_Config[regionNum].masterAccRight[count]\
                                       .accessRight & (MPU_W_MASK | MPU_R_MASK)) >> MPU_W_SHIFT;
                        T_accShift = FSL_FEATURE_MPU_HIGH_MASTER_CONTROL_WIDTH * \
                                     (T_masterNum -(FSL_FEATURE_MPU_MAX_LOW_MASTER_NUMBER + 1U));
                        T_accMask = (MPU_RGDAAC_M4WE_MASK | MPU_RGDAAC_M4RE_MASK) << T_accShift;
                        T_temp = (T_g_mpuBase[T_instance]->RGDAAC[regionNum] & T_accMask) \
                                  >> (MPU_RGDAAC_M4WE_SHIFT + T_accShift);
                        /* Verification Point: Check access rights for high master of region 0 */
                        EU_ASSERT(T_accRights == T_temp);
                    }
                    else
                    {
                        T_accRights = mpu_Diff_Region_Config[regionNum]\
                                     .masterAccRight[count].accessRight;
                        T_accMask = MPU_RGDAAC_M0UM_MASK | MPU_RGDAAC_M0SM_MASK;
                        T_accShift = (T_masterNum * FSL_FEATURE_MPU_LOW_MASTER_CONTROL_WIDTH);
                        T_accRights = T_accRights << T_accShift;
                        T_accMask = T_accMask << T_accShift;
                        T_temp = T_g_mpuBase[T_instance]->RGDAAC[regionNum] & T_accMask;
                        /* Verification Point: Check access rights for low master of region 0 */
                        EU_ASSERT(T_accRights == T_temp);
#if FSL_FEATURE_MPU_HAS_PROCESS_IDENTIFIER
                        T_accRights = MPU_RGDAAC_M0PE(mpu_Diff_Region_Config[regionNum]\
                                      .masterAccRight[count].processIdentifierEnable);
                        T_accMask = MPU_RGDAAC_M0PE_MASK;
                        T_accRights = T_accRights << T_accShift;
                        T_accMask = T_accMask << T_accShift;
                        T_temp = T_g_mpuBase[T_instance]->RGDAAC[regionNum] & T_accMask;
                        /* Verification Point: Check process identifier for low master */
                        EU_ASSERT(T_accRights == T_temp);
#endif /* FSL_FEATURE_MPU_HAS_PROCESS_IDENTIFIER */
                    }
                }
            }
            if (regionNum > 0)
            {
                T_startAddr = T_g_mpuBase[T_instance]->RGD[regionNum].WORD0 \
                                                        & MPU_WORD0_SRTADDR_MASK;
                /* Verification Point: Check start address */
                EU_ASSERT(T_startAddr == mpu_Diff_Region_Config[regionNum].startAddr);

                T_endAddr = T_g_mpuBase[T_instance]->RGD[regionNum].WORD1 & MPU_WORD1_ENDADDR_MASK;
                /* Verification Point: Check end address */
                EU_ASSERT(T_endAddr == mpu_Diff_Region_Config[regionNum].endAddr);

                T_pid = (T_g_mpuBase[T_instance]->RGD[regionNum].WORD3 & MPU_WORD3_PID_MASK) \
                         >> MPU_WORD3_PID_SHIFT;
                /* Verification Point: Check PID setting of regionNum */
                EU_ASSERT(T_pid == mpu_Diff_Region_Config[regionNum].processIdentifier);

                T_pidMask = (T_g_mpuBase[T_instance]->RGD[regionNum].WORD3 \
                            & MPU_WORD3_PIDMASK_MASK) >> MPU_WORD3_PIDMASK_SHIFT;
                /* Verification Point: Check PID mask setting of regionNum */
                EU_ASSERT(T_pidMask == mpu_Diff_Region_Config[regionNum].processIdMask);

                for (count = 0; count < FSL_FEATURE_MPU_MASTER_COUNT; count++)
                {
                    T_masterNum = mpu_Diff_Region_Config[regionNum]\
                                 .masterAccRight[count].masterNum;
                    if (T_masterNum > FSL_FEATURE_MPU_MAX_LOW_MASTER_NUMBER)
                    {
                        T_accRights = (mpu_Diff_Region_Config[regionNum].masterAccRight[count]\
                                       .accessRight & (MPU_W_MASK | MPU_R_MASK)) >> MPU_W_SHIFT;
                        T_accShift = FSL_FEATURE_MPU_HIGH_MASTER_CONTROL_WIDTH * \
                                     (T_masterNum -(FSL_FEATURE_MPU_MAX_LOW_MASTER_NUMBER + 1U));
                        T_accMask = (MPU_WORD2_M4WE_MASK | MPU_WORD2_M4RE_MASK) << T_accShift;
                        T_temp = (T_g_mpuBase[T_instance]->RGD[regionNum].WORD2 & T_accMask) \
                                  >> (MPU_WORD2_M4WE_SHIFT + T_accShift);
                        /* Verification Point: Check access rights for high master */
                        EU_ASSERT(T_accRights == T_temp);
                    }
                    else
                    {
                        T_accRights = mpu_Diff_Region_Config[regionNum]\
                                     .masterAccRight[count].accessRight;
                        T_accMask = MPU_WORD2_M0UM_MASK | MPU_WORD2_M0SM_MASK;
                        T_accShift = (T_masterNum * FSL_FEATURE_MPU_LOW_MASTER_CONTROL_WIDTH);
                        T_accRights = T_accRights << T_accShift;
                        T_accMask = T_accMask << T_accShift;
                        T_temp = T_g_mpuBase[T_instance]->RGD[regionNum].WORD2 & T_accMask;
                        /* Verification Point: Check access rights for low master */
                        EU_ASSERT(T_accRights == T_temp);
#if FSL_FEATURE_MPU_HAS_PROCESS_IDENTIFIER
                        T_accRights = MPU_WORD2_M0PE(mpu_Diff_Region_Config[regionNum]\
                                      .masterAccRight[count].processIdentifierEnable);
                        T_accMask = MPU_WORD2_M0PE_MASK;
                        T_accRights = T_accRights << T_accShift;
                        T_accMask = T_accMask << T_accShift;
                        T_temp = T_g_mpuBase[T_instance]->RGD[regionNum].WORD2 & T_accMask;
                        /* Verification Point: Check process identifier for low master */
                        EU_ASSERT(T_accRights == T_temp);
#endif /* FSL_FEATURE_MPU_HAS_PROCESS_IDENTIFIER */
                    }
                }
            }
        }
    }

    /* Disable clock the all MPU modules */
    T_Mpu_Set_Clock(DISABLED);
}

#ifdef __cplusplus
}
#endif

/** @} */
