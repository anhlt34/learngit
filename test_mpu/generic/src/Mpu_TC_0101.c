/**
*   Copyright (c) 2016, Freescale Semiconductor, Inc.
*   ALL RIGHTS RESERVED.
*   @file    Mpu_TC_0101.c
*   @version M4_SDK_TEST_VERSION_MAJOR.M4_SDK_TEST_VERSION_MINOR.M4_SDK_TEST_VERSION_PATCH
*
*   @brief   Test case 0101
*   @details Test case 0101
*
*   @addtogroup [MPU_TESTS]
*   @{
*/
/*==================================================================================================
ifdef(`M4_SRC_KEEP_REVISION_HISTORY', `dnl - DO NOT modify this M4 line!
Revision History:
                             Modification     Tracking
Author (core ID)              Date D/M/Y       Number     Description of Changes
---------------------------   ----------    ------------  ------------------------------------------
Anh Le - B57183               01/07/2016     ASDK-1341     Initial version for SDK
---------------------------   ----------    ------------  ------------------------------------------
',)dnl - DO NOT modify this M4 line!
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/

#include "Mpu_TC_0101.h"

/*==================================================================================================
*                          LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
==================================================================================================*/


/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/


/*==================================================================================================
*                                      LOCAL CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      LOCAL VARIABLES
==================================================================================================*/


/*==================================================================================================
*                                      GLOBAL CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      GLOBAL VARIABLES
==================================================================================================*/

/*==================================================================================================
*                                   LOCAL FUNCTION PROTOTYPES
==================================================================================================*/


/*==================================================================================================
*                                       LOCAL FUNCTIONS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

/*================================================================================================*/
/**
* @test_id        Mpu_TC_0101
* @brief          Check status error code and hardware information of MPU driver
* @details        This test is used to check:
*                     +) Status error code:
*                           - MPU_STATUS_CLOCK_OFF
*                           - MPU_STATUS_FAIL
*                           - MPU_STATUS_SUCCESS
*                     +) Hardware information
*
* @pre            - The test code shall enable clock the all MPU modules for check disable
*                   clock status.
*                 - The MPU module shall enable clock and use error configuration structure pointer.
* @post           N/A
*
* @test_level     Integration
* @test_type      Functional
* @test_technique BlackBox
* @test_procedure Steps:
*                   -# Disable clock for all MPU modules
*                   -# Initialize the MPU module by calling the MPU_DRV_Init() function
*                   -# Verification Point: The initialization status shall be
*                      MPU_STATUS_CLOCK_OFF.
*                   -# Enabled clock for all MPU modules
*                   -# Initialize the MPU modules with master number is greater than the
*                       maximum number of masters supported by the hardware
*                   -# Verification Point: The initialization status shall be
*                      MPU_STATUS_FAIL.
*                   -# Initialize the MPU modules with correct configure structure
*                   -# Verification Point: The initialization status shall be MPU_STATUS_SUCCESS.
*                   -# Get hardware revision level information
*                   -# Verification Point: Check hardware revision level of MPU
*                   -# Disable clock for all MPU modules
* @pass_criteria  Verification Points are successful
*
* @requirements   MPU_002_001, MPU_003_001, MPU_004_001, MPU_005_002, MPU_005_003, MPU_005_004,
*                 MPU_008_002, MPU_008_003, MPU_009_002, MPU_025_001
* @traceability   N/A
* @execution_type Automated
* @hw_depend      N/A
* @sw_depend      N/A
* @boundary_test  N/A
* @defects
* @test_priority  High
* @note
* @keywords
*/

void Mpu_TC_0101(void)
{
    /* Local variables */
    mpu_status_t T_status = MPU_STATUS_SUCCESS;
    uint8_t T_instance;
    mpu_hardware_info_t T_hardwareInfo;

    /* Initialize and configure clocks for MPU module */
    T_Mpu_Init_Clock();

    /* Disable clock the all MPU modules */
    T_Mpu_Set_Clock(DISABLED);

    for (T_instance = 0; T_instance < MPU_INSTANCE_COUNT; T_instance++)
    {
        /* Check status when disabled clock for MPU module */
        T_status = MPU_DRV_Init(T_instance, T_REGION_COUNT, memProtect1_ErrConfig);
        /* Verification Point: The initialization status shall be MPU_STATUS_CLOCK_OFF */
        EU_ASSERT(T_status == MPU_STATUS_CLOCK_OFF);
    }

    /* Enable clock the all MPU modules */
    T_Mpu_Set_Clock(ENABLED);

    for (T_instance = 0; T_instance < MPU_INSTANCE_COUNT; T_instance++)
    {
        /* Check status when master number is greater max master number */
        T_status = MPU_DRV_Init(T_instance, T_REGION_COUNT, memProtect1_ErrConfig);
        /* Verification Point: The initialization status shall be MPU_STATUS_FAIL */
        EU_ASSERT(T_status == MPU_STATUS_FAIL);
    }

    for (T_instance = 0; T_instance < MPU_INSTANCE_COUNT; T_instance++)
    {
        /* Check status when use correctly configuration structure for MPU module */
        T_status = MPU_DRV_Init(T_instance, T_REGION_COUNT, memProtect1_InitConfig);
        /* Verification Point: The initialization status shall be MPU_STATUS_SUCCESS */
        EU_ASSERT(T_status == MPU_STATUS_SUCCESS);
    }

    T_hardwareInfo.hardwareRevisionLevel = MPU_HAL_GetHardwareRevision(T_g_mpuBase[T_MPU0]);
    /* Verification Point: Check hardware revision level of MPU */
    EU_ASSERT(T_hardwareInfo.hardwareRevisionLevel == T_MPU_HARDWARE_INFO);

    /* Disable clock the all MPU modules */
    T_Mpu_Set_Clock(DISABLED);
}

#ifdef __cplusplus
}
#endif

/** @} */
