#include <stdio.h>

typedef struct island{
    const char *name;
    const char *opens;
    const char *closes;
    struct island *next;
}islands;

void display(islands *start){
    islands *i = start;
    for(; i != NULL; i = i->next)
        printf("Name : %s, open %s-%s\n", i->name, i->opens, i->closes);
}

int main()
{
    islands haNoi = {"HaNoi", "08:30", "11:50", NULL};
    islands DaNang = {"DaNang", "09:30", "12:50", NULL};
    islands Vinh = {"Vinh", "10:30", "13:50", NULL};
    islands Hue = {"Hue", "11:30", "14:50", NULL};

    haNoi.next = &Vinh;
    Vinh.next = &Hue;
    Hue.next = &DaNang;

    islands hoangSa = {"TruongSa", "00:00", "23:59", NULL};
    Hue.next = &hoangSa;
    hoangSa.next = &DaNang;

    display(&haNoi);
    printf("Hello\n");

    int x, y;
    x = y = -5/2*3;
    printf("aaa: %d\n", x = y);
    return 0;
}
